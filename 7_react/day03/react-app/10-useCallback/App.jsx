import React, { useEffect, useRef, useState, useMemo, useCallback } from 'react'

// useCallback: 缓存函数
// useCallback(callback, [依赖项])
// 1. useCallback 会把 callback 缓存起来
// 2. 依赖项改变时重新创建函数，依赖项没改变时直接从缓存中读取函数

const list = []

const App = () => {
  const [title, setTitle] = useState('标题')
  const [arr, setArr] = useState([1,2,3,4,5])

  const add = useCallback(() => {
    setArr([...arr, arr.length + 1])
  }, [arr])

  // const add = () => {
  //   setArr([...arr, arr.length + 1])
  // }

  if (list.indexOf(add) === -1) {
    list.push(add)
  }

  console.log('渲染App', list)

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      <button onClick={add}>add</button>
      <ul>
        {arr.map(it => <li key={it}>{it}</li>)}
      </ul>
    </div>
  )
}

export default App