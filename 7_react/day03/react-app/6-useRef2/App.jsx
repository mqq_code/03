import React, { useEffect, useRef, useState } from 'react'

// ref
// 1. 获取dom
// 2. 存数据，ref定义的变量在组件的整个生命周期中始终指向同一个地址，数据改变组件不会更新

const App = () => {
  const [count, setCount] = useState(0)
  const timer = useRef(null)
  const num = useRef(10)

  const start = () => {
    timer.current = setInterval(() => {
      setCount(c => c + 1)
    }, 1000)
    window.timer1 = timer

    console.log('开始定时器', timer)
  }

  const stop = () => {
    console.log('停止定时器', timer === window.timer1)
    clearInterval(timer.current)
  }

  useEffect(() => {
    return () => {
      clearInterval(timer.current)
    }
  }, [])

  // console.log('App', timer === window.timer1)

  return (
    <div>
      <p>计时：{count}</p>
      <button onClick={start}>开始计时</button>
      <button onClick={stop}>停止定时器</button>
      <button onClick={() => setCount(1000)}>修改count</button>
      <hr />
      {num.current}
      <button onClick={() => {
        num.current++
        console.log(num)
      }}>num+</button>
    </div>
  )
}

export default App