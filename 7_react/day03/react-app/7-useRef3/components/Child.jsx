import React, { Component } from 'react'

export default class Child extends Component {

  state = {
    title: 'Child'
  }

  changeTitle = title => {
    this.setState({
      title
    })
  }

  render() {
    return (
      <div style={{ border: '1px solid', padding: 20 }}>
        <h2>{this.state.title}</h2>
        <button onClick={() => this.changeTitle(Math.random())}>修改标题</button>
      </div>
    )
  }
}
