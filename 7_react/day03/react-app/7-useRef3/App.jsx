import React, { useEffect, useRef, useState } from 'react'
import Child from './components/Child'

// ref
// 1. 获取dom
// 2. 存数据，ref定义的变量在组件的整个生命周期中始终指向同一个地址，数据改变组件不会更新
// 3. 获取 class 子组件的实例对象，调用子组件的方法

const App = () => {

  const childRef = useRef(null)

  const handleClick = () => {
    // 子组件的实例对象
    console.log(childRef.current)
    childRef.current.changeTitle(123)
  }
  
  return (
    <div>
      <h1>App</h1>
      <button onClick={handleClick}>修改child标题</button>
      <Child ref={childRef} />
    </div>
  )
}

export default App