import React, { useState, useEffect } from 'react'

// useEffect: 处理组件的副作用（定时器、绑定原生事件、调接口）
// useEffect(callback, [依赖项])
// callback 在组件渲染完成之后执行，可以获取到最新的dom

// 作用：
// 1. 只传入callback，不传第二个参数：组件每次更新都会执行，类似 componentDidUpdate
// 2. 依赖项传空数组：callback 只执行一次，类似 componentDidMount
// 3. callback 可以 return 一个函数： 此函数在组件销毁时执行，类似 componentWillUnmount
// 4. 依赖项传入具体值：依赖项改变时执行


const Child = () => {
  const [count, setCount] = useState(0)

  useEffect(() => {
    // 在定时器中修改数据，如果想要获取到最新值必须传入函数
    const timer = setInterval(() => {
      setCount(prev => {
        console.log('定时器', prev)
        if (prev + 1 >= 50) {
          clearInterval(timer)
        }
        return prev + 1
      })
    }, 1000)

    return () => {
      console.log('组件销毁清除定时器')
      clearInterval(timer)
    }
  }, [])


  useEffect(() => {
    const keydown = e => {
      console.log('键盘按下', e.key)
    }
    document.addEventListener('keydown', keydown)
    return () => {
      document.removeEventListener('keydown', keydown)
    }
  })

  return <div style={{ border: '1px solid', padding: 20 }}>
    <h2>child组件</h2>
    <p>定时器： {count}</p>
  </div>
}





const App = () => {
  const [title, setTitle] = useState('App标题')
  const [banners, setBanners] = useState([])

  const getBanners = async () => {
    const res = await fetch('https://zyxcl.xyz/music/api/banner').then(res => res.json())
    console.log(res.banners)
    setBanners(res.banners)
  }

  useEffect(() => {
    getBanners()
  }, [])

  // 错误写法：
  //  useEffect 不能接受 async 函数作为参数，
  //  因为 callback 需要 return 一个函数清除副作用，
  //  但是 async 函数返回的是 Promise 对象不能作为清理函数调用
  // useEffect(async () => {
  //   const res = await fetch('https://zyxcl.xyz/music/api/banner').then(res => res.json())
  //   console.log(res.banners)
  //   setBanners(res.banners)
  // }, [])

 
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      {title.length > 0 && <Child />}
      <hr />
      {/* {JSON.stringify(banners)} */}
    </div>
  )
}

export default App

