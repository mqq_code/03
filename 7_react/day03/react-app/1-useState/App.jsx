import React, { useState } from 'react'

// v16.8 之后可以使用hook
// hook: 让函数组件可是实现类组件的功能（定义数据、处理副作用模拟生命周期...）
// hook使用规则
// 1. hook 只能在最顶层使用，不能在if、for、子函数中使用
// 2. hook 只能在函数组件和自定义hook中使用，不能再class组件和普通函数中使用

// useState: 定义函数组件中的数据

const App = () => {
  const [count, setCount] = useState(10)
  const [title, setTitle] = useState('App标题')
  const [list, setList] = useState([1,2,3,4,5])
  const [xm, setXm] = useState({ name: '小明', age: 22 })

  const push = () => {
    setList([...list, Math.random()])
  }
  const remove = val => {
    setList(list.filter(v => v !== val))
  }

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      <button onClick={() => setCount(count - 1)}>-</button>
      {count}
      <button onClick={() => {
        // 用传入新数据覆盖上一次的数据
        setCount(count + 1)
      }}>+</button>
      <hr />
      <button onClick={push}>添加</button>
      <ul>
        {list.map(item =>
          <li key={item}>
            {item}
            <button onClick={() => remove(item)}>删除</button>
          </li>
        )}
      </ul>
      <hr />
      <p>{JSON.stringify(xm)}</p>
      <button onClick={() => {
        // setXm({...xm, age: xm.age + 1})
        const obj = {...xm}
        obj.age++
        setXm(obj)
      }}>age+</button>
    </div>
  )
}

export default App