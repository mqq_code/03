import React, { useState } from 'react'

// v16.8 之后可以使用hook
// hook: 让函数组件可是实现类组件的功能（定义数据、处理副作用模拟生命周期...）
// hook使用规则
// 1. hook 只能在最顶层使用，不能在if、for、子函数中使用
// 2. hook 只能在函数组件和自定义hook中使用，不能再class组件和普通函数中使用

// useState: 定义函数组件中的数据

const App = () => {
  const [count, setCount] = useState(0)

  const add = () => {
    // 更新变量的函数事异步更新的
    // setCount(count + 1) 无法确定count时最新值
    // 如果想获取到最新的 count，可以给 setCount 传入一个函数，函数的参数就是最新值
    setCount(c => {
      console.log('最新的count', c)
      return c + 1
    })
    setCount(c => {
      console.log('最新的count', c)
      return c + 1
    })
    setCount(c => {
      console.log('最新的count', c)
      return c + 1
    })
    console.log(count)
  }

  console.log('渲染App')
 
  return (
    <div>
      <button onClick={() => setCount(count - 1)}>-</button>
      {count}
      <button onClick={add}>+</button>
    </div>
  )
}

export default App