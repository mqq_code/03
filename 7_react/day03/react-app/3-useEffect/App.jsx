import React, { useState, useEffect } from 'react'

// useEffect: 处理组件的副作用（定时器、绑定原生事件、调接口）
// useEffect(callback, [依赖项])
// callback 在组件渲染完成之后执行，可以获取到最新的dom

// 作用：
// 1. 只传入callback，不传第二个参数：组件每次更新都会执行，类似 componentDidUpdate
// 2. 依赖项传空数组：callback 只执行一次，类似 componentDidMount
// 3. callback 可以 return 一个函数： 此函数在组件销毁时执行，类似 componentWillUnmount
// 4. 依赖项传入具体值：依赖项改变时执行




const Child = () => {

  useEffect(() => {
    console.log('Child组件挂载完成')
    return () => {
      console.log('Child 组件销毁了')
    }
  }, [])

  return <div style={{ border: '1px solid', padding: 20 }}>
    <h2>child组件</h2>
  </div>
}




const App = () => {
  const [count, setCount] = useState(0)
  const [title, setTitle] = useState('App标题')


  // useEffect(() => {
  //   console.log('组件渲染完成1', document.querySelector('span').outerHTML)
  // })

  // useEffect(() => {
  //   console.log('组件渲染完成2', document.querySelector('span').outerHTML)
  // }, [])

  // useEffect(() => {
  //   console.log('count 或者 title 改变了', count, title)
  // }, [count, title])


  useEffect(() => {
    console.log('count 改变了', count)

    return () => {
      console.log('count return 的函数', count)
    }
  }, [count])

 
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      <button onClick={() => setCount(count - 1)}>-</button>
      <span>{count}</span>
      <button onClick={() => setCount(count + 1)}>+</button>
      <hr />
      {/* {count > 0 && <Child />} */}
    </div>
  )
}

export default App