import React, { useRef } from 'react'

const App = () => {
  const inp = useRef(null)
  console.log(inp)

  return (
    <div>
      <h1>App</h1>
      {/* 使用ref获取dom元素 */}
      <input type="text" ref={inp} />
      <button onClick={() => {
        console.log(inp.current.value)
      }}>提交</button>
    </div>
  )
}

export default App