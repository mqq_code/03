import React, { useEffect, useRef, useState, useMemo, useCallback } from 'react'
import Child from './components/Child'

// useCallback: 缓存函数
// useCallback(callback, [依赖项])
// 1. useCallback 会把 callback 缓存起来
// 2. 依赖项改变时重新创建函数，依赖项没改变时直接从缓存中读取函数

// 使用场景：
// 当子组件使用了 React.memo 优化组件性能时，如果需要给子组件传入函数，此函数就需要使用 useCallback 进行缓存
// 否则每次组件更新时都会创建新函数导致子组件的 React.memo 就会失效

const App = () => {
  const [title, setTitle] = useState('标题')
  const [count, setCount] = useState(0)

  const changeCount = useCallback(n => {
    setCount(count + n)
  }, [count])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      {count}
      <button onClick={() => setCount(count + 1)}>+</button>
      <hr />
      <Child count={count} onChange={changeCount} />
    </div>
  )
}

export default App