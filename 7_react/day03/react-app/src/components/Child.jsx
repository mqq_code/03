import React, { useState, useImperativeHandle, forwardRef } from 'react'


const Child = (props) => {
  const [title, setTitle] = useState('Child标题')

  console.log('渲染Child组件', props)

  return (
    <div style={{ border: '1px solid', padding: 20 }}>
      <h2>{title}</h2>
      <button onClick={() => setTitle(Math.random())}>修改标题</button>
      <p>父组件的count: {props.count}</p>
      <button onClick={() => props.onChange(2)}>props.count+2</button>
    </div>
  )
}

// React.memo: 高阶组件，会浅比较组件中最新的 props 和当前的 props 是否有更新
// 没有更新就不重新渲染 Child，减少组件不必要的更新
export default React.memo(Child)

// export default React.memo(Child, (prevProps, nextProps) => {
//   console.log(prevProps, nextProps)
//   if (prevProps.count === nextProps.count) {
//     // 需要缓存组件，不重新渲染
//     return true
//   }
//   // 不需要缓存，重新渲染组件
//   return false
// })