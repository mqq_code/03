import React, { useEffect, useRef, useState } from 'react'
import Child from './components/Child'

// ref
// 1. 获取dom
// 2. 存数据，ref定义的变量在组件的整个生命周期中始终指向同一个地址，数据改变组件不会更新
// 3. 获取 class 子组件的实例对象，调用子组件的方法
// 4. 调用 函数子组件内的数据和方法，需要配合 forwardRef 和 useImperativeHandle 使用

const App = () => {

  const childRef = useRef(123)

  const handleClick = () => {
    // 子组件的实例对象
    console.log(childRef)
    childRef.current.reset()
  }

  return (
    <div>
      <h1>App</h1>
      <button onClick={handleClick}>修改child标题</button>
      <Child ref={childRef} />
    </div>
  )
}

export default App