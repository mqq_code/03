import React, { useState, useImperativeHandle, forwardRef } from 'react'

const Child = (props, ref) => {
  const [title, setTitle] = useState('Child标题')

  const reset = () => {
    setTitle('我是重置标题')
  }

  // 作用：给父组件的ref赋值
  useImperativeHandle(ref, () => {
    // 把 return 的对象赋值给 ref.current
    return {
      a: 1, 
      b: 2,
      c: ['a', 'b', 'c'],
      reset,
      title
    }
  }, [title])


  return (
    <div style={{ border: '1px solid', padding: 20 }}>
      <h2>{title}</h2>
      <button onClick={() => setTitle(Math.random())}>修改标题</button>
    </div>
  )
}

// 高阶组件：接收父组件传入的 ref 对象，传给 Child 组件的第二个参数
export default forwardRef(Child)