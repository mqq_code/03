import React, { useEffect, useRef, useState, useMemo } from 'react'

// useMemo: 缓存数据
// useMemo(callback, [依赖项])
// 1. callback 需要有返回值
// 2. useMemo 会把 callback 的返回值缓存起来
// 3. 依赖项改变时重新执行 callback 计算结果，依赖项没改变时直接从缓存中读取数据



const App = () => {
  const [title, setTitle] = useState('标题')

  const [arr, setArr] = useState([1,2,3,4,5])
  const [isOdd, setIsOdd] = useState(true)

  // const filterArr = () => {
  //   console.log('筛选数据')
  //   return arr.filter(v => {
  //     if (isOdd) {
  //       return v % 2 === 1
  //     }
  //     return  v % 2 === 0
  //   })
  // }

  const filterArr = useMemo(() => {
    console.log('筛选数据')
    return arr.filter(v => {
      if (isOdd) {
        return v % 2 === 1
      }
      return  v % 2 === 0
    })
  }, [arr, isOdd])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      <button onClick={() => {
        setArr([...arr, arr.length + 1])
      }}>add</button>

      <button onClick={() => setIsOdd(!isOdd)}>{isOdd ? '奇数' : '偶数'}</button>
      <ul>
        {filterArr.map(it => <li key={it}>{it}</li>)}
      </ul>
      <hr />
      <ul>
        {filterArr.map(it => <li key={it}>{it}</li>)}
      </ul>
      <hr />
      <ul>
        {filterArr.map(it => <li key={it}>{it}</li>)}
      </ul>
      <hr />
      <ul>
        {filterArr.map(it => <li key={it}>{it}</li>)}
      </ul>
    </div>
  )
}

export default App