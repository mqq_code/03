import { createRoot } from 'react-dom/client' // react-dom 核心语法，创建根节点，渲染元素
import React from'react' // react 核心语法，创建元素、创建组件
import './index.scss'
import App from './App'

createRoot(document.getElementById('root')).render(<App a="测试一下" />)
