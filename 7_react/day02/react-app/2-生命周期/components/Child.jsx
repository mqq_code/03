import React, { Component } from 'react'
import style from './Child.module.scss'


class Child extends Component {

  // 组件销毁之前：清除异步，例如定时器、原生事件
  componentWillUnmount() {
    console.log('%c componentWillUnmount组件销毁之前', 'color: green; font-size: 20px')
  }

  render() {
    return (
      <div className={style.box}>
        <h2>Child</h2>
        <p className='desc'>bbbb</p>
      </div>
    )
  }
}

export default Child
