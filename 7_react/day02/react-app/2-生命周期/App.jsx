import React, { Component } from 'react'
import style from './App.module.scss'
import Child from './components/Child'

class App extends Component {

  // 初始化state和props
  constructor(props) {
    super(props)
    this.state = {
      count: 0
    }
    console.log('%c constructor组件初始化', 'color: red; font-size: 20px')
  }

  // 挂载完成，定时器、调用接口、获取dom
  componentDidMount() {
    console.log('%c componentDidMount组件挂载完成', 'color: red; font-size: 20px')
  }

  // 组件更新完成: 可以获取到最新的dom
  componentDidUpdate() {
    console.log('%c componentDidUpdate组件更新完成', 'color: red; font-size: 20px')
  }

  // 挂载和更新阶段都会执行render函数
  render() {
    console.log('%c render渲染', 'color: red; font-size: 20px')
    return (
      <div className={style.app}>
        <h1>标题</h1>
        {this.state.count}
        <button onClick={() => {
          // 触发更新
          this.setState({
            count: this.state.count + 1
          })
        }}>+</button>
        {this.state.count < 5 && <Child />}
      </div>
    )
  }
}

export default App

// 1. 挂载阶段
//   a. constructor 初始化state和props
//   b. render 渲染虚拟dom
//   c. componentDidMount 挂载成功
// 2. 更新阶段
//   a. shouldComponentUpdate 判断组件是否需要更新，用来做性能优化，返回true更新，返回false不更新
//   b. render 渲染虚拟dom
//   c. componentDidUpdate 组件更新完成
// 3. 销毁阶段
//   a. componentWillUnmount 组件销毁之前调用，可以用来清除异步操作