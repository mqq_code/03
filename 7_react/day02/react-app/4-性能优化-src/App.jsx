import React, { Component } from 'react'
import Child from './components/Child'

class App extends Component {

  state = {
    count: 0,
    title: 'app'
  }

  render() {

    console.log('app的render')

    const { count, title } = this.state
    return (
      <div className='app'>
        <input type="text" value={title} onChange={e => this.setState({ title: e.target.value })} />
        <h2>{title}</h2>
        {count}
        <button onClick={() => this.setState({ count: count + 1 })}>+</button>
        <Child count={count} />
      </div>
    )
  }
}

export default App
