import React, { Component, PureComponent } from 'react'


// PureComponent 性能优化：内部实现对组件中的props和state的数据进行浅比较判断是否需要更新
class Child extends PureComponent {

  state = {
    title: '我是child'
  }

  // 优化性能生命周期：可以手动比较 state 和 props 判断组件是否应该更新
  // shouldComponentUpdate(nextProps, nextState) {
  //   // console.log('当前的props', this.props)
  //   // console.log('最新的props', nextProps)
  //   // console.log('当前的state', this.state)
  //   // console.log('最新的state', nextState)
  //   if (this.state.title !== nextState.title || this.props.count !== nextProps.count) {
  //     return true
  //   }
  //   return false
  // }

  render() {
    console.log('child 的render')

    const { title } = this.state
    return (
      <div className='child'>
        <h3>{title}</h3>
        <button onClick={() => {
          this.setState({ title: '随机标题' + Math.random() })
        }}>随机修改标题</button>
        <div>父组件的count： {this.props.count}</div>
      </div>
    )
  }
}

export default Child
