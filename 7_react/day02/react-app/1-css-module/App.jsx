import React from 'react'
import style from './App.module.scss'
import Child from './components/Child'

const App = () => {
  return (
    <div className={style.app}>
      <h1>标题</h1>
      <Child />
      <p>11111</p>

      <div className="box">box</div>
    </div>
  )
}

export default App