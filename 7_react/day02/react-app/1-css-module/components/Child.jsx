import React, { Component } from 'react'
import style from './Child.module.scss'
// css module 实现组件样式隔离

console.log(style)

class Child extends Component {
  render() {
    return (
      <div className={style.box}>
        <h2>Child</h2>
        <p className='desc'>bbbb</p>
      </div>
    )
  }
}

export default Child
