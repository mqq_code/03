import React, { Component } from 'react'
import style from './App.module.scss'
import classNames from 'classnames'
import Comment from './components/comment/Comment'
import Header from './components/header/Header'
class App extends Component {

  state = {
    list: []
  }

  submit = (form) => {
    this.setState({
      list: [form, ...this.state.list]
    })
  }

  remove = id => {
    this.setState({
      list: this.state.list.filter(v => v.id !== id)
    })
  }

  render() {
    const { list } = this.state
    return (
      <div className={style.app}>
        <h2>来 , 说说你在做什么 , 想什么</h2>
        <Header onSubmit={this.submit} />
        <div className={style.list}>
          {list.map(item =>
            <Comment {...item} remove={this.remove} key={item.id} />
            // <Comment
            //   username={item.username}
            //   content={item.content}
            //   id={item.id}
            //   date={item.date}
            //   key={item.id}
            // />
          )}
        </div>
      </div>
    )
  }
}

export default App
