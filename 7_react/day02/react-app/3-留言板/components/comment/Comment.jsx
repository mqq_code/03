import React, { Component } from 'react'
import style from './Comment.module.scss'

export default class Comment extends Component {
  render() {
    return (
      <div className={style.comment}>
        <img src={this.props.avatar} alt="" />
        <div className={style.info}>
          <div className={style.commentRight}>
            <span>{this.props.username}:</span>
            {this.props.content}
          </div>
          <p>{this.props.date}</p>
          <span className={style.del} onClick={() => this.props.remove(this.props.id)}>删除</span>
        </div>
      </div>
    )
  }
}
