import React, { Component } from 'react'
import style from './Header.module.scss'
import classNames from 'classnames'

class Header extends Component {
  state = {
    imgs: [
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face1.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face2.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face3.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face4.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face5.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face6.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face7.gif',
      'https://www.jq22.com/demo/jquery-lyb20151124/img/face8.gif'
    ],
    curIndex: 0,
    form: {
      username: '',
      content: ''
    },
  }

  submit = () => {
    const { imgs, curIndex, form } = this.state
    if (!form.username) {
      alert('请输入用户名！')
      return
    } else if (!form.content) {
      alert('请输入留言！')
      return
    }
    if (140 - form.content.length < 0) {
      alert('字数已超出！')
      return
    }
    // 调用父组件方法，通知父组件要添加的内容
    this.props.onSubmit({
      id: Date.now(),
      ...form,
      avatar: imgs[curIndex],
      date: new Date().toLocaleString()
    })

    this.setState({
      form: {
        username: '',
        content: ''
      },
      curIndex: 0
    })
  }

  changeForm = (key, e) => {
    this.setState({
      form: {...this.state.form, [key]: e.target.value}
    })
  }



  render() {
    const { imgs, curIndex, form } = this.state
    const count = 140 - form.content.length

    return (
      <div>
        <div className={style.username}>
          <input type="text" value={form.username} onChange={e => this.changeForm('username', e)} />
          <ul>
            {imgs.map((v, i) =>
              <li
                key={v}
                // className={curIndex === i ? `${style.item} ${style.active}` : style.item}
                className={classNames(style.item, { [style.active]: curIndex === i })}
                onClick={() => {
                  this.setState({ curIndex: i })
                }}
              >
                <img src={v} alt="" />
              </li>
            )}
          </ul>
        </div>
        <div className={style.content}>
          <textarea value={form.content} onChange={e => this.changeForm('content', e)}></textarea>
        </div>
        <div className={style.submit}>
          <div className={style.count}>
            {count < 0 ?
              <i style={{ color: 'tomato' }}>已超出{Math.abs(count)}个字</i>
            :
              <span>还能输入{count}个字</span>
            }
          </div>
          <button onClick={this.submit}>发布</button>
        </div>
      </div>
    )
  }
}


export default Header