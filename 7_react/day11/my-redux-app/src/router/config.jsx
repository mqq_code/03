import { Navigate } from 'react-router-dom'
import React, { Children } from 'react'
import { ErrorBlock } from 'antd-mobile'
import Home from '../pages/home/Home'
import Login from '../pages/login/Login'
import Detail from '../pages/detail/Detail'
import Address from '../pages/address/Address'
import Create from '../pages/create/Create'
import Cart from '../pages/home/cart/Cart'
import Goods from '../pages/home/goods/Goods'
import Mine from '../pages/home/mine/Mine'

const routes = [
  {
    path: '/',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    children: [
      { path: '/home', element: <Goods /> },
      { path: '/home/cart', element: <Cart /> },
      { path: '/home/mine', element: <Mine /> }
    ]
  },
  {
    path: '/detail/:id',
    element: <Detail />
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/address',
    element: <Address />
  },
  {
    path: '/create/:id?',
    element: <Create />
  },
  {
    path: '*',
    element: <ErrorBlock fullPage status="empty" title="找不到此页面" />
  }
]



export default routes