import React, { useMemo } from 'react'
import {
  Form,
  Input,
  Button,
  TextArea,
  Toast,
  Dialog
} from 'antd-mobile'
import { useDispatch, useSelector } from 'react-redux'
import { createAddress, save, remove } from '../../store/features/address'
import { useNavigate, useParams } from 'react-router-dom'

const Create = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const params = useParams()
  const list = useSelector(state => state.address.list)

  const onFinish = (values) => {
    if (params.id) {
      dispatch(save({...values, id: params.id}))
      Toast.show({
        icon: 'success',
        content: '保存成功',
      })
    } else {
      dispatch(createAddress(values))
      Toast.show({
        icon: 'success',
        content: '新增成功',
      })
    }
    navigate(-1)
  }

  const initState = useMemo(() => {
    return list.find(v => v.id === params.id)
  }, [params.id, list])

  const del = () => {
    const handler = Dialog.show({
      content: '确定要删除此地址吗？',
      actions: [
        [
          {
            key: 'cancel',
            text: '取消',
            onClick: () => {
              handler.close()
              console.log('点击取消')
            }
          },
          {
            key: 'delete',
            text: '删除',
            bold: true,
            danger: true,
            onClick: () => {
              dispatch(remove(params.id))
              handler.close()
              navigate(-1)
            }
          },
        ],
      ]
    })
  }

  return (
    <div>
      <Form
        initialValues={initState}
        onFinish={onFinish}
        layout='horizontal'
        footer={
          <>
            <Button block type='submit' color='primary' size='middle'>提交</Button>
            {params.id &&
              <Button block style={{ marginTop: 10 }} color='danger' size='middle' onClick={del}>删除</Button>
            }
          </>
        }
      >
        <Form.Item
          name='name'
          label='姓名'
          rules={[{ required: true, message: '姓名不能为空' }]}
        >
          <Input placeholder='请输入姓名' />
        </Form.Item>
        <Form.Item
          name='tel'
          label='手机号'
          rules={[
            { required: true, message: '手机号不能为空' },
            { pattern: /^1[3-9]\d{9}$/, message: '手机号格式错误' }
          ]}
        >
          <Input placeholder='请输入手机号' />
        </Form.Item>
        <Form.Item name='address' label='地址' help='详情地址' rules={[{ required: true, message: '地址不能为空' }]}>
          <TextArea
            placeholder='请输入地址'
            maxLength={100}
            rows={2}
            showCount
          />
        </Form.Item>
      </Form>
    </div>
  )
}

export default Create