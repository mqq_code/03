import React, { useEffect, useState } from 'react'
import { List, Button, Toast } from 'antd-mobile'
import {
  EnvironmentOutline
} from 'antd-mobile-icons'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { getGoods, changeCount } from '../../../store/features/cart'
import style from './Goods.module.scss'

const Goods = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const curAddress = useSelector(state => state.address.curAddress)
  const goodsList = useSelector(state => state.cart.goodsList)

  useEffect(() => {
    dispatch(getGoods())
  }, [])

  return (
    <div>
      <List>
        <List.Item prefix={<EnvironmentOutline />} onClick={() => navigate('/address')}>
          {curAddress ? curAddress.address : '请选择配送地址'}
        </List.Item>
      </List>
      <div className={style.list}>
        {goodsList.map(item =>
          <div key={item.id} className={style.goodsItem}>
            <img src={item.img} alt="" />
            <p>{item.name}</p>
            <p>¥{item.price}</p>
            <Button size="mini" color="primary" onClick={() => {
              dispatch(changeCount({
                id: item.id,
                num: 1
              }))
              Toast.show('添加成功')
            }}>+</Button>
          </div>
        )}
      </div>
    </div>
  )
}

export default Goods