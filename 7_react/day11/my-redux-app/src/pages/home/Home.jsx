import React from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import style from './Home.module.scss'
import { Badge, TabBar } from 'antd-mobile'
import {
  AppOutline,
  MessageFill,
  UnorderedListOutline,
} from 'antd-mobile-icons'

const Home = () => {
  const location = useLocation()
  const navigate = useNavigate()
  const tabs = [
    {
      key: '/home',
      title: '首页',
      icon: <AppOutline />
    },
    {
      key: '/home/cart',
      title: '购物车',
      icon: <UnorderedListOutline />
    },
    {
      key: '/home/mine',
      title: '我的',
      icon: <MessageFill />
    }
  ]

  return (
    <div className={style.home}>
      <main>
        <Outlet />
      </main>
      <TabBar className={style.tabBar} activeKey={location.pathname} onChange={navigate}>
        {tabs.map(item => (
          <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
        ))}
      </TabBar>
    </div>
  )
}

export default Home