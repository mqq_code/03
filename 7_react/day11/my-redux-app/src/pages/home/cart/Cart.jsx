import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { List, Button, Space, Checkbox } from 'antd-mobile'
import { changeCount, changeChecked, changeAll } from '../../../store/features/cart'

const Cart = () => {
  const dispatch = useDispatch()
  // const curAddress = useSelector(state => state.address.curAddress)
  const cartList = useSelector(state => state.cart.cartList)
  const [checkAll, setCheckAll] = useState(false)

  useEffect(() => {
    setCheckAll(cartList.every(v => v.checked))
  }, [cartList])

  return (
    <div style={{ height: '100%', overflow: 'auto' }}>
      <div className='list'>
        {cartList.map(it =>
          <div key={it.id} style={{ borderBottom: '1px solid', padding: 10 }}>
            <Checkbox checked={it.checked} onChange={c => {
              dispatch(changeChecked({
                id: it.id,
                checked: c
              }))
            }}>选中状态</Checkbox>
            <h4>{it.name}</h4>
            <p>¥{it.price}</p>
            <Space>
              <Button color="warning" size="mini" onClick={() => {
                dispatch(changeCount({
                  id: it.id,
                  num: -1
                }))
              }}>-</Button>
              {it.count}
              <Button color="primary" size="mini" onClick={() => {
                dispatch(changeCount({
                  id: it.id,
                  num: 1
                }))
              }}>+</Button>
            </Space>
          </div>
        )}
      </div>
      <div>
        <Checkbox checked={checkAll} onChange={value => {
          setCheckAll(value)
          dispatch(changeAll(value))
        }}>全选</Checkbox>
      </div>
    </div>
  )
}

export default Cart