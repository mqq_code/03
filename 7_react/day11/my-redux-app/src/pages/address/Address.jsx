import React from 'react'
import style from './Address.module.scss'
import { Button, Empty, List } from 'antd-mobile'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { changeAddress } from '../../store/features/address'
import { CheckCircleFill, EditSOutline } from 'antd-mobile-icons'

const Address = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const list = useSelector(state => state.address.list)
  const curAddress = useSelector(state => state.address.curAddress)

  return (
    <div className={style.address}>
      <div className={style.list}>
        {list.length === 0 ?
          <Empty style={{ height: '100%' }} imageStyle={{ width: 100 }} description='暂无数据' />
        :
          <List header='地址列表'>
            {list.map(item => (
              <List.Item
                key={item.id}
                prefix={<CheckCircleFill color={curAddress?.id === item.id ? 'tomato' : ''} />}
                description={item.address}
                onClick={() => {
                  dispatch(changeAddress(item))
                  navigate(-1)
                }}
                arrowIcon={false}
                extra={<EditSOutline onClick={(e) => {
                  e.stopPropagation()
                  navigate(`/create/${item.id}`)
                }} />}
              >
                {item.name} - {item.tel}
              </List.Item>
            ))}
          </List>
        }
      </div>
      <div className={style.footer}>
        <Button block shape='rounded' color='primary' onClick={() => navigate('/create')}>新增地址</Button>
      </div>
    </div>
  )
}

export default Address