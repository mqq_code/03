import Mock from 'mockjs'

const data = Mock.mock({
  "list|20": [
    {
      "id": "@id",
      "name": "@cname",
      "price|5-50": 5,
      "img": "@image(100x100, @color)"
    }
  ]
})


Mock.mock('/api/list', () => {
  return data.list
})