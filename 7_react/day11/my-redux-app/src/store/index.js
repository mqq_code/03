import { configureStore } from '@reduxjs/toolkit'
import address from './features/address'
import cart from './features/cart'

// 创建store
const store = configureStore({
  reducer: {
    address,
    cart
  }
})

export default store
