import { createSlice } from '@reduxjs/toolkit'

export const addressSlice = createSlice({
  name: 'address',
  initialState: {
    list: [
      {
        id: '11',
        name: '王小明',
        tel: 13633336666,
        address: '北京市朝阳区大悦城'
      },
      {
        id: '22',
        name: '李小刚',
        tel: 15899990000,
        address: '北京市海淀区八维'
      }
    ], // 地址列表
    curAddress: {
      id: '22',
      name: '李小刚',
      tel: 15899990000,
      address: '北京市海淀区八维'
    } // 当前选中的地址
  },
  reducers: {
    createAddress(state, action) {
      state.list.unshift({
        ...action.payload,
        id: Date.now() + ''
      })
    },
    changeAddress(state, action) {
      state.curAddress = action.payload
    },
    save(state, { payload }) {
      // 根据传入的数据查找下标，替换数据
      const index = state.list.findIndex(v => v.id === payload.id)
      state.list.splice(index, 1, payload)
      // 如果修改的是当前正在选中的地址，同步修改选中的对象
      if (state.curAddress.id === payload.id) {
        state.curAddress = payload
      }
    },
    remove(state, { payload }) {
      const index = state.list.findIndex(v => v.id === payload)
      state.list.splice(index, 1)
      if (state.curAddress.id === payload) {
        state.curAddress = null
      }
    }
  }
})

export const { createAddress, changeAddress, save, remove } = addressSlice.actions

export default addressSlice.reducer