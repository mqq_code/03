import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

export const getGoods = createAsyncThunk('getGoods', async () => {
  const res = await axios.get('/api/list')
  return res.data
})

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    goodsList: [],
    cartList: []
  },
  reducers: {
    changeCount(state, { payload }) {
      const index = state.cartList.findIndex(v => v.id === payload.id)
      if (index > -1) {
        state.cartList[index].count += payload.num
        if (state.cartList[index].count <= 0) {
          state.cartList.splice(index, 1)
        }
      } else {
        const item = state.goodsList.find(v => v.id === payload.id)
        state.cartList.push({
          ...item,
          count: 1
        })
      }
    },
    changeChecked(state, { payload }) {
      const item = state.cartList.find(v => v.id === payload.id)
      item.checked = payload.checked
    },
    changeAll(state, { payload }) {
      state.cartList.forEach(item => {
        item.checked = payload
      })
    }
  },
  extraReducers: builder => {
    builder
      .addCase(getGoods.fulfilled, (state, action) => {
        console.log(action.payload)
        state.goodsList = action.payload
      })
  }
})

export const { changeCount, changeChecked, changeAll } = cartSlice.actions

export default cartSlice.reducer