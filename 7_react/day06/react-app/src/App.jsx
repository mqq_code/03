import React, { useReducer, useState } from 'react'

// 修改和返回最新的state
const reducer = (state, action) => {
  // action: dispatch 传入的参数
  if (action.type === 'change_name') {
    return {...state, name: action.payload}
  } else if (action.type === 'change_age') {
    return {...state, age: state.age + 1}
  }
  return state
}
const initState = {
  name: 'xxx',
  age: 22,
  hobby: []
}


const App = () => {

  // const [obj, setObj] = useState({
  //   name: 'xxx',
  //   age: 22,
  //   hobby: []
  // })

  const [state, dispatch] = useReducer(reducer, initState)

  console.log(state)

  return (
    <div>
      <h2>App</h2>
      <p>姓名: {state.name}
        <button onClick={() => {
          // 调用 dispatch 触发 reducer 函数执行，返回新的state
          // 把 action 传入 reducer函数，reducer 根据 action.type 判断本次要修改的内容
          dispatch({
            type: 'change_name',
            payload: Math.random()
          })
        }}>修改姓名</button>
      </p>
      <p>年龄: {state.age}
        <button onClick={() => {
          dispatch({
            type: 'change_age'
          })
        }}>修改年龄</button>
      </p>
    </div>
  )
}

export default App
