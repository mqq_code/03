import React, { Component } from 'react'

export default function (Com) {
  
  class Pos extends Component {
    state = {
      pos: {
        x: 0,
        y: 0
      }
    }
    mousemove = e => {
      this.setState({
        pos: {
          x: e.clientX,
          y: e.clientY
        }
      })
    }
    componentDidMount() {
      document.addEventListener('mousemove', this.mousemove)
    }
    componentWillUnmount() {
      document.removeEventListener('mousemove', this.mousemove)
    }

    render() {
      return (
        <Com pos={this.state.pos} {...this.props} />
      )
    }
  }

  return Pos
}
