import React, { useState } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'



const App = () => {
  const [show, setShow] = useState(true)
  const [title, setTitle] = useState('app 标题')
  return (
    <div>
      <h1>{title}</h1>
      <button onClick={() => setShow(!show)}>切换</button>
      {show && <Child1 />}
      <Child2 title={title} />
    </div>
  )
}

export default App