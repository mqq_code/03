import React, { Component } from 'react'
import withPos from '../hoc/withPos'

// 高阶组件：接收一个组件作为参数，返回值为新组件的函数
// 作用：抽取公用逻辑，给组件添加额外的功能

class Child2 extends Component {
  render() {
    return (
      <div className='box'>
        <h2>Child2</h2>
        <p>app的标题： {this.props.title}</p>
        <p>pos: {JSON.stringify(this.props.pos)}</p>
        <input type="text" />
        <button>提交</button>
      </div>
    )
  }
}
export default withPos(Child2)