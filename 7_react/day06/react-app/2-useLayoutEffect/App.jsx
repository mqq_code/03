import React, { useEffect, useLayoutEffect, useState } from 'react'

const App = () => {
  const [str, setStr] = useState(0)

  // useEffect(() => {
  //   // 组件渲染完成之后执行此函数
  //   if (str === '1111111111111111111111111111111111111111111111111111111') {
  //     setStr(0)
  //   }
  // }, [str])

  useLayoutEffect(() => {
    // dom 构建成功浏览器绘制之前执行
    if (str === '1111111111111111111111111111111111111111111111111111111') {
      setStr(0)
    }
  }, [str])

  return (
    <div>
      <h2>App</h2>
      <p>{str}</p>
      <button onClick={() => setStr(Math.random())}>随机</button>
      <button onClick={() => {
        setStr('1111111111111111111111111111111111111111111111111111111')
      }}>修改</button>
    </div>
  )
}

export default App
