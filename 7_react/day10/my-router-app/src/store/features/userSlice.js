import { createSlice } from '@reduxjs/toolkit'

// 创建数据仓库
export const userSlice = createSlice({
  name: 'userSlice',
  initialState: {
    name: '小明',
    age: 20
  },
  reducers: {
    // 定义修改数据的方法
    setName(state, action) {
      state.name = action.payload
    },
    changeAge(state) {
      state.age++
    }
  }
})

// 抛出 action
export const { setName, changeAge } = userSlice.actions

// 抛出reducer
export default userSlice.reducer

