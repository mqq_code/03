import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

// 创建异步 action， 调用接口
export const getBanner = createAsyncThunk('getBanner', async () => {
  const res = await axios.get('https://zyxcl.xyz/music/api/banner123')
  return res.data.banners
})
export const getToplist = createAsyncThunk('getToplist', async () => {
  const res = await axios.get('https://zyxcl.xyz/music/api/toplist')
  return res.data.list
})




// 创建数据仓库
export const cartSlice = createSlice({
  name: 'cartSlice',
  initialState: {
    list: [],
    banners: []
  },
  reducers: {
    
  },
  // 接收异步 action 的结果
  extraReducers: builder => {
    builder
      .addCase(getBanner.pending, (state, action) => {
        // getBanner 状态为 pending 时执行此函数
        console.log('等待中')
      })
      .addCase(getBanner.fulfilled, (state, action) => {
        // getBanner 状态改为 fulfilled 时执行此函数
        state.banners = action.payload
        console.log('成功')
      })
      .addCase(getBanner.rejected, (state, action) => {
        // getBanner 状态改为 rejected 时执行此函数
        console.log('失败')
      })
      .addCase(getToplist.fulfilled, (state, action) => {
        console.log(action.payload)
        state.list = action.payload
      })
  }
})

export default cartSlice.reducer
