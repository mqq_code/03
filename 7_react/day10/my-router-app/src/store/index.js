import { configureStore } from '@reduxjs/toolkit'
import user from './features/userSlice'
import cart from './features/cartSlice'

// 创建store
const store = configureStore({
  reducer: {
    user,
    cart
  }
})

export default store
