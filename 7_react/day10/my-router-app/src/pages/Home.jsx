import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setName, changeAge } from '../store/features/userSlice'

const Home = () => {
  const name = useSelector(s => s.user.name)
  const age = useSelector(s => s.user.age)

  const dispatch = useDispatch()

  return (
    <div>
      <h1>Home</h1>
      <input type="text" value={name} onChange={e => {
        dispatch(setName(e.target.value))
      }} />
      <p>姓名：{name}</p>
      <p>年龄：{age}
        <button onClick={() => {
          dispatch(changeAge())
        }}>+</button>
      </p>
    </div>
  )
}

export default Home