import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getBanner, getToplist } from '../store/features/cartSlice'

const About = () => {
  const dispatch = useDispatch()

  const name = useSelector(s => s.user.name)
  const banners = useSelector(s => s.cart.banners)
  const list = useSelector(s => s.cart.list)

  useEffect(() => {
    dispatch(getBanner())
  }, [])

  return (
    <div>
      <h1>About</h1>
      <p>{name}</p>
      <hr />
      {JSON.stringify(banners)}
      <hr />
      <button onClick={() => {
        dispatch(getToplist())
      }}>获取toplist</button>
      <div>
        {JSON.stringify(list)}
      </div>
    </div>
  )
}

export default About