import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Home = () => {

  const dispatch = useDispatch()

  // 获取store中的数据
  const title = useSelector(rootState => rootState.title)

  console.log(title)


  return (
    <div>
      <h1>Home</h1>
      <h2>标题：{title}</h2>
      <button onClick={() => {
        // store.dispatch: 执行store中的reducer函数，把action传给reducer函数
        // action: 必须有type属性的对象，用来描述本次修改的内容
        dispatch({
          type: 'change_title',
          payload: Math.random()
        })
      }}>修改标题</button>
    </div>
  )
}

export default Home