import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const About = () => {
  const dispatch = useDispatch()
  const title = useSelector(s => s.title)
  const list = useSelector(s => s.list)

  return (
    <div>
      <h1>About</h1>
      <h2>{title}</h2>
      <input type="text" value={title} onChange={e => {
        dispatch({
          type: 'change_title',
          payload: e.target.value
        })
      }} />
      <hr />
      <button onClick={() => {
        dispatch({
          type: 'push_list',
          payload: Math.random()
        })
      }}>添加</button>
      <ul>
        {list.map(item => <li key={item}>{item}</li>)}
      </ul>
    </div>
  )
}

export default About