import React, { useEffect, useState } from 'react'
import store from '../store'

const About = () => {
  const [title, setTitle] = useState(store.getState().title)
  const [list, setList] = useState(store.getState().list)

  useEffect(() => {
    const clear = store.subscribe(() => {
      console.log('about页面监听', store.getState())
      setTitle(store.getState().title)
      setList(store.getState().list)
    })
    return clear 
  }, [])


  return (
    <div>
      <h1>About</h1>
      <h2>{title}</h2>
      <input type="text" value={title} onChange={e => {
        store.dispatch({
          type: 'change_title',
          payload: e.target.value
        })
      }} />
      <hr />
      <button onClick={() => {
        store.dispatch({
          type: 'push_list',
          payload: Math.random()
        })
      }}>添加</button>
      <ul>
        {list.map(item => <li key={item}>{item}</li>)}
      </ul>
    </div>
  )
}

export default About