import React, { useEffect, useState } from 'react'
import store from '../store'

// 获取store数据
// console.log(store.getState())
// console.log(store)

const Home = () => {

  const [title, setTitle] = useState(store.getState().title)

  useEffect(() => {
    // 监听store中的数据改变
    const clearSub = store.subscribe(() => {
      console.log('store的数据改变了', store.getState().title)
      setTitle(store.getState().title)
    })

    return () => {
      // 组件销毁时清除监听
      console.log('home组件销毁')
      clearSub()
    }
  }, [])


  return (
    <div>
      <h1>Home</h1>
      <h2>标题：{title}</h2>
      <button onClick={() => {
        // store.dispatch: 执行store中的reducer函数，把action传给reducer函数
        // action: 必须有type属性的对象，用来描述本次修改的内容
        store.dispatch({
          type: 'change_title',
          payload: Math.random()
        })
      }}>修改标题</button>
    </div>
  )
}

export default Home