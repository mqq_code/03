import { createStore } from 'redux'

const initState = {
  title: '我是store的标题',
  list: []
}

// 返回和更新数据
const reducer = (state, action) => {
  // console.log('reducer函数', state, action)
  // console.log('title', state.title)

  // 根据传入的 action.type 判断本次应该如何修改
  if (action.type === 'change_title') {
    return {...state, title: action.payload}
  } else if (action.type === 'push_list') {
    return {...state, list: [...state.list, action.payload]}
  }
  return state
}

// 创建store
const store = createStore(reducer, initState)

export default store