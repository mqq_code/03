import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setTitleAction, getBannerApi } from '../store/actions'

const Home = () => {

  const dispatch = useDispatch()
  // 获取store中的数据
  const title = useSelector(rootState => rootState.title)
  const banners = useSelector(rootState => rootState.banners)


  useEffect(() => {
    // 添加 redux-thunk 中间件后，可以给 dispatch 传入一个函数
    dispatch(getBannerApi)
  }, [])


  return (
    <div>
      <h1>Home</h1>
      <h2>标题：{title}</h2>
      <button onClick={() => {
        dispatch(setTitleAction(Math.random()))

        // dispatch({
        //   type: 'change_title',
        //   payload: 'asdfads'
        // })
      }}>修改标题</button>
      <hr />
      <ul>
        {banners.map(it =>
          <li key={it.targetId}>
            <img src={it.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home