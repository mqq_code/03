import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { setTitleAction, pushListAction, getBannerApi } from '../store/actions'

const About = () => {
  const dispatch = useDispatch()
  const title = useSelector(s => s.title)
  const list = useSelector(s => s.list)
  const banners = useSelector(s => s.banners)

  useEffect(() => {
    dispatch(getBannerApi)
  }, [])

  return (
    <div>
      <h1>About</h1>
      <h2>{title}</h2>
      <input type="text" value={title} onChange={e => {
        dispatch(setTitleAction(e.target.value))
      }} />
      <hr />
      <button onClick={() => {
        dispatch(pushListAction(Date.now()))
      }}>添加</button>
      <ul>
        {list.map(item => <li key={item}>{item}</li>)}
      </ul>
      <hr />
      {JSON.stringify(banners)}
    </div>
  )
}

export default About