import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger' // 在调用 dispatch 时自动打印更新前后的数据
import { thunk } from 'redux-thunk' // 调用 dispatch 时允许传入函数，处理异步逻辑

const initState = {
  title: '我是store的标题',
  list: [],
  banners: []
}

// 返回和更新数据
const reducer = (state, action) => {
  if (action.type === 'change_title') {
    return {...state, title: action.payload}
  } else if (action.type === 'push_list') {
    return {...state, list: [...state.list, action.payload]}
  } else if (action.type === 'set_banners') {
    return {...state, banners: action.payload}
  }
  return state
}

// applyMiddleware: 给 store 添加中间件
// 中间件: 增强 dispatch，给 dispatch 添加额外的功能
const store = createStore(reducer, initState, applyMiddleware(thunk, logger))

export default store