import axios from 'axios'

// 统一管理所有action



// 获取抡博图数据
export const getBannerApi = async (dispatch) => {
  const res = await axios.get('https://zyxcl.xyz/music/api/banner')
  // 调用原本的 dispatch，执行 reducer 函数
  dispatch({
    type: 'set_banners',
    payload: res.data.banners
  })
}


// 修改标题
export const setTitleAction = payload => {
  return {
    type: 'change_title',
    payload
  }
}

// 添加list
export const pushListAction = payload => {
  return {
    type: 'push_list',
    payload
  }
}
