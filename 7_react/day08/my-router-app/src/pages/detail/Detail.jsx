import React, { useEffect, useMemo, useState } from 'react'
import { useLocation, useSearchParams, useParams, useNavigate } from 'react-router-dom'
import { getCinemaInfo, getCinemaFilms, getSchedule } from '../../services'
import style from './Detail.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import classNames from 'classnames'

const Detail = () => {
  const [cinemaInfo, setCinemaInfo] = useState(null)
  const [films, setFilms] = useState([])
  const [activeIndex, setActiveIndex] = useState(0)
  const [schedules, setSchedules] = useState([])
  const [daysIndex, setDaysIndex] = useState(0)
  const [showTitle, setShowtitle] = useState(false)
  const [filmFixed, setFilmFixed] = useState(false)

  const navigate = useNavigate()

  // 获取动态路由参数
  const params = useParams()

  const getCinemaInfoApi = async () => {
    try {
      const res = await getCinemaInfo(params.id)
      console.log(res.data.data.cinema)
      setCinemaInfo(res.data.data.cinema)
    } catch(e) {
      console.log(e)
    }
  }
  const getCinemaFilmsApi = async () => {
    try {
      const res = await getCinemaFilms(params.id)
      console.log(res.data.data.films)
      setFilms(res.data.data.films)
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    getCinemaInfoApi()
    getCinemaFilmsApi()
  }, [])

  useEffect(() => {
    // 调用接口
    if (films[activeIndex]) {
      getSchedule({
        cinemaId: params.id,
        filmId: films[activeIndex].filmId,
        date: films[activeIndex].showDate[daysIndex]
      })
      .then(res => {
        console.log(res.data.data.schedules)
        setSchedules(res.data.data.schedules)
      })
    }
    
  }, [activeIndex, films, daysIndex])

  const handleScroll = e => {
    console.log(e.target.scrollTop)
    if (e.target.scrollTop > 45) {
      setShowtitle(true)
    } else {
      setShowtitle(false)
    }
    if (e.target.scrollTop > 295) {
      setFilmFixed(true)
    } else {
      setFilmFixed(false)
    }
  }

  return (
    <div className={style.detail} onScroll={handleScroll}>
      <div className={style.titleBar}>
        <div className={style.back} onClick={() => {
          navigate(-1)
        }}>
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAA2CAYAAACm9ZtyAAAAAXNSR0IArs4c6QAABApJREFUWAnlWUlPFEEUruoeyLgwoCYmTPewROTgeJQrW0KCkOBFExNioheNetfoD8DoWUj0ZFQS8cgBNIqG6InoTT1hhNk4uByYg8jMdPne0I/0TO/LyEESqKWr3vfVq9f9vSoY+x9+hBBcUZRxVVXHsP7P15xUlOn2pCLwF4kYCUjGRiPqipKaEoJdI9ucc0F1LBtKIKmqNzSh3doF5PxpNptd3G1DJWZsRFlvV9XLQhN3ySbnbL73eM+leg80JCCSydR5wbRZAK96GEDf7N8XH1tdXf1DhKiMfAuqQcbFYwJnnK8cams9YwWOJCL1ALxmA5pgL+BVi1dXyPknmbOBXC73q9q2+CNb9AXqSqVSpyqaeAmTD6ABcPvX5pg8DEH33clgJAQ6OjpOVDRtCYDadPB8U0weWl9fzzqB47PQMdDZ2dldrmiv4F0/UgVn7AeAjwD4NzdwfbyXYdZjALy9VK68gz0/tjOCb7KYNLyRyXy0nmHuDRyEEHCHK4ItMyFOolnY898SZ6ch4JbNMPY9gbYgnU4fBPBFA3iJM3HOL3iVuD036yddXV3x7VJ5Adw+pI/QOJMmC4XsM+sZzr2+PDA4OBjbLpXmDOAQxvxqUHCk5lkLABQ0PfUIpGyC1sQlfrOQyz2kdpDSswcUVb0vmJgkEIlLUwB+j9pBS09vAWq6UVZB2WYK+fz1oKDGea4EUNNrZJXx2Xw+e6FeVo1G/dQdY8BG0y9GBY5EbT1goelvm5tiY2tra1t+Vug21jIIbTR9ImpwSw8E0XS3VTo9r5HjoJruBOD2rGYLKhXtAUxowUkQaAVdVjfcjIR5XkMgjKGgc2sIyLJ0BQwV0Rh8epPb5cpr1Pygxr3MM72GpiBk/LMssX6nxNILkN2YGg/gINR00PazEAOlnUkijdqPOYCdkTD9Jg+QsT39ECEJ1HhIsXYPlZgDQC7wHHMCIhlFaesBMt5oMTLFAAFTiZoP2n+H2pgTQG4wTe2wZc2X0M5Ysbi5lEgkjsLzPn1MX6KlNY79dnO89rtuARnSU7InxqxIT8lCZUWuW0AEMAfo7e2BXIDNUx8mKqCc+PEK/OPZA4Swp2k5ksCcAM77E6BWKzopicF9QP3lExF2K317gAxaHM224Lsx6vd0FJgAEjEfTlkxJkt4J/CBiLqVoQjoJLrhhPwe1RPbEKQ/Y7Lcn8lkvriB43PPb4GdMbwHgFWPIDCOwXsCvC/AewO7Ocb+0ATQGK5WlqRRqPrOJUJvgXE1plzCwyVVJB4gEqZcAi4v3HKJSD1ARPzkEpF6gAjY5BJzVrlEQzxARLzkEg0lgETqj/ayxMchVhaIZEO2gIxjCUf52/CNmKE+lHWq/7NyJ5fYw3/ZOK30L7lS0JLnwwCCAAAAAElFTkSuQmCC" width="11px" height="18px" />
        </div>
        {showTitle && <div className={style.title}>{cinemaInfo?.name}</div>}
      </div>

      <header>
        <h3>{cinemaInfo?.name}</h3>
        <ul>
          {cinemaInfo?.services.map(item =>
            <li key={item.name}>{item.name}</li>
          )}
        </ul>
        <p>{cinemaInfo?.address}
          <a href={`tel:${cinemaInfo?.phone}`}>电话</a>
        </p>
      </header>
      
      <div className={style.swiperFilm}>
        <div className={style.bg}>
          <div className={style.imgBg} style={{ backgroundImage: `url(${films[activeIndex]?.poster})` }}></div>
        </div>
        <Swiper
          spaceBetween={16}
          slidesPerView={4}
          centeredSlides={true}
          className={style.swiper}
          onSlideChange={swiper => {
            setActiveIndex(swiper.activeIndex)
          }}
        >
          {films.map((it, index) =>
            <SwiperSlide key={it.filmId} className={classNames(style.slide, { [style.active]: activeIndex === index })}>
              <div className={style.filmImg}>
                <img src={it.poster} alt="" />
              </div>
            </SwiperSlide>
          )}
        </Swiper>
      </div>

      <div className={style.filmDesc}>
        <div className={classNames({ [style.fixed]: filmFixed })}>
          <div className={style.film}>
            <h3>{films[activeIndex]?.name}: {films[activeIndex]?.grade}分</h3>
            <p>{films[activeIndex]?.category} | {films[activeIndex]?.runtime}分钟 ｜ {films[activeIndex]?.director} | {films[activeIndex]?.actors.map(v => v.name).join(' ')}</p>
          </div>
          <div className={style.days}>
            {films[activeIndex]?.showDate.map((it, index) =>
              <p key={it} className={classNames({ [style.active]: daysIndex === index })} onClick={() => setDaysIndex(index)}>{it}</p>
            )}
          </div>
        </div>
      </div>
      

      <div className={style.schedules}>
        {schedules.map((item, index) =>
          <div key={item.scheduleId} className={style.schedulesItem}>
            <div className={style.time}>
              {/* <p>{item.showAt}</p>
              <p>{item.endAt}</p> */}
              <p>{index}</p>
            </div>
            <div className={style.filmLanguage}>
              <h4>{item.filmLanguage} {item.imagery}</h4>
              <p>{item.hallName + item.hallName + item.hallName}</p>
            </div>
            <div className={style.price}>
              <p>¥{item.salePrice / 100}</p>
              <button>购票</button>
            </div>
          </div>
        )}
      </div>

    </div>
  )
}

export default Detail