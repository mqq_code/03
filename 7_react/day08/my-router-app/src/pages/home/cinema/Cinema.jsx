import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const Cinema = () => {
  const [cinemas, setCinemas] = useState([])
  // 跳转路由的 hook
  const navigate = useNavigate()

  useEffect(() => {
    axios.get('https://m.maizuo.com/gateway', {
      params: {
        cityId: 110100,
        ticketFlag: 1,
        k: 2990364
      },
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"17218918638581224398323713","bc":"110100"}',
        'X-Host': 'mall.film-ticket.cinema.list'
      }
    })
      .then(res => {
        console.log(res.data.data.cinemas)
        setCinemas(res.data.data.cinemas)
      })
  }, [])

  const goDetail = id => {
    // search 传参数
    // navigate(`/detail?id=${id}&a=100&b=abc`)

    // 动态路由传参数
    navigate(`/detail/${id}`)
  }

  return (
    <div>
      {cinemas.map(item =>
        <div key={item.cinemaId} style={{ border: '1px solid', padding: 20 }} onClick={() => goDetail(item.cinemaId)}>
          <h4>{item.name}</h4>
          <p>id: {item.cinemaId}</p>
          <p>{item.address}</p>
          <p>价格：¥{item.lowPrice / 100}</p>
        </div>
      )}
    </div>
  )
}

export default Cinema