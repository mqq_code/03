import React, { Suspense } from 'react'
// import Child from './Child'
const Child = React.lazy(() => import('./Child'))

const Mine = () => {
  return (
    <div>
      <h1>个人中心页面</h1>
      <Suspense fallback={<div style={{ background: 'green' }}>loading。。。</div>}>
        <Child />
      </Suspense>
      <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
        <li>10</li>
      </ul>
    </div>
  )
}

export default Mine