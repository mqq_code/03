import React from 'react'

const Child = () => {
  return (
    <div style={{ width: 300, height: 200, background: 'blue' }}>Child</div>
  )
}

export default Child