import {
  Navigate, // 重定向
} from 'react-router-dom'
import React, { Suspense } from 'react'
import Auth from '../auth/Auth'
import Home from '../pages/home/Home'
import Movie from '../pages/home/movie/Movie'
import Mine from '../pages/home/mine/Mine'
// import Cinema from '../pages/home/cinema/Cinema'
// import Login from '../pages/login/Login'
// import Detail from '../pages/detail/Detail'

// 异步组件：打包时把组件打包成单独的 js 文件，减少主包的体积，加快首屏的加载速度
// 注意：React 中的异步组件必须配合 Suspense 使用，必须 Suspense 组件内使用
const Cinema = React.lazy(() => import('../pages/home/cinema/Cinema'))
const Login = React.lazy(() => import('../pages/login/Login'))
const Detail = React.lazy(() => import('../pages/detail/Detail'))

const routes = [
  {
    path: '/',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    title: '首页',
    children: [
      {
        path: '/home',
        element: <Navigate to="/home/movie" />
      },
      {
        path: '/home/movie',
        element: <Movie />,
        title: '电影'
      },
      {
        path: '/home/cinema',
        element: <Cinema />,
        title: '影院',
        isAuth: true
      },
      {
        path: '/home/mine',
        element: <Mine />,
        title: '个人中心',
        isAuth: true
      },
    ]
  },
  {
    path: '/detail/:id',
    element: <Detail />,
    title: '详情页面',
    isAuth: true
  },
  {
    path: '/login',
    element: <Login />,
    title: '登录'
  },
  {
    path: '*',
    element: <div>找不到此页面</div>,
    title: '404'
  }
]


const TitleCom = (props) => {
  if (props.title) {
    document.title = props.title
  }
  if (props.isAuth) {
    return <Auth>{props.children}</Auth>
  }
  return props.children
}

const formatRoutes = (routeConfig) => {
  return routeConfig.map(item => {
    return {
      path: item.path,
      element: (
        <TitleCom title={item.title} isAuth={item.isAuth}>
          {item.element}
        </TitleCom>
      ),
      children: item.children ? formatRoutes(item.children) : []
    }
  })
}


export default formatRoutes(routes)