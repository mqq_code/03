import React, { useEffect, useState } from 'react'
import Header from './components/header/Header'
import Compare from './components/compare/Compare'
import Card from './components/card/Card'
import axios from 'axios'


const App = () => {
  const [list, setList] = useState([])
  const [checked, setChecked] = useState([])
  const [show, setShow] = useState(false)

  const getList = async () => {
    const res = await axios.get('https://zyxcl.xyz/exam_api/bottle')
    setList(res.data.value)
  }

  useEffect(() => {
    getList()
  }, [])

  const changeChecked = (title) => {
    const newList = [...list]
    const index = newList.findIndex(v => v.title === title)
    // 判断长度是否够 3 个
    if (checked.length >= 3 && !newList[index].checked) {
      return
    }
    // 改变状态
    newList[index].checked = !newList[index].checked
    setList(newList)
    if (newList[index].checked) {
      setChecked([...checked, newList[index]])
    } else {
      setChecked(checked.filter(v => v.title !== title))
    }
  }

  return (
    <div className='app'>
      {checked.length > 0 && <Header list={checked} onChange={changeChecked} onCompare={() => setShow(true)} />}
      
      <main>
        {list.map(item =>
          <Card key={item.title} {...item} onChange={changeChecked} />
        )}
      </main>

      {show && <Compare list={checked} onClose={() => setShow(false)} />}
    </div>
  )
}

export default App