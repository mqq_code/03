import React from 'react'
import style from './Card.module.scss'
import classNames from 'classnames'

const Card = (props) => {
  return (
    <div className={classNames(style.card, { [style.active]: props.checked })} >
      <img src={props.src} alt="" />
      <h3>{props.title}</h3>
      <p>{props.price}</p>
      <button onClick={() => props.onChange(props.title)}>
        {props.checked ? '删除' : '添加购物车'}
      </button>
      <span
        className={classNames('iconfont', style.icon, props.checked ? 'icon-duigou' : 'icon-jia')}
        onClick={() => props.onChange(props.title)}
      ></span>
    </div>
  )
}

export default Card