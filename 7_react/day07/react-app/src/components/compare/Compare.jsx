import React, { useState } from 'react'
import style from './Compare.module.scss'
import classNames from 'classnames'

const Compare = (props) => {
  const [leave, setLeave] = useState(false)
  const animateClass = (i) => {
    if (leave) {
      return i % 2 === 0 ? 'animate__fadeOutUpBig' : 'animate__fadeOutDownBig'
    }
    return i % 2 === 0 ? 'animate__fadeInDownBig' : 'animate__fadeInUpBig'
  }
  return (
    <div className={style.compare}>
      {props.list.map((item, index) =>
        <div
          key={item.title}
          className={classNames(
            'animate__animated',
            style.item,
            animateClass(index)
          )}
          onAnimationEnd={() => {
            if (leave) {
              props.onClose()
            }
          }}
        >
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.price}</p>
          <p>{item.year}</p>
          <p>{item.region}</p>
          <p>{item.varietal}</p>
          <p>{item.alcohol}</p>
        </div>
      )}
      {!leave &&
        <div
          className={classNames('iconfont', 'icon-guanbi', style.close)}
          onClick={() => {
            setLeave(true)
            // setTimeout(() => {
            //   props.onClose()
            // }, 600)
          }}
        ></div>
      }
    </div>
  )
}

export default Compare