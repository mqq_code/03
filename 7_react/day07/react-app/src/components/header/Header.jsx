import React from 'react'
import style from './Header.module.scss'

const Header = (props) => {
  return (
    <div className={style.header}>
      <ul>
        {props.list.map(item =>
          <li key={item.title}>
            <img src={item.src} alt="" />
            <span className="iconfont icon-guanbi" onClick={() => props.onChange(item.title)}></span>
          </li>
        )}
        {props.list.length < 3 && <li className={style.empty}></li>}
      </ul>
      <button disabled={props.list.length < 2} onClick={props.onCompare}>比较</button>
    </div>
  )
}

export default Header