import React from 'react'
import {
  Routes, // 路由外层组件
  Route, // 路由配置组件
  Navigate, // 重定向
  Link, // 跳转页面
  NavLink // 跳转页面，有高亮
} from 'react-router-dom'
import Home from './pages/home/Home'
import Login from './pages/login/Login'
import Detail from './pages/detail/Detail'

const App = () => {
  return (
    <div className='app'>
      <nav>
        <NavLink to="/home">首页</NavLink>
        <NavLink to="/detail">详情</NavLink>
        <NavLink to="/login">登录</NavLink>
      </nav>
      {/* 配置路由 */}
      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/home" element={<Home />} />
        {/* <Route path="/" element={<Home />} /> */}
        <Route path="/detail" element={<Detail />} />
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<div>找不到此页面</div>} />
      </Routes>
    </div>
  )
}

export default App