import { createRoot } from 'react-dom/client'
import App from './App.jsx'
import './index.scss'
import {
  // 路由根组件： 所有和路由相关的内容必须在子组件内使用
  HashRouter, // hash 模式，url 有 #
  BrowserRouter // history 模式，url 没有 #
} from 'react-router-dom'

createRoot(document.getElementById('root')).render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
)
