import React from 'react'
import {
  useRoutes // 配置路由表
} from 'react-router-dom'
import routeConfig from './router/config'

const App = () => {
  const routes = useRoutes(routeConfig)

  return routes

  // return (
    // <Routes>
    //   <Route path="/" element={<Navigate to="/home" />} />
    //   <Route path="/home" element={<Home />}>
    //     <Route path="/home" element={<Navigate to="/home/movie" />} />
    //     <Route path='/home/movie' element={<Movie />}></Route>
    //     <Route path='/home/cinema' element={<Cinema />} />
    //     <Route path='/home/mine' element={<Mine />} />
    //   </Route>
    //   <Route path="/detail" element={<Detail />} />
    //   <Route path="/login" element={<Login />} />
    //   <Route path="*" element={<div>找不到此页面</div>} />
    // </Routes>
  // )
}

export default App