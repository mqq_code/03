import React from 'react'
import {
  Routes, // 路由外层组件
  Route, // 路由配置组件
  Navigate, // 重定向
  Link, // 跳转页面
  NavLink // 跳转页面，有高亮
} from 'react-router-dom'
import Home from './pages/home/Home'
import Movie from './pages/home/movie/Movie'
import Cinema from './pages/home/cinema/Cinema'
import Mine from './pages/home/mine/Mine'
import Login from './pages/login/Login'
import Detail from './pages/detail/Detail'

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/home" />} />
      <Route path="/home" element={<Home />}>
        <Route path="/home" element={<Navigate to="/home/movie" />} />
        <Route path='/home/movie' element={<Movie />}></Route>
        <Route path='/home/cinema' element={<Cinema />} />
        <Route path='/home/mine' element={<Mine />} />
      </Route>
      <Route path="/detail" element={<Detail />} />
      <Route path="/login" element={<Login />} />
      <Route path="*" element={<div>找不到此页面</div>} />
    </Routes>
  )
}

export default App