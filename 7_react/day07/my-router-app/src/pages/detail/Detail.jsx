import React, { useEffect, useState } from 'react'
import { useLocation, useSearchParams, useParams } from 'react-router-dom'
import axios from 'axios'
import moment from 'moment'

const Detail = () => {
  const [cinemaInfo, setCinemaInfo] = useState(null)
  const [films, setFilms] = useState([])

  // 获取当前页面的路由信息
  const location = useLocation()
  const [searchParams] = useSearchParams()
  // console.log('获取 search 参数', location.search)
  // console.log('获取 search 参数', searchParams.get('id'))

  // 获取动态路由参数
  const params = useParams()
  console.log(params)

  useEffect(() => {
    axios.get('https://m.maizuo.com/gateway', {
      params: {
        cinemaId: params.id, // searchParams.get('id'),
        k: 9703511
      },
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"17218918638581224398323713","bc":"110100"}',
        'X-Host': 'mall.film-ticket.cinema.info'
      }
    })
      .then(res => {
        console.log(res.data.data.cinema)
        setCinemaInfo(res.data.data.cinema)
      })

    axios.get('https://m.maizuo.com/gateway', {
      params: {
        cinemaId:  params.id, // searchParams.get('id'),
        k: 9703511
      },
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"17218918638581224398323713","bc":"110100"}',
        'X-Host': 'mall.film-ticket.film.cinema-show-film'
      }
    })
      .then(res => {
        console.log(res.data.data.films)
        setFilms(res.data.data.films)
      })

  }, [])

  return (
    <div>
      <h3>{cinemaInfo?.name}</h3>
      <ul>
        {cinemaInfo?.services.map(item =>
          <li key={item.name}>{item.name}</li>
        )}
      </ul>
      <p>{cinemaInfo?.address}</p>

      <div>
        {films.map(it =>
          <div key={it.filmId} style={{ border: '1px solid', padding: 20 }}>
            <p>上映时间：{moment(Date.now() - it.premiereAt).format('YYYY-MM-DD')}</p>
            <img src={it.poster} width={120} alt="" />
            <ul>
              {it.showDate.map(t =>
                <li key={t}>{moment(Date.now() - it.premiereAt + t).format('YYYY-MM-DD')} - {t}</li>
              )}
            </ul>
          </div>
        )}
      </div>
    </div>
  )
}

export default Detail