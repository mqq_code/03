import { createContext } from 'react'

const ctx = createContext()

export const Provider = ctx.Provider
export default ctx