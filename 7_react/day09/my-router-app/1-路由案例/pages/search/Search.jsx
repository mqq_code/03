import React, { useEffect, useState } from 'react'
import style from './Search.module.scss'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const urls = [
  'https://zyxcl.xyz/exam_api/zh',
  'https://zyxcl.xyz/exam_api/xl',
  'https://zyxcl.xyz/exam_api/sx'
]

const searchApi = async keyword => {
  // 调用所有接口
  const res = await Promise.all(urls.map(v => axios.get(v)))
  // 把多维数组转成一维数组
  const allData = res.reduce((prev, val) => {
    val.data.items.forEach(item => {
      // 去重
      if (!prev.find(v => v.item_id === item.item_id)) {
        prev.push(item)
      }
    })
    return prev
  }, [])
  
  const list = allData.filter(v => v.title.includes(keyword))
  return list
}

const Search = () => {
  const [searchList, setSearchList] = useState([])

  const navigate = useNavigate()

  return (
    <div className={style.search}>
      <div className={style.header}>
        <input type="text" onChange={async e => {
          if (e.target.value.trim().length === 0) {
            setSearchList([])
          } else {
            const list = await searchApi(e.target.value.trim())
            setSearchList(list)
          }
        }} />
      </div>
      <ul>
        {searchList.map(item =>
          <li key={item.item_id} onClick={() => {
            navigate(`/detail/${item.item_id}`)
          }}>{item.title}</li>
        )}
      </ul>
    </div>
  )
}

export default Search