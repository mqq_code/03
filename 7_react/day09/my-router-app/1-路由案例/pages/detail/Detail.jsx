import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'

const urls = [
  'https://zyxcl.xyz/exam_api/zh',
  'https://zyxcl.xyz/exam_api/xl',
  'https://zyxcl.xyz/exam_api/sx'
]

const Detail = () => {
  const params = useParams()
  const [info, setInfo] = useState({})

  useEffect(() => {
    Promise.all(urls.map(v => axios.get(v)))
      .then(res => {
        // const allData = res.map(v => v.data.items).flat()
        const allData = res.reduce((prev, val) => {
          return prev.concat(val.data.items)
        }, [])
        const item = allData.find(v => v.item_id === Number(params.id))
        setInfo(item)
      })
  }, [])

  return (
    <div>
      <img src={info.img} width={300} alt="" />
      <p>{JSON.stringify(info)}</p>
    </div>
  )
}

export default Detail