import React, { useEffect, useState, useContext, useMemo } from 'react'
import { useNavigate, useLocation, useParams } from 'react-router-dom'
import style from './Goods.module.scss'
import axios from 'axios'
import classNames from 'classnames'
import storeCtx from '../../../context/storeCtx'

const types = {
  zh: 'https://zyxcl.xyz/exam_api/zh',
  xl: 'https://zyxcl.xyz/exam_api/xl',
  sx: 'https://zyxcl.xyz/exam_api/sx'
}

const Goods = () => {
  const [list, setList] = useState([])
  const navigate = useNavigate()
  const location = useLocation()
  const params = useParams()
  // 接收父级组件传入的数据
  const { isGrid, sortType } = useContext(storeCtx)

  useEffect(() => {
    const url = types[params.type]
    if (url) {
      axios.get(url).then(res => {
        setList(res.data.items)
      })
    } else {
      navigate('/404')
    }
  }, [params])

  const sortList = useMemo(() => {
    if (sortType === 0) return list
    const newList = [...list]
    return newList.sort((a, b) => {
      if (sortType === 1) {
        return a.price - b.price
      } else {
        return b.price - a.price
      }
    })
  }, [list, sortType])

  return (
    <div className={classNames({ [style.grid]: isGrid })}>
      {sortList.map(item =>
        <div key={item.item_id} className={style.item} onClick={() => {
          navigate(`/detail/${item.item_id}`)
        }}>
          <div className={style.imgWrap}>
            <img src={item.img} alt="" />
          </div>
          <div className={style.desc}>
            <h4>{item.title}</h4>
            <p>¥{item.price}</p>
          </div>
        </div>
      )}
    </div>
  )
}

export default Goods