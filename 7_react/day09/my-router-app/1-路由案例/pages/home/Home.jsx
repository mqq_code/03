import { Outlet, NavLink, useNavigate } from 'react-router-dom'
// css module: 模块化样式，此样式不会影响全局样式
import style from './Home.module.scss'
import storeCtx from '../../context/storeCtx'
import React, { useContext } from 'react'


const Home = () => {
  const navigate = useNavigate()
  const { isGrid, setIsGrid, sortType, setSortType } = useContext(storeCtx)

  return (
    <div className={style.home}>
      <div className={style.search}>
        <p onClick={() => navigate('/search')}>搜索</p>
      </div>
      <nav>
        <NavLink to="/home/goods/zh">综合</NavLink>
        <NavLink to="/home/goods/xl">销量</NavLink>
        <NavLink to="/home/goods/sx">上新</NavLink>
        <p onClick={() => {
          const type = sortType + 1
          setSortType(type > 2 ? 0 : type)
        }}>价格 {sortType === 1 ? '升' : (sortType === 2 ? '降' : '')}</p>
        <div className={style.icon} onClick={() => setIsGrid(!isGrid)}>{isGrid ? '网格' : '列表'}</div>
      </nav>
      <main>
        <Outlet />
      </main>
    </div>
  )
}

export default Home