import axios from 'axios'

// 获取影院详情
export const getCinemaInfo = (cinemaId) => {
  return axios.get('https://m.maizuo.com/gateway', {
    params: {
      cinemaId,
      k: 9703511
    },
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"17218918638581224398323713","bc":"110100"}',
      'X-Host': 'mall.film-ticket.cinema.info'
    }
  })
}

// 获取影院的电影列表
export const getCinemaFilms = (cinemaId) => {
  return axios.get('https://m.maizuo.com/gateway', {
    params: {
      cinemaId,
      k: 9703511
    },
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"17218918638581224398323713","bc":"110100"}',
      'X-Host': 'mall.film-ticket.film.cinema-show-film'
    }
  })
}

// 获取影院中的电影列表
export const getSchedule = ({ cinemaId, filmId, date }) => {
  return axios.get('https://m.maizuo.com/gateway', {
    params: {
      filmId,
      cinemaId,
      date,
      k: 8995402
    },
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"17218918638581224398323713","bc":"110100"}',
      'X-Host': 'mall.film-ticket.schedule.list'
    }
  })
}