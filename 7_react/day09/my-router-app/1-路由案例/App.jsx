import { useState } from 'react'
import { useRoutes } from 'react-router-dom'
import routeConfig from './router/config'
import { Provider } from './context/storeCtx'

const App = () => {
  const routes = useRoutes(routeConfig)
  const [isGrid, setIsGrid] = useState(false)
  const [sortType, setSortType] = useState(0) // 0: 默认排序, 1: 升序，2: 降序

  return (
    <Provider value={{
      isGrid,
      setIsGrid,
      sortType,
      setSortType
    }}>
      {routes}
    </Provider>
  )

  // return <div>
  //   <nav className='test'>app</nav>
  //   {routes}
  // </div>
}

export default App