import { useRoutes, NavLink } from 'react-router-dom'
import routeConfig from './router/config'

const App = () => {
  const routes = useRoutes(routeConfig)

  return (
    <div>
      <nav>
        <NavLink to="/home">首页</NavLink>
        <NavLink to="/search">搜索</NavLink>
        <NavLink to="/detail/1223">详情</NavLink>
      </nav>
      <hr />
      {routes}
    </div>
  )
}

export default App