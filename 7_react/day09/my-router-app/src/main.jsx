import { Suspense } from 'react'
import { createRoot } from 'react-dom/client'
import App from './App.jsx'
import './index.scss'
import {
  // 路由根组件： 所有和路由相关的内容必须在子组件内使用
  HashRouter, // hash 模式，url 有 #
  BrowserRouter // history 模式，url 没有 #
} from 'react-router-dom'
import { Provider } from './store/index.jsx'

createRoot(document.getElementById('root')).render(
  // fallback: 加载 Suspense 内部的异步组件时，加载过程中展示的备用内容
  <Suspense fallback={<div style={{ color: 'red', fontSize: 50 }}>加载中。。。</div>}>
    <BrowserRouter>
      <Provider>
        <App />
      </Provider>
    </BrowserRouter>
  </Suspense>
)
