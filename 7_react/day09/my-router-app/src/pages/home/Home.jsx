import React from 'react'
import { useStore } from '../../store'

const Home = () => {
  const { state, dispatch } = useStore()

  return (
    <div>
      <h1>Home</h1>
      <h2>{state.rootTitle}</h2>
      <button onClick={() => {
        dispatch({
          type: 'change_title',
          payload: Math.random()
        })
      }}>修改title</button>
    </div>
  )
}

export default Home