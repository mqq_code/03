import { createContext, useContext, useReducer } from 'react'

const storeCtx = createContext()


const reducer = (state, action) => {
  console.log(action)
  if (action.type === 'change_title') {
    return {...state, rootTitle: action.payload}
  }
  return state
}

const initState = {
  rootTitle: '全局标题',
  count: 10
}

// 封装提供数据的组件
export const Provider = (props) => {
  const [state, dispatch] = useReducer(reducer, initState)
  return (
    <storeCtx.Provider value={{ state, dispatch }}>
      {props.children}
    </storeCtx.Provider>
  )
}

// 封装获取数据的 hook
export const useStore = () => useContext(storeCtx)