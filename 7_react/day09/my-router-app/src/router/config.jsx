import {
  Navigate, // 重定向
} from 'react-router-dom'
import React, { Suspense } from 'react'
import Home from '../pages/home/Home'
const Login = React.lazy(() => import('../pages/login/Login'))
const Detail = React.lazy(() => import('../pages/detail/Detail'))
const Search = React.lazy(() => import('../pages/search/Search'))

const routes = [
  {
    path: '/',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    title: '首页'
  },
  {
    path: '/detail/:id',
    element: <Detail />,
    title: '详情页面',
    isAuth: true
  },
  {
    path: '/search',
    element: <Search />,
    title: '搜索页面'
  },
  {
    path: '/login',
    element: <Login />,
    title: '登录'
  },
  {
    path: '*',
    element: <div>找不到此页面</div>,
    title: '404'
  }
]



export default routes