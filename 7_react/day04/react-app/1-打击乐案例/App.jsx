import React, { useEffect, useRef, useState } from 'react'
import Keyboard from './components/keyboard/Keyboard'
import Control from './components/control/Control'
import axios from 'axios'

const App = () => {
  const [list, setList] = useState([])
  const [power, setPower] = useState(true)
  const [bank, setBank] = useState(false)
  const [title, setTitle] = useState('')
  const [volume, setVolume] = useState(50)

  // 创建播放器对象
  const audioRef = useRef(new Audio())

  useEffect(() => {
    axios.get('https://zyxcl.xyz/exam_api/music/list')
      .then(res => {
        setList(res.data.value)
      })
  }, [])

  const play = (item) => {
    // 修改高亮
    setList(list.map(v => {
      if (item.id === v.id) {
        return {
          ...v,
          isDown: true
        }
      }
      return v
    }))
    // 判断是否应该播放
    if (power) {
      setTitle(item.id)
      audioRef.current.src = bank ? item.bankUrl : item.url
      audioRef.current.autoplay = true
    }
  }

  const keyup = (item) => {
    setList(list.map(v => {
      if (item.id === v.id) {
        return {
          ...v,
          isDown: false
        }
      }
      return v
    }))
  }

  useEffect(() => {
    if (!power) {
      setTitle('')
    }
  }, [power])

  useEffect(() => {
    audioRef.current.volume = volume / 100
  }, [volume])
  
  return (
    <div className='app'>
      <Keyboard
        list={list}
        power={power}
        onKeydown={play}
        onKeyup={keyup}
      />
      <Control
        power={power}
        setPower={setPower}
        bank={bank}
        setBank={setBank}
        title={title}
        volume={volume}
        setVolume={setVolume}
      />
    </div>
  )
}

export default App