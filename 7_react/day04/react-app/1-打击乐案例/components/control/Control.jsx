import React from 'react'
import style from './Control.module.scss'
import Switch from '../switch/Switch'
import Range from '../range/Range'


const Control = (props) => {
  return (
    <div className={style.control}>
      <Switch checked={props.power} onChange={props.setPower}>power</Switch>
      <div className={style.title}>{props.title}</div>
      <Range value={props.volume} onChange={props.setVolume} />
      <Switch checked={props.bank} onChange={props.setBank}>bank</Switch>
    </div>
  )
}

export default Control