import React, { useEffect, useRef, useState } from 'react'
import style from './Range.module.scss'

const Range = (props) => {
  const [left, setLeft] = useState(0)
  const [maxLeft, setMaxLeft] = useState(0)
  const isDown = useRef(false)
  const rangeRef = useRef(null)

  const mouseDown = (e) => {
    isDown.current = true
  }

  useEffect(() => {
    const rangeInfo = rangeRef.current.getBoundingClientRect()
    const maxLeft = rangeInfo.width - 10
    setMaxLeft(maxLeft)
    const mousemove = (e) => {
      if (isDown.current) {
        let left = e.clientX - rangeInfo.left - 5
        if (left < 0) left = 0
        if (left > maxLeft) left = maxLeft
        setLeft(left)
        props.onChange(Math.floor(left / maxLeft * 100))
      }
    }
    const mouseup = () => {
      isDown.current = false
    }
    document.addEventListener('mousemove', mousemove)
    document.addEventListener('mouseup', mouseup)
    return () => {
      document.removeEventListener('mousemove', mousemove)
      document.removeEventListener('mouseup', mouseup)
    }
  }, [])

  useEffect(() => {
    const rangeInfo = rangeRef.current.getBoundingClientRect()
    const maxLeft = rangeInfo.width - 10
    setLeft(props.value / 100 * maxLeft)
  }, [props.value])

  return (
    <div className={style.range} ref={rangeRef}>
      <i style={{ left }} onMouseDown={mouseDown}></i>
      <p style={{ width: left }}>
        <span style={{ width: maxLeft }}></span>
      </p>
    </div>
  )
}

export default Range