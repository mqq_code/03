import React, { useEffect } from 'react'
import style from './Keyboard.module.scss'
import classNames from 'classnames'

const Keyboard = (props) => {


  useEffect(() => {
    const keydown = e => {
      const item = props.list.find(v => v.keyCode === e.keyCode)
      if (item) {
        props.onKeydown(item)
      }
    }
    const keyup = e => {
      const item = props.list.find(v => v.keyCode === e.keyCode)
      if (item) {
        props.onKeyup(item)
      }
    }
    document.addEventListener('keydown', keydown)
    document.addEventListener('keyup', keyup)
    return () => {
      document.removeEventListener('keydown', keydown)
      document.removeEventListener('keyup', keyup)
    }
  }, [props.list])


  return (
    <div className={style.keyboard}>
      {props.list.map(item =>
        <div
          className={classNames(style.key, {
            [style.down]: item.isDown,
            [style.light]: item.isDown && props.power
          })}
          key={item.id}
          onMouseDown={() => props.onKeydown(item)}
          onMouseUp={() => props.onKeyup(item)}
        >{item.keyTrigger}</div>
      )}
    </div>
  )
}

export default Keyboard