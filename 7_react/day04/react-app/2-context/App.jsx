import React, { useState } from 'react'
import Child1 from './components/Child1'
import ctx from './context/ctx'

const App = () => {
  const [count, setCount] = useState(0)
  return (
    // 给所有后代组件传入数据
    <ctx.Provider value={{ count, setCount, a: 100, b: 'aabb' }}>
      <div className='box'>
        <h1>app</h1>
        <button onClick={() => setCount(count + 1)}>+</button>
        <p>count: {count}</p>
        <Child1 />
      </div>
    </ctx.Provider>
  )
}

export default App