import React, { useContext } from 'react'
import ctx from '../context/ctx'
import titleCtx from '../context/titleCtx'

const Child2 = () => {
  // 接收app组件传入的数据
  const { count, setCount } = useContext(ctx)
  const title = useContext(titleCtx)

  console.log(title)
  return (
    <div className='box'>
      <h2>Child2</h2>
      <p>app的变量： {count}</p>
      <button onClick={() => setCount(count + 2)}>+2</button>
    </div>
  )
}

// const Child2 = () => {
//   return (
//     <ctx.Consumer>
//       {value => {
//         console.log('接收父级组件传入的数据', value)
//         return (
//           <div className='box'>
//             <h2>Child2</h2>
//             <p>app的变量： {value.count}</p>
//             <button onClick={() => value.setCount(value.count + 2)}>+2</button>
//           </div>
//         )
//       }}
//     </ctx.Consumer>
//   )
// }

export default Child2