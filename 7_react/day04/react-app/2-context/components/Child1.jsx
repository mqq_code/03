import React from 'react'
import Child2 from './Child2'
import titleCtx from '../context/titleCtx'


const Child1 = () => {
  return (
    <titleCtx.Provider value={[1,2,3,4,5,6]}>
      <div className='box'>
        <h2>Child1</h2>
        <Child2 />
      </div>
    </titleCtx.Provider>
  )
}

export default Child1