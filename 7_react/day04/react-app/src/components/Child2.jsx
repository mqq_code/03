import React, { useState, useRef, useEffect } from 'react'
import { useCount } from '../hooks/useCount'

const Child2 = () => {

  const { count, start } = useCount(3610)

  useEffect(() => {
    start()
  }, [])

  const format = s => {
    const h = Math.floor(s / 3600)
    const m = Math.floor(s / 60 % 60)
    s = s % 60
    return `${h}:${m}:${s}`
  }

  return (
    <div className='box'>
      <h2>Child2</h2>
      <p>限时秒杀：{format(count)}</p>
    </div>
  )
}

export default Child2