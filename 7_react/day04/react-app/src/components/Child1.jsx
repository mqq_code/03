import React, { useState, useRef, useEffect } from 'react'
import { useCount } from '../hooks/useCount'

const Child1 = () => {

  const { count, start, stop, reset } = useCount(20)

  return (
    <div className='box'>
      <h2>Child1</h2>
      <p>倒计时：{count}</p>
      <button onClick={start}>开始</button>
      <button onClick={stop}>停止</button>
      <button onClick={reset}>重置</button>
    </div>
  )
}

export default Child1