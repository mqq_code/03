import React, { useState } from 'react'
import Child2 from './components/Child2'
import Child1 from './components/Child1'

const App = () => {
  const [show, setShow] = useState(true)
  return (
    <div>
      <h1>app</h1>
      <button onClick={() => setShow(!show)}>切换</button>
      {show && <Child1 />}
      <Child2 />
    </div>
  )
}

export default App