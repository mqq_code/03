import { useState, useRef, useEffect } from 'react'


// 自定义hook：函数名必须以 use 开头
// 作用：抽取组件中的公用逻辑方便复用代码
export const useCount = (num = 10) => {

  const [count, setCount] = useState(num)
  const timer = useRef(null)

  const start = () => {
    timer.current = setInterval(() => {
      setCount(c => c - 1)
      console.log('定时器')
    }, 1000)
  }

  const stop = () => {
    clearInterval(timer.current)
  }

  const reset = () => {
    setCount(10)
    stop()
  }

  useEffect(() => {
    if (count <= 0) {
      stop()
    }
  }, [count])

  useEffect(() => {
    return stop
  }, [])

  return {
    count,
    start,
    stop,
    reset
  }
}