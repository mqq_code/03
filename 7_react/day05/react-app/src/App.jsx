import React, { useEffect, useRef, useState } from 'react'
import Search from './components/search/Search'
import DefaultList from './components/defaultList/DefaultList'
import Suggest from './components/suggest/Suggest'
import Result from './components/result/Result'
import axios from 'axios'


const App = () => {
  const [listType, setListType] = useState(0) // 0: 默认列表，1: 搜索建议，2: 搜索结果 
  const [searchValue, setSearchValue] = useState('') // 搜索内容
  const [resultList, setResultList] = useState([]) // 搜索结果
  const [history, setHistory] = useState(JSON.parse(localStorage.getItem('searchHistory')) || []) // 搜索历史
  const [suggestList, setSuggestList] = useState([]) // 搜素建议
  const timer = useRef(null)
  // 添加历史记录
  const addHistory = (val) => {
    const index = history.indexOf(val)
    let list = [...history]
    if (index > -1) {
      list.splice(index, 1)
    }
    list.unshift(val)
    if (list.length > 10) {
      list = list.slice(0, 10)
    }
    setHistory(list) // 存搜索历史
  }

  // 开始搜索，展示搜索结果
  const search = async searchWord => {
    setSearchValue(searchWord) // 给 input 赋值
    setListType(2) // 跳转搜索结果
    addHistory(searchWord) // 存搜索历史
    // 调用搜索接口
    const res = await axios.get('https://zyxcl.xyz/music/api/search', {
      params: {
        keywords: searchWord
      }
    })
    setResultList(res.data.result.songs)
  }
  
  // 调用搜索建议接口，添加防抖，防止连续多次调用接口
  // 防抖：连续调用某个函数只执行最后一次
  const getSuggestList = async keywords => {
    if (timer.current) clearTimeout(timer.current)
    timer.current = setTimeout(async () => {
      const res = await axios.get('https://zyxcl.xyz/music/api/search/suggest', {
        params: {
          keywords,
          type: 'mobile'
        }
      })
      if (res.data.result?.allMatch) {
        setSuggestList(res.data.result.allMatch)
      } else {
        setSuggestList([])
      }
    }, 500)
  }


  // 渲染页面
  const renderList = () => {
    if (listType === 0) {
      return (
        <DefaultList
          history={history}
          onSearch={search}
          onClear={() => setHistory([])}
        />
      )
    } else if (listType === 1) {
      return <Suggest keywords={searchValue} list={suggestList} onSearch={search} />
    } else {
      return <Result list={resultList} />
    }
  }

  useEffect(() => {
    localStorage.setItem('searchHistory', JSON.stringify(history))
  }, [history])

  return (
    <div className='app'>
      <Search
        value={searchValue}
        onChange={value => {
          setSearchValue(value)
          // 展示搜索建议
          setListType(1)
          getSuggestList(value)
        }}
        onSubmit={search}
        onCancel={() => {
          setListType(0)
          setSearchValue('')
          setResultList([])
        }}
      />
      {renderList()}
    </div>
  )
}

export default App