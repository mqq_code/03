import React from 'react'

const style = {
  lineHeight: '40px',
  borderBottom: '1px dashed',
  padding: '0 20px'
}

const Suggest = props => {

  const format = text => {
    const reg = new RegExp(props.keywords, 'ig')

    return text.replace(reg, t => {
      return `<b style="color: red">${t}</b>`
    })
  }

  return (
    <div>
      <h3 style={{ padding: '0 20px' }}>搜索: {props.keywords}</h3>
      {props.list.length === 0 && <p style={{ textAlign: 'center', lineHeight: '40px' }}>没有匹配的搜索建议</p>}
      <ul>
        {props.list.map(item =>
          <li
            style={style}
            key={item.keyword}
            onClick={() => props.onSearch(item.keyword)}
            dangerouslySetInnerHTML={{ __html: format(item.keyword) }}
          >
          </li>
        )}
      </ul>
    </div>
  )
}

export default Suggest
