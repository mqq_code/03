import React from 'react'
import style from './Result.module.scss'

const Result = (props) => {
  return (
    <div className={style.result}>
      <h2>搜索结果</h2>
      {props.list.map(item =>
        <div className={style.song} key={item.id}>
          <h3>{item.name}</h3>
          <p>歌手: {item.artists.map(v => v.name).join('/')}</p>
        </div>
      )}
    </div>
  )
}

export default Result