import React, { useEffect, useRef, useState } from 'react'
import style from './Search.module.scss'

const Search = (props) => {
  const [shpwInp, setShow] = useState(false)
  const inpRef = useRef(null)

  useEffect(() => {
    if (shpwInp) {
      inpRef.current.focus()
    }
  }, [shpwInp])

  useEffect(() => {
    if (props.value.length > 0) {
      setShow(true)
    }
  }, [props.value])

  return (
    <div className={style.search}>
      {shpwInp ?
        <div className={style.inputBar}>
          <input
            ref={inpRef}
            type="text"
            placeholder='请输入要搜索的歌曲/歌手'
            value={props.value}
            onChange={e => {
              props.onChange(e.target.value)
            }}
            onKeyDown={e => {
              if (e.keyCode === 13) {
                props.onSubmit(e.target.value)
              }
            }}
          />
          {props.value.length > 0 &&
            <i onClick={() => props.onChange('')}>x</i>
          }
          <span onClick={() => {
            setShow(false)
            props.onCancel()
          }}>取消</span>
        </div>
      :
        <div className={style.bar} onClick={() => setShow(true)}>请输入要搜索的歌曲/歌手</div>
      }
    </div>
  )
}

export default Search