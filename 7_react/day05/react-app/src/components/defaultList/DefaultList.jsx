import React, { useEffect, useState } from 'react'
import style from './DefaultList.module.scss'
import axios from 'axios'
import classNames from 'classnames'

const DefaultList = (props) => {
  const [hotList, setHotList] = useState([])

  useEffect(() => {
    axios.get('https://zyxcl.xyz/music/api/search/hot/detail')
      .then(res => {
        setHotList(res.data.data)
      })
  }, [])

  return (
    <div>
      {props.history.length > 0 &&
        <>
          <div className={style.title}>
            <span>搜索历史</span>
            <span onClick={props.onClear}>删除</span>
          </div>
          <ul className={style.history}>
            {props.history.map(it =>
              <li key={it}  onClick={() => props.onSearch(it)}>{it}</li>
            )}
          </ul>
        </>
      }

      <div className={style.title}>热门搜索</div>
      <ul className={style.hot}>
        {hotList.map((item, index) =>
          <li key={item.searchWord} onClick={() => props.onSearch(item.searchWord)}>
            <span className={classNames({ [style.red]: index < 3 })}>{index + 1}</span>
            {item.searchWord}
            {item.iconUrl && <img src={item.iconUrl} alt="" />}
          </li>
        )}
      </ul>
    </div>
  )
}

export default DefaultList