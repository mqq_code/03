import React from 'react'
import { useParams, useLocation, useSearchParams } from 'react-router-dom'

const Detail = () => {
  const location = useLocation()
  const params = useParams()
  const [searchParams] = useSearchParams()

  console.log(params.id)
  console.log(location.search)

  const a = searchParams.get('a')
  console.log(a)

  return (
    <div>Detail</div>
  )
}

export default Detail