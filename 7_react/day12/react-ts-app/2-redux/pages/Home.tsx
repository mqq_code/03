import { NavLink, useNavigate } from 'react-router-dom'
import { changeName, getBanner } from '../store/reducers/user'
import { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../hooks/store'

const Home = () => {
  const username = useAppSelector(state => state.user.username)
  const cartList = useAppSelector(state => state.cart.cartList)
  const banners = useAppSelector(state => state.user.banners)

  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const goDetail = () => {
    navigate('/detail/123?a=100&b=200')
  }

  useEffect(() => {
    dispatch(getBanner())
  }, [])


  return (
    <div>
      <h2>Home</h2>
      <h3>{username}</h3>
      <button onClick={() => {
        dispatch(changeName(Math.random() + ''))
      }}>修改姓名</button>
      <hr />
      {JSON.stringify(banners)}
      <hr />
      <NavLink to="/about">关于我们</NavLink>
      <button onClick={goDetail}>跳转详情页面</button>
    </div>
  )
}

export default Home