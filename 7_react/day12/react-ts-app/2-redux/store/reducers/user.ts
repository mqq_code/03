import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { getBannerApi, type Banner } from '../../services/api'


export const getBanner = createAsyncThunk('getBanner', async () => {
  const res = await getBannerApi()
  return res.data.banners
})


// 定义state类型
interface State {
  username: string;
  age: number;
  banners: Banner[];
}

// 定义初始数据
const initialState: State = {
  username: '小明',
  age: 222,
  banners: []
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    changeName(state, action: PayloadAction<string>) {
      state.username = action.payload
    }
  },
  extraReducers: builder => {
    builder
      .addCase(getBanner.fulfilled, (state, action) => {
        state.banners = action.payload
      })
  }
})

export const { changeName } = userSlice.actions

export default userSlice.reducer
