import { createSlice } from '@reduxjs/toolkit'


interface CartItem {
  name: string;
  count: number;
}

const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    cartList: [] as CartItem[],
    goodsList: []
  },
  reducers: {
    
  }
})

export default cartSlice.reducer
