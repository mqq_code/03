import { configureStore } from '@reduxjs/toolkit'
import user from './reducers/user'
import cart from './reducers/cart'

const store = configureStore({
  reducer: {
    user,
    cart
  }
})


// // 从 store 本身推断出 `RootState` 和 `AppDispatch` 类型
export type RootState = ReturnType<typeof store.getState>
// // 推断出类型: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export default store


// const obj = {
//   name: '洗哦啊明',
//   age: 22,
//   say: (text: string) => {
//     return 111//`我是小明, ${text}`
//   }
// }

// type IObj = typeof obj
// type Sayfn = typeof obj.say
// type a = ReturnType<typeof obj.say>





