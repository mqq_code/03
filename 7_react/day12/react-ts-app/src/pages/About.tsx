import React, { useEffect, useState } from 'react'
import { Space, Table, Tag, Button, Image, Input, Drawer } from 'antd'
import type { TableProps } from 'antd'
import { getSongsApi, searchApi, lyricApi } from '../services/api'
import type { Song, SongItem } from '../services/api'

type LyricItem = {
  time: string;
  text: string;
  timeStr: string;
}

const About = () => {
  const [open, setOpen] = useState(false);
  const [lyric, setLyric] = useState<LyricItem[]>([]);
  const [keywords, setKeywords] = useState('张杰')
  const [query, setQuery] = useState({
    limit: 5,
    offset: 0
  })
  const [total, setTotal] = useState(0)
  const [data, setData] = useState<SongItem[]>([])

  const parseSubtitle = (subtitle: string) => {
    // 正则表达式匹配时间戳和文本
    const regex = /^\[(\d{2}:\d{2}.\d{3})\](.*)$/;
    const match = subtitle.match(regex);
    if (match && match[2]) {
      // 将时间戳转换成 '分钟m秒s' 的格式
      console.log(match[1])
      const timeParts = match[1].split(':');
      const seconds = parseInt(timeParts[0]) * 60 + parseInt(timeParts[1].split('.')[0]);
      const time = `${seconds}s`;
  

      const s = Math.round(parseInt(timeParts[1]))
      // 返回对象
      return {
        time: time,
        text: match[2],
        timeStr: timeParts[0] + ':' + (s < 10 ? '0' + s : s)
      };
    } else {
      // 如果没有匹配到，返回null或者抛出错误
      return null;
    }
  }
  // 列的数据
  const columns: TableProps<SongItem>['columns'] = [
    {
      title: '序号',
      key: 'no',
      render: (_, record, index) => {
        return index + 1
      }
    },
    {
      title: '歌名',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: '歌手',
      key: 'address',
      render: (_, record) => {
        return (
          <Space>
            {record.artists.map(v =>
              <Tag key={v.id} color={'#' + Math.random().toString(16).slice(2, 8)}>{v.name}</Tag>
            )}
          </Space>
        )
      }
    },
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '专辑',
      render: (_, record) => record.album.name
    },
    {
      title: '操作',
      render: (_, record) => {
        return (
          <Space>
            <Button size="small" type="primary" onClick={async () => {
              setOpen(true)
              const res = await lyricApi(record.id)
              const lyricArr = res.data.lrc.lyric.split('\n').map(parseSubtitle).filter(v => v !== null)
              setLyric(lyricArr)
            }}>查看歌词</Button>
          </Space>
        )
      }
    }
  ];

  // useEffect(() => {
  //   getSongsApi(19723756)
  //     .then(res => {
  //       setData(res.data.playlist.tracks)
  //     })
  // }, [])

  const search = async () => {
    const res = await searchApi({
      keywords,
      limit: query.limit,
      offset: query.offset
    })
    console.log(res.data.result)
    setData(res.data.result.songs)
    setTotal(res.data.result.songCount)
  }

  useEffect(() => {
    if (keywords) {
      search()
    }
  }, [query])

console.log(lyric)
  return (
    <div>
      <Space>
        <Input type='text' placeholder='请输入搜索的歌曲' value={keywords} onChange={e =>  setKeywords(e.target.value)} />
        <Button type="primary" onClick={search}>搜索</Button>
      </Space>
      
      <Table
        style={{ marginTop: 20 }}
        bordered
        columns={columns}
        dataSource={data}
        rowKey="id"
        pagination={{
          current: query.offset / query.limit + 1,
          pageSize: query.limit,
          total,
          pageSizeOptions: [5, 10, 15],
          onChange: (page, pageSize) => {
            setQuery({
              offset: (page - 1) * pageSize,
              limit: pageSize
            })
          }
        }}
      />
      <Drawer title="歌词" width={500} onClose={() => setOpen(false)} open={open}>
        {lyric.map(item =>
          <p className='lyric' key={item.time} style={{ display: 'flex', justifyContent: 'space-between' }}>
            {item.text}
            <span>{item.timeStr}</span>
          </p>
        )}
      </Drawer>
    </div>
  )
}

export default About


// const str = '[00:10]歌词1111'
// const reg = /^\[(.*)\](.*)/
// console.log(reg.test(str))
// console.log(str.match(reg))
// console.log(reg.exec(str))
