import React from 'react'
import { Space, Table, Tag, Button } from 'antd'
import type { TableProps } from 'antd'



interface Person {
  name: string;
  age: number;
  sex: 0 | 1;
  address: string;
}
// 列的数据
const columns: TableProps<Person>['columns'] = [
  {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
    render: (_, record) => {
      return <b style={{ color: 'red' }}>{record.name}</b>
    }
  },
  {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '地址',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: '性别',
    dataIndex: 'sex',
    key: 'sex',
    render: (_, record) => {
      return <b>{record.sex === 1 ? '男' : '女'}</b>
    }
  },
  {
    title: '操作',
    render: (_, record) => {
      return (
        <Space>
          <Button size="small" type="primary">编辑</Button>
          <Button size="small" danger>删除</Button>
        </Space>
      )
    }
  }
];

const data: Person[] = [
  {
    name: '王小明',
    age: 32,
    sex: 0,
    address: '北京',
  },
  {
    name: '李达',
    age: 42,
    address: '上海',
    sex: 1
  },
  {
    name: '章三',
    age: 32,
    address: '广州',
    sex: 1
  },
];

const Home = () => {
  return (
    <div>
      <h1>home</h1>
      <Table columns={columns} dataSource={data} rowKey={record => record.name} />
    </div>
  )
}

export default Home