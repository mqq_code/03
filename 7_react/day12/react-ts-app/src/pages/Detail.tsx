import React from 'react'
import type { FormProps } from 'antd';
import { Button, Checkbox, Form, Input, Space } from 'antd';



type FieldType = {
  user?: string;
  pwd?: string;
  remember?: string;
};

const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
  console.log('Success:', values);
};

const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
  console.log('Failed:', errorInfo);
};

const Detail = () => {

  // 获取 form 组件实例
  const [form] = Form.useForm()


  return (
    <div>
      <Form
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        initialValues={{ user: '王小明', pwd: 11111, remember: true }}
        autoComplete="off"
        form={form}
      >
        <Form.Item<FieldType>
          label="用户名"
          name="user"
          rules={[{ required: true, message: '请输入用户名!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item<FieldType>
          label="密码"
          name="pwd"
          rules={[{ required: true, message: '请输入密码!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item<FieldType>
          name="remember"
          valuePropName="checked"
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        {/* <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item> */}
      </Form>
      <hr />
      <Space>
        <Button type="primary" onClick={() => {

          // 触发提交事件
          // form.submit()

          // 触发验证
          form.validateFields()
            .then(value => {
              console.log(value)
            })
        }}>提交</Button>
        <Button onClick={() => {
          form.resetFields()
        }}>重置</Button>

        <Button danger onClick={() => {

          
          // form.setFieldsValue({
          //   user: '',
          //   pwd: '',
          //   remember: true
          // })

          form.resetFields(['pwd'])
          form.setFieldValue('pwd', '')

          // form.setFields([{
          //   name: 'pwd',
          //   validating: false,
          //   value: ''
          // }])
        }}>清空</Button>
      </Space>
    </div>
  )
}

export default Detail