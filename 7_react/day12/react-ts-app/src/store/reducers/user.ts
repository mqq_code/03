import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'

// 定义state类型
interface State {
}

// 定义初始数据
const initialState: State = {
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
  },
  extraReducers: builder => {

  }
})

export const {} = userSlice.actions

export default userSlice.reducer
