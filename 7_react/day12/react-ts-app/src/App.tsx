import React from 'react'
import { useRoutes, Navigate } from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'
import Detail from './pages/Detail'

const App: React.FC = () => {
  const routes = useRoutes([
    {
      path: '/',
      element: <Home />
    },
    {
      path: '/detail',
      element: <Detail />
    },
    {
      path: '/about',
      element: <About />
    },
    {
      path: '*',
      element: <Navigate to="/" />
    }
  ])
  return routes
}

export default App