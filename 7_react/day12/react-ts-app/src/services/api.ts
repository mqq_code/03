import axios from 'axios'


export interface Banner {
  targetId: number
  imageUrl: string
}

interface BannersResponse {
  banners: Banner[]
  code: number
}

export const getBannerApi = () => {
  return axios.get<BannersResponse>('https://zyxcl.xyz/music/api/banner')
}



export interface Song {
  name: string;
  id: number;
  ar: { id: number; name: string; }[];
  al: {
    name: string;
    id: number;
    picUrl: string;
  }
}

interface SongsResponse {
  code: number;
  playlist: {
    id: number;
    name: string;
    description: string;
    coverImgUrl: string;
    creator: {
      avatarUrl: string;
      nickname: string;
    }
    tracks: Song[];
  }
}


export const getSongsApi = (id: number) => {
  return axios.get<SongsResponse>('https://zyxcl.xyz/music/api/playlist/detail', {
    params: {
      id
    }
  })
}


export interface SongItem {
  name: string;
  id: number;
  artists: { id: number; name: string; img1v1Url: string; }[];
  album: {
    name: string;
    id: number;
    picUrl: string;
  }
}
interface SearchResponse {
  code: number;
  result: {
    hasMore: boolean;
    songCount: number;
    songs: SongItem[]
  }
}
interface SearchParams {
  keywords: string
  limit: number
  offset: number
}
export const searchApi = (params: SearchParams) => {
  return axios.get<SearchResponse>('https://zyxcl.xyz/music/api/search', {
    params
  })
}



interface LyricResponse {
  code: number;
  lrc: {
    lyric: string;
  }
}
export const lyricApi = (id: number) => {
  return axios.post<LyricResponse>('https://zyxcl.xyz/music/api/lyric', { id })
}
