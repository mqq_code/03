import React from 'react'


interface Props {
  title: string;
  count: number;
  a?: boolean;
  children?: React.ReactNode;
  changeCount: (n : number) => void;
}

const Child: React.FC<Props> = (props) => {
  return (
    <div className='box'>
      <h2>Child</h2>
      <div>title： {props.title}</div>
      <div>count: {props.count} <button onClick={() => props.changeCount(-2)}>-2</button></div>
      {props.children}
    </div>
  )
}

export default Child