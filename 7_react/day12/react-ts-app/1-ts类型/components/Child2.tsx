import React, { Component } from 'react'

interface Props {
  title: string;
  count: number;
  a?: boolean;
  changeCount: (n: number) => void;
  children: React.ReactNode;
}

interface Item {
  id: number;
  text: string;
}
interface State {
  arr: Item[]
}

class Child2 extends Component<Props, State> {

  state = {
    arr: [] as Item[]
  }

  add = () => {
    this.setState({
      arr: [...this.state.arr, { id: Date.now(), text: Math.random() + '' }]
    })
  }

  render() {
    return (
      <div className='box'>
        <h2>Child2</h2>
        <button onClick={this.add}>添加数据</button>
        <ul>
          {this.state.arr.map(item => 
            <li key={item.id}>{item.text}</li>
          )}
        </ul>
        <hr />
        <p>title: {this.props.title}</p>
        <p>
          count: {this.props.count}
          <button onClick={() => this.props.changeCount(-3)}>-3</button>
        </p>
        {this.props.children}
      </div>
    )
  }
}

export default Child2