import React, { useEffect, useRef, useState } from 'react'
import { getBannerApi, type Banner } from './services/api'
// import type { Banner } from './services/api'
import Child from './components/Child'
import Child2 from './components/Child2'

const App: React.FC = () => {

  const [count, setCount] = useState(0)
  const [banners, setBanners] = useState<Banner[]>([])
  const [title, setTitle] = useState('页面标题')
  const titleRef = useRef<HTMLHeadingElement>(null)

  useEffect(() => {
    getBannerApi()
      .then(res => {
        setBanners(res.data.banners)
      })
  }, [])

  const changeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value)
  }

  useEffect(() => {
    console.log(titleRef.current?.innerHTML)
  }, [])


  return (
    <div>
      <h1 ref={titleRef}>{title}</h1>
      <input type="text" value={title} onChange={changeTitle} />
      <div>count: {count} <button onClick={() => setCount(count + 1)}>+</button></div>
      {/* <Child title={title} count={count} a={true} changeCount={n => setCount(count + n)}>
        <ul>
          {banners.map(item =>
            <li key={item.imageUrl}>
              <img src={item.imageUrl} width={300} alt="" />
            </li>
          )}
        </ul>
        <p>aaaa</p>
      </Child> */}

      <Child2 title={title} count={count} a={true} changeCount={n => setCount(count + n)}>
        <ul>
          {banners.map(item =>
            <li key={item.imageUrl}>
              <img src={item.imageUrl} width={300} alt="" />
            </li>
          )}
        </ul>
        <p>aaaa</p>
      </Child2>

    </div>
  )
}

export default App