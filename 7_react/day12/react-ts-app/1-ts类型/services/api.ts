import axios from 'axios'


export interface Banner {
  targetId: number
  imageUrl: string
}

interface BannersResponse {
  banners: Banner[]
  code: number
}

export const getBannerApi = () => {
  return axios.get<BannersResponse>('https://zyxcl.xyz/music/api/banner')
}