import { Component, createRef } from 'react'

class App extends Component {
  state = {
    title: 'App组件的标题'
  }
  // 创建ref对象
  inpRef = createRef()
  titleRef = createRef()

  render() {
    const { title } = this.state
    return (
      <div className='app'>
        <h2 ref={this.titleRef} style={{ color: 'red' }}>{title}</h2>
        {/* 受控组件：组件的状态受到state控制，必须定义onChange事件，在onchange中修改state状态 */}
        <input type="text" value={title} onChange={e => {
          this.setState({
            title: e.target.value
          })
        }} />
        <button onClick={() => {
          this.setState({ title: 'App组件的标题' })
        }}>还原标题</button>
        <hr />
        {/* 非受控组件 */}
        <input type="text" ref={this.inpRef} defaultValue={123} />
        <input type="checkbox" defaultChecked={true} />
        <button onClick={() => {
          console.log(this.inpRef.current.value)
          console.log(this.titleRef.current)
        }}>获取input数据</button>
      </div>
    )
  }
}

export default App
