import { Component } from 'react'

class App extends Component {
  state = {
    form: {
      name: '小明',
      age: 22,
      sex: "0",
      address: 'sh',
      married: true
    }
  }

  changeForm = (key, val) => {
    const newForm = {...this.state.form}
    newForm[key] = val
    this.setState({
      form: newForm
    })
  }

  render() {
    const { form } = this.state
    return (
      <div className='app'>
        <p>姓名：<input type="text" value={form.name} onChange={e => this.changeForm('name', e.target.value)} /></p>
        <p>年龄：<input type="number" value={form.age} onChange={e => this.changeForm('age', e.target.value)} /></p>
        <p>性别：
          <input type="radio" name="sex" value="1" checked={form.sex === '1'} onChange={e => this.changeForm('sex', e.target.value)} /> 男
          <input type="radio" name="sex" value="0" checked={form.sex === '0'} onChange={e => this.changeForm('sex', e.target.value)} /> 女
        </p>
        <p>地址：
          <select value={form.address} onChange={e => this.changeForm('address', e.target.value)}>
            <option value="bj">北京</option>
            <option value="sh">上海</option>
            <option value="gz">广州</option>
          </select>
        </p>
        <p>已婚：<input type="checkbox" checked={form.married} onChange={e => this.changeForm('married', e.target.checked)} /></p>
        <button>提交</button>
        {JSON.stringify(form)}
      </div>
    )
  }
}

export default App
