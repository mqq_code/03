import { createRoot } from 'react-dom/client' // react-dom 核心语法，创建根节点，渲染元素
import React from'react' // react 核心语法，创建元素、创建组件
import './index.css'

// 创建 react 元素
// const title = React.createElement('h1', { className: 'title' }, [
//   '开始学习',
//   React.createElement('b', {}, 'React!')
// ])

// jsx: js + xml
// jsx 语法本质上 React.createElement 的语法糖，编译时会通过 babel 转译成 React.createElement
// 注意：class => className, for => htnlFor
const title = <h1 className='title'>开始学习 <b>React!</b></h1>
console.log(title)

const boxStyle = { color: '#fff' }
const box = <div className='box' style={boxStyle}>
  <label htmlFor="a">已婚</label>
  <input type="checkbox" id="a" />
</div>



// jsx 中渲染变量，使用 打括号包裹
const titleText = '我是标题'
const box1 = <div>
  <h2>{titleText}</h2>
  <p>字符串: {'aaaaa'}</p>
  <p>数字: {1111}</p>
  <p>布尔值: {false}</p>
  <p>null: {null}</p>
  <p>undefined: {undefined}</p>
  <p>数组: {[1,2,3,4,5,6]}</p>
  {/* 不能渲染变量，会导致页面报错 */}
  {/* <p>对象: {  { name: '小明' }   }</p> */}
</div>




createRoot(document.getElementById('root')).render(box)
