import { Component } from 'react'



// 组件方式一：类组件
class App extends Component {
  state = {
    title: 'App组件的标题',
    arr: [1, 2, 3, 4, 5, 6]
  }

  pushArr = () => {
    const newArr = [...this.state.arr]
    newArr.push(newArr.length + 1)
    this.setState({
      // arr: [...this.state.arr, this.state.arr.length + 1]
      arr: newArr
    })
  }

  render() {
    const OddArr = this.state.arr.filter(v => v % 2 === 1)
    return (
      <div className='app'>
        <h2 style={{ color: 'red' }}>{this.state.title}</h2>
        <button onClick={this.pushArr}>push</button>
        <ul>
          {this.state.arr.map(it => {
            return <li key={it}>{it}</li>
          })}
        </ul>
      </div>
    )
  }
}

export default App


// import React from 'react'
// // 组件方式二：函数组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App






// class Person {
//   name = 1000

//   say() {
//     console.log(this.name)
//   }
// }