import { Component } from 'react'


// 组件方式一：类组件
class App extends Component {
  // 定义组件的状态，类似 vue2 中的data
  state = {
    title: 'App组件的标题',
    num: 10
  }

  addNum = (e) => {
    // console.log(e)
    // 调用 this.setState 触发组件更新
    // 把传入的对象和当前组件的 state 进行合并
    this.setState({
      num: this.state.num + 1
    })
  }

  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  renderText = n => {
    if (n < 10) {
      return <div>分数：{n}, 考的稀碎</div>
    } else if (n >= 10 && n < 20) {
      return <b style={{ color: 'red' }}>分数：{n}, 考的一般</b>
    } else {
      return <h3 style={{ color: 'green' }}>分数：{n}, 你真棒</h3>
    }
  }

  // class 组件中必须有一个 render 函数返回要渲染的虚拟 dom
  // render 函数会自动执行
  render() {
    console.log('渲染App', this.state)
    return (
      <div className='app'>
        <h2 style={{ color: 'red' }}>{this.state.title}</h2>
        <button onClick={() => {
          // 调用函数传参数必须套一层箭头函数
          this.changeNum(-1)
        }}>-</button>
        <p>num: {this.state.num}</p>
        <button onClick={this.addNum}>+</button>
        <hr />
        <h3>条件渲染</h3>
        {/* <p>{this.state.num > 10 ? '及格' : '不及格'}</p> */}
        {/* <div>
          {this.state.num > 10 ?
            <p style={{ color: 'red' }}>{this.state.num}分：及格</p>
          :
            <i>不及格</i>
          }
        </div> */}
        {/* <div>
          {this.state.num > 10 ?
            <p style={{ color: 'red' }}>{this.state.num}分：及格</p>
          : null}
        </div>
        <div>
          {this.state.num > 10 && <p style={{ color: 'red' }}>{this.state.num}分：及格</p>}
        </div> */}
        {this.renderText(this.state.num)}
      </div>
    )
  }
}

export default App


// import React from 'react'
// // 组件方式二：函数组件
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }

// export default App






// class Person {
//   name = 1000

//   say() {
//     console.log(this.name)
//   }
// }