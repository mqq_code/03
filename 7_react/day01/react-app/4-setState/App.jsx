import { Component } from 'react'

class App extends Component {
  state = {
    title: 'App组件的标题',
    count: 0
  }
  
  add = () => {
    // setState 是异步更新数据
    // this.setState({
    //   count: this.state.count + 1
    // }, () => {
    //   // 等待页面更新后执行，可以获取到最新的dom元素
    //   console.log(document.querySelector('b').outerHTML)
    // })

    // 同时调用多次 setState 会合并成一次更新，只渲染一次
    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1, title: '123' })
    // this.setState({ count: this.state.count + 1 })


    this.setState({ count: this.state.count + 1 })
    // 想要确保本次修改数据时能够获取到最新的数据，可以给 setState 传入一个函数
    // 函数的参数就是最新的 state，函数中 return 的数据会和当前的state进行合并
    this.setState(state => {
      console.log('最新的数据', state)
      return {
        count: state.count + 1
      }
    })
    this.setState(state => ({ count: state.count + 1 }))


    setTimeout(() => {
      this.setState({ count: this.state.count + 1 })
    })
  }

  render() {
    const { title, count } = this.state
    console.log('渲染组件', count)
    return (
      <div className='app'>
        <h2 style={{ color: 'red' }}>{title}</h2>
        <button>-</button>
        <b>{count}</b>
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}

export default App
