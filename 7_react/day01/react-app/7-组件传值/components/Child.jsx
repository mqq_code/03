import { Component } from 'react'

class Child extends Component {

  render() {
    console.log('接收父组件传入的参数', this.props)
    return (
      <div className='child'>
        <h2>Child</h2>
        {this.props.top}

        <p>父组件的count：{this.props.count}</p>
        <p>父组件的a: {this.props.a}</p>
        <button onClick={() => this.props.change(2)}>修改父组件的count</button>

        <div className="child">
          {/* 组件调用时双标签的内容 */}
          {this.props.children}
        </div>

        {this.props.bottom}

      </div>
    )
  }
}

export default Child




// const Child = (props) => {

//   return (
//     <div className='child'>
//       <h2>Child</h2>
//       {props.top}

//       <p>父组件的count：{props.count}</p>
//       <p>父组件的a: {props.a}</p>
//       <button onClick={() => props.change(2)}>修改父组件的count</button>

//       <div className="child">
//         {/* 组件调用时双标签的内容 */}
//         {props.children}
//       </div>

//       {props.bottom}

//     </div>
//   )
// }

// export default Child
