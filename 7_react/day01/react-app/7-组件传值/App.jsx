import { Component } from 'react'
import Child from './components/Child'

class App extends Component {
  state = {
    count: 0
  }
  changeCount = n => {
    this.setState({ count: this.state.count + n })
  }

  clickTop = () => {
    alert('点击了top')
  }

  render() {
    return (
      <div className='app'>
        <h1>app</h1>
        <button onClick={() => this.changeCount(-1)}>-</button>
        {this.state.count}
        <button onClick={() => this.changeCount(1)}>+</button>
        <Child
          count={this.state.count}
          a="aaaaaaaaa"
          change={this.changeCount}
          top={<div onClick={this.clickTop} style={{ background: 'red' }}>我是一个传入的元素</div>}
          bottom={<b>我是一个b标签</b>}
        >
          <ul>
            <li>111</li>
            <li>2222</li>
            <li>3333</li>
          </ul>
          <input type="text" />
        </Child>
      </div>
    )
  }
}

export default App
