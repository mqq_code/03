import { Component } from 'react'

class App extends Component {

  state = {
    count: 0
  }

  add(e) {
    console.log(this)
    console.log('合成事件对象', e)
    // console.log('原生事件的事件对象', e.nativeEvent)
  }

  add1 = (a) => {
    console.log(a)
    console.log(this)
  }

  componentDidMount() {
    document.querySelector('h1').addEventListener('click', function (e) {
      // console.log(this)
      console.log(e)
    })
  }

  render() {
    return (
      <div className='app'>
        <h1>app</h1>
        {this.state.count}
        {/* 合成事件: 
          1. React 自己实现了一套事件机制，统一了不同浏览器之间事件系统的差异。
          2. React 组件上声明的事件最终绑定到了 document 这个 DOM 节点上，而不是 React 组件对应的 DOM 节点。只有document这个节点上面才绑定了DOM原生事件，其他节点没有绑定事件。这样简化了DOM原生事件，减少了内存开销。
        */}
        <button onClick={this.add}>+</button>
        {/* <button onClick={this.add.bind(this)}>+</button>
        <button onClick={this.add1}>+</button>
        <button onClick={() => this.add()}>+</button>
        <button onClick={() => this.add1(1)}>+</button> */}

        <p onClick={() => {}}>pppp</p>
      </div>
    )
  }
}

export default App





// const eventsList = [
//   {
//     element: button,
//     clickFn: this.add
//   },
//   {
//     element: p,
//     clickFn: fn
//   }
// ]


// document.addEventListener('click', e => {
//   const fn = eventsList.find(v => v.element === e.target).clickFn

//   fn()
// })
