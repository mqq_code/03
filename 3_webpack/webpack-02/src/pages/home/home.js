import '../../font/iconfont.css'
import '../../scss/style.scss'
import img1 from '../../assets/img2.jpg'

import Swiper from 'swiper'
import { Navigation, Pagination } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

const swiper = new Swiper('.swiper', {
  loop: true,
  modules: [Navigation, Pagination],
  pagination: {
    el: '.swiper-pagination',
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
})

console.log(img1)

const createImg = url => {
  const image = new Image()
  image.src = url
  image.onload = () => {
    document.body.appendChild(image)
    console.log(30, image)
  }
}

createImg(img1)


