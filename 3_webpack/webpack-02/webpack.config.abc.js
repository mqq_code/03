const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

// 配置 webpack
module.exports = {
  // 打包模式 生产模式: production， 开发模式: development
  mode: 'development',
  // 生成源代码和打包后代码的映射关系
  devtool: 'source-map',
  // 入口文件
  // entry: './src/home.js',
  // 数组多入口: 合并多个js输出到一个文件中
  // entry: [
  //   './src/pages/home/home.js',
  //   './src/pages/detail/detail.js',
  //   './src/pages/player/player.js'
  // ],
  // 对象方式多入口
  entry: {
    home: './src/pages/home/home.js',
    detail: './src/pages/detail/detail.js',
    player: './src/pages/player/player.js'
  },
  // 出口文件
  output: {
    // 输出目录，必须是绝对路径
    path:  path.join(__dirname, 'build'),
    // 指定输出的文件名
    filename: 'js/[name]-[hash:8].js',
    // 每次打包时清空上一次打包的内容
    clean: true,
    // 设置输出的图片名称
    assetModuleFilename: 'assets/[name]-[hash:8][ext]'
  },
  // 配置 loader 加载器，让 js 可以解析其他类型的文件
  module: {
    rules: [
      {
        // 配置如果js中引入了 css或者scss文件，如何解析
        test: /\.(css|scss)$/,
        // yarn add -D style-loader css-loader sass-loader sass
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|webp|svg|gif|ttf)$/,
        // 根据图片大小判断是否需要转成base64，默认小于 8kb 转成 base64
        type: 'asset',
        parser: {
          // 设置大小
          dataUrlCondition: {
            maxSize: 8 * 1024 // 15kb
          }
        }
      },
      {
        // 解析 html 内使用的图片
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.js$/,
        // 使用 babel 转译js语法时忽略 node_modules文件夹
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  },
  // 配置插件扩展webpack功能
  plugins: [
    // 自动生成 html，把生成的 js 自动引入到页面中
    new HtmlWebpackPlugin({
      template: './src/pages/home/index.html',
      filename: 'index.html',
      chunks: ['home']
    }),
    new HtmlWebpackPlugin({
      template: './src/pages/detail/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new HtmlWebpackPlugin({
      template: './src/pages/player/player.html',
      filename: 'player.html',
      chunks: ['player']
    }),
    // 抽离css文件
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    })
  ],
  // 配置devserver
  devServer: {
    port: 9004,
    hot: true, // 热更新
    // 添加请求代理，处理跨域
    proxy: [
      {
        // /zyx/sell/api/seller => http://ustbhuangyi.com/zyx/sell/api/seller
        context: ['/zyx'],
        target: 'http://ustbhuangyi.com',
        // 重写路径 /zyx/sell/api/seller => http://ustbhuangyi.com/sell/api/seller
        pathRewrite: { '^/zyx': '' },
        // 发送请求时，修改 host
        changeOrigin: true,
      }
    ]
  },
  resolve: {
    // 配置路径别名
    alias: {
      Utils: path.resolve(__dirname, 'src/utils/'),
      '@': path.resolve(__dirname, 'src/'),
    },
  },
}