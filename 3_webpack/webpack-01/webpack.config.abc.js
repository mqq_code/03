const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');

// 配置 webpack
module.exports = {
  // 打包模式 生产模式: production， 开发模式：development
  mode: 'production',
  // 入口文件
  entry: './src/home.js',
  // 出口文件
  output: {
    // 输出目录，必须是绝对路径
    path:  path.join(__dirname, 'build'),
    // 指定输出的文件名
    filename: 'js/main-[hash:8].js',
    // 每次打包时清空上一次打包的内容
    clean: true,
    // 设置输出的图片名称
    assetModuleFilename: 'assets/[name]-[hash:8][ext]'
  },
  // 配置 loader 加载器，让 js 可以解析其他类型的文件
  module: {
    rules: [
      {
        // 配置如果js中引入了 css或者scss文件，如何解析
        test: /\.(css|scss)$/,
        // yarn add -D style-loader css-loader sass-loader sass
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|webp|svg|gif|ttf)$/,
        // 根据图片大小判断是否需要转成base64，默认小于 8kb 转成 base64
        type: 'asset',
        parser: {
          // 设置大小
          dataUrlCondition: {
            maxSize: 8 * 1024 // 15kb
          }
        }
      },
      {
        // 解析 html 内使用的图片
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.js$/,
        // 使用 babel 转译js语法时忽略 node_modules文件夹
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  },
  // 配置插件扩展webpack功能
  plugins: [
    // 自动生成 html，把生成的 js 自动引入到页面中
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    })
  ],
  // 配置devserver
  devServer: {
    port: 9004
  }
}