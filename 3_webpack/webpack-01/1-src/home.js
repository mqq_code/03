// commonJS 规范
// 1. 引入 require
// 2. 抛出 module.exports

// esmodule 规范
// 1. 引入 import 
// 2. 抛出 export
import axios from 'axios'
import './scss/style.scss'
import num, { aaa, bbb, format as fn1 } from './utils'
import * as all from './utils'

console.log(all)
console.log(num)
console.log('aaa', aaa)
console.log('bbb', bbb)
console.log(fn1)