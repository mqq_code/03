// 单独抛出，export 后边必须是定义变量
export const format = () => {
  return new Date().toLocaleString()
}

export const aaa = 12345
export const bbb = 3457788


// 默认抛出
export default 10000
// export default {
//   format,
//   aaa,
//   bbb
// }