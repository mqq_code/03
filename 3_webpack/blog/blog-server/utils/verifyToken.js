const jwt = require('jsonwebtoken')

const privateKey = 'zyx_bw_blog'


function verifyToken(req, res, next) {
  console.log('有人请求了', req.url)
  try {
    // 接收token, 解析token
    const token = req.headers.authorization
    console.log(token)
    const tokenUser = jwt.verify(token, privateKey)
    console.log(tokenUser)
    // 把解析出来的token信息存到req中
    req.tokenUser = tokenUser
    next()
  } catch(e) {
    // 解析token失败直接通知前端重新登录
    res.statusCode = 401
    res.send({
      code: 401,
      msg: '用户信息失效，请重新登录'
    })
  }
}


module.exports = {
  verifyToken,
  privateKey
}