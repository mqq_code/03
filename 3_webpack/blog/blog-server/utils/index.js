const path = require('path')
const fs = require('fs')

// 封装读写文件工具函数
const readFile = (filePath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, filePath), 'utf-8', (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })
}
const writeFile = (filePath, fileContent) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, filePath), JSON.stringify(fileContent), (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

// 用户列表读写
const readUser = () => readFile('../db/userlist.json')
const writeUser = userlist => writeFile('../db/userlist.json', userlist)

// 文章列表读写
const readArticle = () => readFile('../db/articlelist.json')
const writeArticle = articlelist => writeFile('../db/articlelist.json', articlelist)


module.exports = {
  readFile,
  writeFile,
  readUser,
  writeUser,
  readArticle,
  writeArticle
}