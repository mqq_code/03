const express = require('express')
const ip = require('ip')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const {
  readUser,
  writeUser,
  readArticle,
  writeArticle
} = require('./utils')

const {
  privateKey,
  verifyToken
} = require('./utils/verifyToken')


const app = express()
// 中间件，给接口统一添加功能
// 让post接口可以获取json格式的参数
app.use(express.json())
// 处理跨域
app.use(cors())


// 注册
app.post('/api/register', async (req, res) => {
  const { user, pass } = req.body
  // 读取用户列表，根据参数去列表查找是否存在
  const userlist = await readUser()
  const cur = userlist.find(v => v.user === user)
  if (cur) {
    res.send({
      code: -1,
      msg: '用户名已存在'
    })
  } else {
    // 数据中没有此用户，把用户添加到列表中，写入文件
    userlist.push({
      uid: Date.now(),
      user,
      pass
    })
    await writeUser(userlist)
    res.send({
      code: 0,
      msg: '成功'
    })
  }
})


// 登录
app.post('/api/login', async (req, res) => {
  // 读取用户列表，根据参数去列表查找是否存在
  const { user, pass } = req.body
  const userlist = await readUser()
  const cur = userlist.find(v => v.user === user && v.pass === pass)
  if (cur) {
    // 根据用户信息生成token返回给前端
    const token = jwt.sign(cur, privateKey, { expiresIn: '1h' })
    res.send({
      code: 0,
      msg: '成功',
      token
    })
  } else {
    res.send({
      code: -1,
      msg: '用户名或密码错误'
    })
  }
})

// 单独添加中间件
// 获取文章列表
app.get('/api/article/list', verifyToken, async (req, res) => {
  console.log('获取token', req.tokenUser)
  const articleList = await readArticle()
  res.send({
    code: 0,
    msg: '成功',
    list: articleList.map(it => {
      return {
        id: it.id,
        createTime: it.createTime,
        username: it.username,
        title: it.title,
        isDel: it.username === req.tokenUser.user
      }
    })
  })
})

// 新增文章
app.post('/api/article/create', verifyToken, async (req, res) => {
  const { title, content } = req.body
  // 读取文章列表，把内容添加到文章列表
  const articleList = await readArticle()
  articleList.unshift({
    id: Date.now(),
    username: req.tokenUser.user,
    createTime: Date.now(),
    title,
    content
  })
  // 写入数据
  await writeArticle(articleList)
  res.send({
    code: 0,
    msg: '创建成功'
  })
})

// 给后续所有请求都添加中间件
app.use(verifyToken)

// 文章详情
app.get('/api/article/detail', async (req, res) => {
  const { id } = req.query
  // 读取文章列表，根据id查找文章
  const articleList = await readArticle()
  const article = articleList.find(v => v.id === Number(id))

  if (article) {
    res.send({
      code: 0,
      msg: '成功',
      article
    })  
  } else {
    res.send({
      code: -1,
      msg: '文章不存在'
    })  
  }
})


// 删除文章
app.delete('/api/article/del', async (req, res) => {
  const { id } = req.query
  // 读取文章列表，根据id查找文章
  const articleList = await readArticle()
  const index = articleList.findIndex(v => v.id === Number(id))

  if (index > -1) {
    // 判断此文章是不是本用户创建的
    if (articleList[index].username === req.tokenUser.user) {
      articleList.splice(index, 1)
      await writeArticle(articleList)
      res.send({
        code: 0,
        msg: '成功'
      })  
    } else {
      res.send({
        code: -2,
        msg: '没有权限'
      })  
    }
  } else {
    res.send({
      code: -1,
      msg: '文章不存在'
    })  
  }
})

// 编辑文章
app.post('/api/article/update', async (req, res) => {
  const { id, title, content } = req.body
  // 读取文章列表，根据id查找文章
  const articleList = await readArticle()
  const index = articleList.findIndex(v => v.id === Number(id))
  if (index > -1) {
    // 判断此文章是不是本用户创建的
    if (articleList[index].username === req.tokenUser.user) {
      // 替换内容
      articleList[index].title = title
      articleList[index].content = content
      articleList[index].updateTime = Date.now()
      
      await writeArticle(articleList)

      res.send({
        code: 0,
        msg: '成功'
      })  
    } else {
      res.send({
        code: -2,
        msg: '没有权限'
      })  
    }
  } else {
    res.send({
      code: -1,
      msg: '文章不存在'
    })  
  }
})


const port = 9002
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://${ip.address()}:${port}`)
})