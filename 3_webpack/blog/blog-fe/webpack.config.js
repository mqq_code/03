const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  entry: {
    home: './src/pages/home/index.js',
    login: './src/pages/login/login.js',
    detail: './src/pages/detail/detail.js',
    create: './src/pages/create/create.js'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'js/[name].[hash:8].js',
    clean: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/pages/home/index.html',
      filename: 'index.html',
      chunks: ['home']
    }),
    new HtmlWebpackPlugin({
      template: './src/pages/login/login.html',
      filename: 'login.html',
      chunks: ['login']
    }),
    new HtmlWebpackPlugin({
      template: './src/pages/detail/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new HtmlWebpackPlugin({
      template: './src/pages/create/create.html',
      filename: 'create.html',
      chunks: ['create']
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash:8].css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  }
}