
export const $ = el => document.querySelector(el)
export const $all = el => [...document.querySelectorAll(el)]

export const getQuery = () => {
  const searchArr = location.search.slice(1).split('&')
  const query = {}
  searchArr.forEach(it => {
    const [k, v] = it.split('=')
    query[k] = v
  })
  return query
}