import axios from 'axios'
import './index.scss'
import { $, $all } from '../../utils'
import moment from 'moment'
import { getListApi, delApi } from '../../services'

$('.create-btn').addEventListener('click', () => {
  location.href = './create.html'
})

const del = async id => {
  if (window.confirm('确定要删除吗？')) {
    try {
      const res = await delApi(id)
      if (res.data.code === 0) {
        getList()
      } else {
        alert(res.data.msg)
      }
    } catch(e) {
      console.log(e)
    }
  } else {
    console.log('取消删除')
  }
}

const render = (list) => {
  $('main').innerHTML = list.map(item => {
    return `
      <div class="row" data-id="${item.id}">
        <h3>${item.title}</h3>
        <p>
          <b>${item.username}</b>
          <span>${moment(item.createTime).format('YYYY-MM-DD kk:mm:ss')}</span>
        </p>
        ${item.isDel ? '<button class="del">删除</button>' : ''}
        ${item.isDel ? '<button class="edit">编辑</button>' : ''}
      </div>
    `
  }).join('')

  $all('.row').forEach(item => {
    item.addEventListener('click', e => {
      const id = item.getAttribute('data-id')
      if (e.target.classList.contains('del')) {
        del(id)
      } else if (e.target.classList.contains('edit')) {
        location.href = `./create.html?id=${id}`
      } else {
        location.href = `./detail.html?id=${id}`
      }
    })
  })
}

const getList = async () => {
  try {
    const res = await getListApi()
    if (res.data.code === 0) {
      render(res.data.list)
    } else {
      alert(res.data.msg)
    }
  } catch(e) {
    console.log(e)
  }
}

getList()

// 轮询接口，自动更新
// setInterval(() => {
//   getList()
// }, 1000)






console.log('首页')