import './create.scss'
import { $, getQuery } from '../../utils'
import { getDetailApi, createArticleApi, updateArticleApi } from '../../services'

const query = getQuery()

const init = async () => {
  const res = await getDetailApi(query.id)
  if (res.data.code === 0) {
    $('.app input').value = res.data.article.title
    $('.app textarea').value = res.data.article.content
  } else {
    location.href = './index.html'
  }
}

if (query.id) {
  init()
  $('button').innerHTML = '保存'
}

const createArticle = async (title, content) => {
  try {
    // 调用接口
    const res = await createArticleApi({ title, content })
    alert('创建成功')
    location.href = './index.html'
  } catch (e) {
    console.log(e)
  }
}

const updateArticle = async (title, content) => {
  try {
    // 调用接口
    const res = await updateArticleApi({
      title,
      content,
      id: query.id
    })
    if (res.data.code === 0) {
      alert('编辑成功')
      location.href = './index.html'
    } else {
      alert(res.data.msg)
    }
  } catch (e) {
    console.log(e)
  }
}

$('button').addEventListener('click', async () => {
  const title = $('input').value.trim()
  const content = $('textarea').value.trim()

  if (!title || !content) {
    alert('请输入标题和内容')
    return
  }
  if (query.id) {
    // 调用编辑接口
    updateArticle(title, content)
  } else {
    createArticle(title, content)
  }
})


