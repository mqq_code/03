import { getQuery, $ } from '../../utils'
import './detail.scss'
import moment from 'moment'
import { getDetailApi } from '../../services'

const query = getQuery()

getDetailApi(query.id)
  .then(res => {
    if (res.data.code === 0) {
      $('.app h2').innerHTML = res.data.article.title
      $('.app b').innerHTML = res.data.article.username
      $('.app span').innerHTML = moment(res.data.article.createTime).format('YYYY_MM_DD kk:mm:ss')
      $('.content').innerHTML = res.data.article.content
    } else {
      alert(res.data.msg)
    }
  })