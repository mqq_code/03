import './login.scss'
import { $, $all } from '../../utils'
import { registerApi, loginApi } from '../../services'

// 切换登录和注册
$all('.trigger b').forEach(b => {
  b.addEventListener('click', () => {
    $('.active').classList.remove('active')
    b.classList.add('active')
    const islogin = b.getAttribute('data-islogin')
    if (islogin) {
      $('.login-form').classList.add('show')
      $('.register-form').classList.remove('show')
    } else {
      $('.register-form').classList.add('show')
      $('.login-form').classList.remove('show')
    }
  })
})

// 注册
$('.register-btn').addEventListener('click', async () => {
  const user = $('.register-form .user').value.trim()
  const pass = $('.register-form .pass').value.trim()
  const check = $('.register-form .check').value.trim()
  if (!user || !pass) {
    alert('请输入用户名和密码!')
    return
  }
  if (pass !== check) {
    alert('两次密码不一致!')
    return
  }
  // 调用注册接口
  const res = await registerApi({ user, pass })
  console.log('res', res)
  if (res.data.code === 0) {
    alert('注册成功！')
    $('.login-form').classList.add('show')
    $('.register-form').classList.remove('show')
  } else {
    alert(res.data.msg)
  }
})

// 登录
$('.login-btn').addEventListener('click', async () => {
  const user = $('.login-form .user').value.trim()
  const pass = $('.login-form .pass').value.trim()
  if (!user || !pass) {
    alert('请输入用户名和密码!')
    return
  }
  // 调用登录接口
  const res = await loginApi({ user, pass })
  if (res.data.code === 0) {
    alert('登录成功！')
    localStorage.setItem('token', res.data.token)
    location.href = './index.html'
  } else {
    alert(res.data.msg)
  }
})