import axios from 'axios'


axios.defaults.baseURL = 'http://10.37.28.29:9002'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 所有通过axios发送的请求都会先执行此函数，可以在此函数内给所有请求添加公共参数
  // console.log(config)
  // if (config.data) {
  //   config.data.abctest = '测试一下'
  // }
  config.headers.abcd = 'ABCDEFG'
  config.headers.Authorization = localStorage.getItem('token')
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  console.log('response', response)
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 可以给所有请求统一处理错误
  if (error.response.status === 401) {
    alert('用户信息失效，请重新登录')
    localStorage.removeItem('token')
    location.href = './login.html'
  }
  return Promise.reject(error);
});



// 注册接口
export const registerApi = ({ user, pass }) => {
  return axios.post('/api/register', {
    user,
    pass
  })
}
// 登录接口
export const loginApi = ({ user, pass }) => {
  return axios.post('/api/login', {
    user,
    pass
  })
}

// 获取列表接口
export const getListApi = () => {
  return axios.get('/api/article/list')
}
// 删除接口
export const delApi = (id) => {
  return axios.delete('/api/article/del', {
    params: {
      id
    }
  })
}
// 获取详情接口
export const getDetailApi = (id) => {
  return axios.get('/api/article/detail', {
    params: {
      id
    }
  })
}
// 创建文章接口
export const createArticleApi = ({ title, content }) => {
  return axios.post('/api/article/create', {
    title,
    content
  })
}
// 更新文章接口
export const updateArticleApi = ({ title, content, id }) => {
  return axios.post('/api/article/update', {
    title,
    content,
    id
  })
}

