const path = require('path')
const fs = require('fs')
const url = require('url')
const { readFile } = require('../utils/utils')


const static = async (request, response) => {
  const { pathname } = url.parse(request.url, true)

  // 去dist中查找是否存在此文件
  const fullpath = path.join(__dirname, '../dist', pathname ===  '/' ? '/index.html' : pathname)

  if (fs.existsSync(fullpath)) {
    const info = fs.statSync(fullpath)
    if (info.isFile()) {
      const data = await readFile(fullpath)
      response.end(data)
      return true
    }
  }

  return false
}
module.exports = static