const fs = require('fs')

const readFile = (url) => {
  return new Promise((resolve, reject) => {
    fs.readFile(url, 'utf-8', (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}

const writeFile = (url, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(url, data, err => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

module.exports = {
  readFile,
  writeFile
}