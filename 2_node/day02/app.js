const http = require('http')
const path = require('path')
const url = require('url')
const fs = require('fs')
const qs = require('querystring')

const static = require('./path/static')
const { readFile, writeFile } = require('./utils/utils')


const addLog = async (request) => {
  const logsPath = path.join(__dirname, 'log/log.json')

  // 读取log文件
  const data = await readFile(logsPath)
  // 读取成功，push数据
  const logs = JSON.parse(data)
  logs.push({
    time: new Date().toLocaleString(),
    url: request.url,
    host: request.headers.host,
    'user-agent': request.headers['user-agent']
  })
  // 写数据
  writeFile(logsPath,  JSON.stringify(logs))
}


const api = {
  '/api/logs GET': async function(request, response) {
    const data = await readFile(path.join(__dirname, 'log/log.json'))
    response.setHeader('content-type', 'application/json;charset=utf-8')
    response.end(data)
  },
  '/api/list GET': async function(request, response) {
    const { pathname, query } = url.parse(request.url, true)

    console.log('get请求接收前端传入的参数', query)
    const { page, pagesize } = query
    // 读取所有数据
    const data = await readFile(path.join(__dirname, 'data/userlist.json'))
    // json 转对象
    const list = JSON.parse(data)
    // page = 1, pagesize = 10  list.slice(0, 10)
    // page = 2, pagesize = 10  list.slice(10, 20)
    // page = 3, pagesize = 10  list.slice(20, 30)
    // 根据传入的分页参数截取数据
    const res = list.slice(page * pagesize - pagesize, page * pagesize)
    // 设置响应头
    response.setHeader('content-type', 'application/json;charset=utf-8')
    response.end(JSON.stringify({
      total: list.length,
      list: res
    }))
  },
  '/api/del POST': async function(request, response) {
    const { pathname, query } = url.parse(request.url, true)
    // console.log('post 请求接收 query 参数', query)
    let data = await readFile(path.join(__dirname, 'data/userlist.json'))
    data = JSON.parse(data)

    // post 请求接收 body 参数
    let params = ''
    // 接收数据拼接到 params 中
    request.on('data', chunk => {
      params += chunk
    })
    // 接收完成
    request.on('end', async () => {
      console.log('post参数接收完成', params)
      // 解析 application/x-www-form-urlencoded 格式的参数
      // params = qs.decode(params)
      // console.log(params)

      // 解析 json 格式数据
      params = JSON.parse(params)
      console.log(params.id)

      // 根据id从data中查找下标，删除
      const index = data.findIndex(v => v.id === params.id)
      data.splice(index, 1)
      // 更新数据
      await writeFile(path.join(__dirname, 'data/userlist.json'), JSON.stringify(data))

      response.setHeader('content-type', 'application/json;charset=utf-8')
      response.end(JSON.stringify({
        code: 0,
        msg: '删除成功'
      }))
    })
  }
}

const app = http.createServer(async (request, response) => {

  // 记录访问的时间和url
  // addLog(request)
  const { pathname, query } = url.parse(request.url, true)

  // 去查找是不是静态文件
  const isStatic = await static(request, response)
  if (isStatic) return

  // 接口： 给前端传输数据
  if (api[`${pathname} ${request.method}`]) {
    api[`${pathname} ${request.method}`](request, response)
    return
  }

  response.statusCode = 404
  response.setHeader('content-type', 'text/html;charset=utf-8')
  response.end(`
    <div>
      <h1>访问的资源不存在</h1>
    </div>
  `)
})

const port = 9000
app.listen(port, () => {
  console.log(`服务启动成功 http://10.37.28.28:${port}`)
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
})





  // if (pathname === '/api/logs' && request.method === 'GET') {
  //   const data = await readFile(path.join(__dirname, 'log/log.json'))
  //   response.setHeader('content-type', 'application/json;charset=utf-8')
  //   response.end(data)
  //   return
  // } else if (pathname === '/api/list' && request.method === 'GET') {
  //   console.log('get请求接收前端传入的参数', query)
  //   const { page, pagesize } = query
  //   const data = await readFile(path.join(__dirname, 'data/userlist.json'))
  //   // json 转对象
  //   const list = JSON.parse(data)
  //   // page = 1, pagesize = 10  list.slice(0, 10)
  //   // page = 2, pagesize = 10  list.slice(10, 20)
  //   // page = 3, pagesize = 10  list.slice(20, 30)
  //   // 根据传入的分页参数截取数据
  //   const res = list.slice(page * pagesize - pagesize, page * pagesize)

  //   response.setHeader('content-type', 'application/json;charset=utf-8')
  //   response.end(JSON.stringify(res))
  //   return
  // } else if (pathname === '/api/del' && request.method === 'POST') {

  // } else if (pathname === '/api/create' && request.method === 'POST') {

  // }


  // 产品经理：策划产品，定制需求
  // UI/UE: 视觉设计/交互设计
  // 前端：开发页面
  // 后端：数据
  // 测试: 测试功能
