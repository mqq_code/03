// 引入node提供的文件操作工具
const fs = require('fs')
// 处理路径
const path = require('path')

// 读取文件
// 同步方法
// const data = fs.readFileSync('./笔记.md', 'utf-8')
// console.log(data)

// 异步方法
// fs.readFile('index.js', 'utf-8', (err, data) => {
//   if (err) {
//     console.log('error: ', err)
//   } else {
//     // 读取文件成功
//     console.log(data)
//   }
// })

// 覆盖文件内容，如果没有此文件，会自动创建文件
// fs.writeFileSync('a.txt', `修改时间：${new Date().toLocaleString()}， 内容：${Math.random()}`)
// fs.writeFile('a.txt', '测试一下', (err, data) => {
//   if (!err) {
//     console.log('a.txt修改成功')
//   } else {
//     console.log('修改a.txt失败', err)
//   }
// })

// 追加文件内容
// fs.appendFile('a.txt', 'cccccc', err => {
//   if (!err) {
//     console.log('a.txt修改成功')
//   } else {
//     console.log('修改a.txt失败', err)
//   }
// })
// fs.appendFileSync('a.txt', 'bbbbbbb')

// 删除文件
// try {
//   fs.unlinkSync('a.txt')
// } catch(e) {
//   console.log('报错了：', e)
// }
// console.log('=========== end ===========')
// fs.unlink('a.txt', err => {
//   if (err) {
//     console.log('删除失败')
//   } else {
//     console.log('删除成功')
//   }
// })


// 判断文件是否存在
// console.log(fs.existsSync('index.js'))

// 拷贝文件
// fs.copyFileSync('index.js', 'utils/info/abc.js')

// 修改文件名，可以移动文件
// fs.renameSync('aaaaaaa.js', 'utils/js/abcdefg.js')


// 获取文件信息
// const info = fs.statSync('./js')
// console.log(info.isDirectory()) // 判断是不是文件夹
// console.log(info.isFile()) // 判断是不是文件


// 创建文件夹
// fs.mkdirSync('utils/js/abc')

// 删除文件夹
// fs.rmdirSync('utils')


// 读取文件目录
// const menu = fs.readdirSync('./')
// console.log(menu)


// fs如果使用相对路径读取文件是相对于运行时终端所在的目录

console.log('当前文件夹的绝对路径', __dirname)
console.log('当前文件的绝对路径', __filename)

// fs.readFile(path.join(__dirname, './index.js'), 'utf-8', (err, data) => {
//   if (err) {
//     console.log('报错了:', err)
//   } else {
//     console.log(data)
//   }
// })


// console.log(path.join(__dirname, '../../', 'index.js'))

// 拼接路径
// console.log(path.join('/a/b/c/d/f/', '../../', '/e/g/h', 'a.js'))
// 返回一个绝对路径，从后往前拼接，找到绝对路径就直接返回
console.log(path.resolve('/a/b', '/c', '/d.js'))