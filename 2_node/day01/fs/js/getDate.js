
const getDate = () => {
  return new Date().toLocaleString()
}
const ABC = 100
const Arr = [1,2,3,4,5]

// 抛出变量
// exports.bcd = 'BBBBBBBBBBB'
// exports.efg = 'GGGGGGGGGGG'


// 抛出此变量给其他文件使用
// module.exports = getDate

module.exports = {
  getDate,
  ABC,
  Arr
}
