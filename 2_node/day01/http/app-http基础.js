const http = require('http')

// 创建服务器应用
const app = http.createServer((request, response) => {
  // request: 请求信息
  // response: 响应对象
  // console.log('有人访问了我的服务器应用')
  console.log('请求：', request.url)


  if (request.url === '/a') {
    response.end('[1,2,3,4,5,6,7]')
    return
  } else if (request.url === '/b/c/d') {
    response.end('{a: 100, b: 200}')
    return
  }
  response.end('success')
})

// 监听端口号: 0 - 65535
const PORT = 8000
app.listen(PORT, () => {
  console.log(`服务器应用启动成功 http://10.37.28.28:${PORT}`)
})