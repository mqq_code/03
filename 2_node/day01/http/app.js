const http = require('http')
const fs = require('fs')
const path = require('path')
const url = require('url')
const readFile = require('./utils/read.js')


// 创建服务器应用
const app = http.createServer((request, response) => {

  // console.log('请求：', request.url)
  const { pathname, query } = url.parse(request.url, true)
  console.log(pathname, query)


  // 判断文件是否存在
  const filePath = path.join(__dirname, './dist', pathname === '/' ? '/index.html' : pathname)
  if (fs.existsSync(filePath)) {
    const data = fs.readFileSync(filePath)
    response.end(data)
    return
  }

  console.log('请求方式', request.method)
  // 接口：给前端返回数据
  if (pathname === '/search' && request.method === 'GET') {
    // 读取json文件
    readFile(path.join(__dirname, 'data.json'))
      .then(data => {
        // 根据参数去数据中查找
        const arr = JSON.parse(data)
        const item = arr.filter(v => v.name.includes(query.keywords) || v.description?.includes(query.keywords))
        // 找到数据返回
        // 通知浏览器返回的数据是json格式
        response.setHeader('content-type', 'application/json;chartset=utf-8')
        response.end(JSON.stringify(item))
      })
      .catch(e => {
        console.log('读取文件失败了', e)
        response.setHeader('content-type', 'application/json;chartset=utf-8')
        response.end(JSON.stringify({
          msg: '查找的数据不存在'
        }))
      })
    return
  }

  response.statusCode = 404
  response.setHeader('content-type', 'text/plain;charset=utf-8')
  response.setHeader('test123', 'ABCDEFG')
  response.end('<h1 style="color: red">请求的内容不存在 Not found</h1>')
})

// 监听端口号: 0 - 65535
const PORT = 8001
app.listen(PORT, () => {
  console.log(`服务器应用启动成功 http://10.37.28.28:${PORT}`)
})



// http://10.37.28.28:8001/index.html?a=100&b=200&c=300000
// 协议：http、htpps
// 域名（IP）: 10.37.28.28、baidu.com
// 端口: 8001 (0-65535)
// 路径: /index.html
// 查询参数（search）: a=100&b=200&c=300000