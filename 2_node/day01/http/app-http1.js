const http = require('http')
const fs = require('fs')
const path = require('path')
const url = require('url')

// 创建服务器应用
const app = http.createServer((request, response) => {
  // request: 请求信息
  // response: 响应对象

  console.log('请求：', request.url)
  const { pathname, query } = url.parse(request.url, true)

  // if (request.url === '/' || request.url === '/index.html') {
  //   const data = fs.readFileSync(path.join(__dirname, './dist/index.html'))
  //   response.end(data)
  //   return
  // } else if (request.url === '/style.css') {
  //   const data = fs.readFileSync(path.join(__dirname, './dist/style.css'))
  //   response.end(data)
  //   return
  // } else if (request.url === '/index.js') {
  //   const data = fs.readFileSync(path.join(__dirname, './dist/index.js'))
  //   response.end(data)
  //   return
  // } else if (request.url === '/favicon.ico') {
  //   const data = fs.readFileSync(path.join(__dirname, './dist/favicon.ico'))
  //   response.end(data)
  //   return
  // }

  // 判断文件是否存在
  const filePath = path.join(__dirname, './dist', pathname === '/' ? '/index.html' : pathname)
  if (fs.existsSync(filePath)) {
    const data = fs.readFileSync(filePath)
    response.end(data)
    return
  }
  


  if (request.url === '/a') {
    response.end('[1,2,3,4,5,6,7]')
    return
  } else if (request.url === '/b/c/d') {
    response.end('{a: 100, b: 200}')
    return
  }

  response.statusCode = 404
  response.setHeader('content-type', 'text/plain;charset=utf-8')
  response.setHeader('test123', 'ABCDEFG')
  response.end('<h1 style="color: red">请求的内容不存在 Not found</h1>')
})

// 监听端口号: 0 - 65535
const PORT = 8001
app.listen(PORT, () => {
  console.log(`服务器应用启动成功 http://10.37.28.28:${PORT}`)
})



// http://10.37.28.28:8001/index.html?a=100&b=200&c=300000
// 协议：http、htpps
// 域名（IP）: 10.37.28.28、baidu.com
// 端口: 8001 (0-65535)
// 路径: /index.html
// 查询参数（search）: a=100&b=200&c=300000