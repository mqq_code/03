const express = require('express')
const ip = require('ip')
const axios = require('axios')
const { createProxyMiddleware } = require('http-proxy-middleware')


const app = express()
app.use(express.static('./dist'))

// 配置请求代理，所有以 /bw 开头的请求都会转发发到 http://10.37.28.29:9002
app.use(
  '/bw',
  createProxyMiddleware({
    target: 'http://10.37.28.29:9002/',
    pathRewrite: {'^/bw' : ''}, // /bw/api/list => http://10.37.28.29:9002/api/list
    changeOrigin: true,
  })
)






// app.get('/test/abc', (req, res) => {
//   // 调用第三方接口
//   axios.get('http://10.37.28.29:9002/api/list')
//     .then(data => {
//       // 返回给前端页面
//       res.send(data.data)
//     })
// })
  




const port = 8005
app.listen(port, () => {
  console.log(`后端服务运行成功 http://localhost:${port}`)
  console.log(`后端服务运行成功 http://127.0.0.1:${port}`)
  console.log(`后端服务运行成功 http://${ip.address()}:${port}`)
})