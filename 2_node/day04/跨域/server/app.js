const express = require('express')
const ip = require('ip')


const app = express()

app.use((req, res, next) => {
  // 接口配置允许跨域访问的响应头，
  // Access-Control-Allow-Origin: 允许跨域访问的地址
  // const allowedOrigins = ['http://10.37.28.29:8005', 'http://localhost:8005']
  // const requestOrigin = req.headers.origin
  // if (allowedOrigins.includes(requestOrigin)) {
  //   res.setHeader('Access-Control-Allow-Origin', requestOrigin)
  // }
  res.setHeader('Access-Control-Allow-Origin', '*')
  next()
})



app.get('/api/list', (req, res) => {
  res.send({
    code: 0,
    msg: '成功',
    values: [1,2,3,4,5,6,7,8]
  })  
})


app.post('/api/goods', (req, res) => {
  res.send({
    code: 0,
    msg: '我是goods接口'
  })
})


app.post('/api/detail', (req, res) => {
  res.send({
    code: 0,
    msg: '我是detail接口'
  })
})


app.get('/jsonp/test', (req, res) => {
  const list = [
    { name: '小明1', age: 21 },
    { name: '小明2', age: 22 },
    { name: '小明3', age: 23 },
    { name: '小明4', age: 24 }
  ]
  res.setHeader('content-type', 'application/javascript')
  res.end(`jsonpCallback(${JSON.stringify(list)})`)
})



const port = 9002
app.listen(port, () => {
  console.log(`后端服务运行成功 http://localhost:${port}`)
  console.log(`后端服务运行成功 http://127.0.0.1:${port}`)
  console.log(`后端服务运行成功 http://${ip.address()}:${port}`)

})