const express = require('express')
const ip = require('ip')
const multer  = require('multer')
const path = require('path')
const fs = require('fs')
const jwt = require('jsonwebtoken');

const app = express()
app.use(express.static('./dist'))
app.use(express.static('./images'))
app.use(express.json())

const privateKey = 'zyx_bw'


const readDb = (file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, './db', file), 'utf-8', (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })
}
const writeDb = (file, content) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, './db', file), JSON.stringify(content), (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}
// 注册接口
app.post('/api/signin', async (req, res) => {
  // console.log('注册接口获取数据', req.body)
  try {
    // 读取用户列表，判断用户是否存在，不存在就添加到用户列表
    const userlist = await readDb('userlist.json')
    const index = userlist.findIndex(v => v.username === req.body.username)
    if (index > -1) {
      res.send({
        code: -1,
        msg: '用户名已存在'
      })
      return
    }
    userlist.push({
      uid: Date.now(),
      ...req.body
    })
    // 写入数据库
    await writeDb('userlist.json', userlist)
    res.send({
      code: 0,
      msg: '注册成功'
    })
  } catch (e) {
    res.send({
      code: -1,
      msg: '注册失败'
    })
  }
})

// 登录
app.post('/api/login', async (req, res) => {
  const { username, pass } = req.body
  const userlist = await readDb('userlist.json')
  const user = userlist.find(v => v.username === username && v.pass === pass)
  if (user) {
    // 给前端页面设置cookie，httpOnly: 不允许前端读写此cookie
    // res.cookie('token', '12345', { maxAge: 100000, httpOnly: true })

    // 生成token返回给前端
    const token = jwt.sign(user, privateKey, { expiresIn: '1h' })
    res.send({
      code: 0,
      msg: '登录成功',
      token
    })
  } else {
    res.send({
      code: -1,
      msg: '用户名或者密码错误'
    })
  }
})


// 获取个人信息
app.get('/api/userinfo', async (req, res) => {
  // 从请求头中获取cookie，验证身份信息
  // console.log(req.headers.cookie)

  try {
      // 从请求头中获取token
      // console.log(req.headers.authorazation)
      // 解析token
      const token = jwt.verify(req.headers.authorazation, privateKey)
      console.log('解密后的token', token)
      // 根据token去查找信息
      const userlist = await readDb('userlist.json')
      const user = userlist.find(v => v.uid === token.uid)
      if (user) {
        res.send({
          code: 0,
          msg: '成功',
          userInfo: user
        })
      } else {
        res.statusCode = 401
        res.send({
          code: -1,
          msg: '登录信息失效，请重新登录',
        })
      }
  } catch(e) {
    res.statusCode = 401
    res.send({
      code: -1,
      msg: '登录信息失效，请重新登录',
    })
  }
  
})













// 设置存储引擎
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'images/') // 保存的路径，'images'文件夹，需要确保存在
  },
  filename: function (req, file, cb) {
    // 将保存文件名设置为原始文件名
    cb(null, Date.now() + '-' + file.originalname)
  }
})

const upload = multer({ storage: storage })

app.post('/api/upload', upload.single('file'), async (req, res) => {
  // 单个文件上传
  if (!req.file) {
    return res.status(400).send('No file uploaded.');
  }

  try {
    const token = jwt.verify(req.headers.authorazation, privateKey)
    // 根据token去查找信息
    const userlist = await readDb('userlist.json')
    const index = userlist.findIndex(v => v.uid === token.uid)
    userlist[index].avatar = `http://${ip.address()}:${port}/${req.file.filename}`
    // 写入数据库
    await writeDb('userlist.json', userlist)
    // req.file 是文件信息
    console.log(req.file)
    res.send({
      code: 0,
      msg: '成功',
      url: `http://${ip.address()}:${port}/${req.file.filename}`
    });
  } catch(e) {
    res.statusCode = 401
    res.send({
      code: -1,
      msg: '登录信息失效，请重新登录',
    })
  }

});




const port = 8005
app.listen(port, () => {
  console.log(`后端服务运行成功 http://localhost:${port}`)
  console.log(`后端服务运行成功 http://127.0.0.1:${port}`)
  console.log(`后端服务运行成功 http://${ip.address()}:${port}`)
})