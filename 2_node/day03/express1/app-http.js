const http = require('http')
const url = require('url')
const ip = require('ip')
const path = require('path')
const fs = require('fs')



const app = http.createServer((request, response) => {

  const { pathname, query } = url.parse(request.url)

  const fullPath = path.join(__dirname, 'dist', pathname === '/' ? '/index.html' : pathname)
  if (fs.existsSync(fullPath)) {
    const info = fs.statSync(fullPath)
    if (info.isFile()) {
      response.end(fs.readFileSync(fullPath))
      return
    }
  }

  const imgUrl = path.join(__dirname, 'img', pathname)
  if (fs.existsSync(imgUrl)) {
    response.end(fs.readFileSync(imgUrl))
    return 
  }

  // 定义list接口
  if (pathname === '/api/list' && request.method === 'GET') {
    const data = fs.readFileSync(path.join(__dirname, './data/goods.json'), 'utf-8')
    response.setHeader('content-type', 'application/json;charset=utf-8')
    response.end(data)
    return
  } else if (pathname === '/api/imgs' && request.method === 'GET') {
    const data = fs.readFileSync(path.join(__dirname, './data/data.json'), 'utf-8')
    response.setHeader('content-type', 'application/json;charset=utf-8')
    response.end(data)
    return
  }



  response.end('success')
})

app.listen(9000, () => {
  console.log('服务启动成功 http://localhost:9000')
  console.log('服务启动成功 http://127.0.0.1:9000')
  console.log(`服务启动成功 http://${ip.address()}:9000`)
})