const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')


const data = Mock.mock({
  "list|20": [{
    "id": "@id",
    "price|1-100": 1,
    "count": 0,
    "name": "@cname",
    "desc": "@cword(30)",
    "email": "@email",
    "address": "@county(true)",
    "img": "@image(100x100, @color)"
  }]
})

fs.writeFileSync(path.join(__dirname, './goods.json'), JSON.stringify(data.list))