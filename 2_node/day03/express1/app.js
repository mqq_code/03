const express = require('express')
const ip = require('ip')
const fs = require('fs')
const path = require('path')

// 创建服务器应用
const app = express()
// 允许post接收 json 格式的参数
app.use(express.json())
// 允许post接收 urlencoded 格式的参数
app.use(express.urlencoded())

// 处理静态资源
app.use(express.static(path.join(__dirname, './dist')))
app.use(express.static(path.join(__dirname, './img')))

// 定义get接口
app.get('/api/list', (req, res) => {
  // 获取get请求的参数
  console.log('query', req.query)

  const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data/goods.json'), 'utf-8'))
  // send方法会自动把对象转成json
  res.send({
    code: 0,
    msg: '成功',
    data
  })

  // 返回字符串
  // res.setHeader('content-type', 'application/json;charset=utf-8')
  // res.end(JSON.stringify({
  //   code: 0,
  //   msg: '成功',
  //   data
  // }))
})

app.get('/api/imgs', (req, res) => {
  const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data/data.json'), 'utf-8'))
  
  // send方法会自动把对象转成json
  res.send({
    code: 0,
    msg: '成功',
    data
  })
})

// 定义post接口
app.post('/api/add/count', (req, res) => {
  console.log('post参数', req.body)
  // send方法会自动把对象转成json
  res.send({
    code: 0,
    msg: '成功'
  })
})


const port = 9000
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
  console.log(`服务启动成功 http://${ip.address()}:${port}`)
})