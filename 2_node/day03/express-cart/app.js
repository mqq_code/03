const express = require('express')
const ip = require('ip')
const fs = require('fs')
const path = require('path')

// 创建服务器应用
const app = express()
// 允许post接收 json 格式的参数
app.use(express.json())
// 允许post接收 urlencoded 格式的参数
app.use(express.urlencoded())

// 处理静态资源
app.use(express.static(path.join(__dirname, './dist')))

const readGoodsList = () => JSON.parse(fs.readFileSync(path.join(__dirname, './data/goods.json'), 'utf-8'))
const readCartList = () => JSON.parse(fs.readFileSync(path.join(__dirname, './data/cart.json'), 'utf-8'))
const writeCartList = (cartlist) => {
  fs.writeFileSync(path.join(__dirname, './data/cart.json'), JSON.stringify(cartlist))
}


// 商品列表接口
app.get('/api/list', (req, res) => {
  // 读取list文件
  const data = readGoodsList()
  res.send({
    code: 0,
    msg: '成功',
    data
  })
})

// 详情页面接口
app.get('/api/goods/info', (req, res) => {
  console.log('接收前端传入的参数', req.query)
  const { id } = req.query
  // 所有商品列表
  const data = readGoodsList()
  // 根据传入的id去商品列表中查找
  const item = data.find(v => v.id === id)
  if (item) {
    res.send({
      code: 0,
      msg: '成功',
      data: item
    })
  } else {
    res.send({
      code: -1,
      msg: '商品不存在'
    })
  }
})

// 商品加减接口
app.post('/api/add/count', (req, res) => {
  const { id, num = 1 } = req.body
  try {
    // 读取购物车数据
    const cartlist = readCartList()
    // 根据id去购物车中查找商品，判断商品是否存在
    const cartIndex = cartlist.findIndex(v => v.id === id)
    if (cartIndex > -1) {
      // 如果存在就直接加1
      cartlist[cartIndex].count += num
      // 如果数量为0就从购物车中删除
      if (cartlist[cartIndex].count === 0) {
        cartlist.splice(cartIndex, 1)
      }
      writeCartList(cartlist)

    } else {
      // 不存在，根据id去所有商品中查找
      const data = readGoodsList()
      const item = data.find(v => v.id === id)
      // 把找到的商品添加到购物车列表中
      item.count = 1
      cartlist.push(item)
      writeCartList(cartlist)
    }
    res.send({
      code: 0,
      msg: '成功'
    })
  } catch(e) {
    res.send({
      code: -1,
      msg: '失败'
    })
  }
})


// 购物车列表接口
app.get('/api/cartlist', (req, res) => {
  // 读取购物车数据
  const cartlist = readCartList()
  res.send({
    code: 0,
    msg: '成功',
    data: cartlist
  })
})

const port = 9000
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
  console.log(`服务启动成功 http://${ip.address()}:${port}`)
})