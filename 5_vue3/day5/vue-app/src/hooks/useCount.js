import { ref, computed, onUnmounted, onMounted } from 'vue'

const addZero = n => n >= 10 ? n : `0${n}`

const format = s => {
  const h = Math.floor(s / 60 / 60)
  const m = Math.floor(s / 60 % 60)
  s = s % 60
  return `${addZero(h)}:${addZero(m)}:${addZero(s)}`
}


export const useCount = (s, immediate) => {

  const count = ref(s)
  const formatCount = computed(() => {
    return format(count.value)
  })

  let timer = null

  const start = () => {
    timer = setInterval(() => {
      count.value--
      console.log(count.value)
      if (count.value === 0) {
        stop()
      }
    }, 1000);
  }

  const stop = () => {
    clearInterval(timer)
  }

  const reset = () => {
    stop()
    count.value = 10
  }

  onUnmounted(() => {
    stop()
  })

  onMounted(() => {
    if (immediate) {
      start()
    }
  })


  return {
    count,
    formatCount,
    start,
    stop,
    reset
  }
}