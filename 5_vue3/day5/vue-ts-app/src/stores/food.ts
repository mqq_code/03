import { defineStore } from 'pinia'
import { ref } from 'vue'
import axios from 'axios'

export interface Food {
  category_id: string;
  content: string;
  glycemicIndex: string;
  imgUrl: string;
  level: number;
  name:  string;
  quality: number;
  isActive?: boolean;
}

interface TabItem {
  name: string;
  list: Food[]
}

interface FoodResponse {
  code: number;
  msg: string;
  value: TabItem[]
}

export const useFoodStore = defineStore('food', () => {
  const food = ref<TabItem[]>([]) // 所有数据
  const selected = ref<Food[]>([]) // 选中的数据

  const getFood = async () => {
    const res = await axios.get<FoodResponse>('https://zyxcl.xyz/exam_api/food')
    food.value = res.data.value
  }

  const change = (food: Food) => {
    food.isActive = !food.isActive

    if (food.isActive) {
      selected.value.push(food)
    } else {
      const index = selected.value.findIndex(v => v.name === food.name)
      selected.value.splice(index, 1)
    }
  }

  return {
    food,
    selected,
    getFood,
    change
  }
})