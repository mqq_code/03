"use strict";
{
    // 枚举： 可以使用一组方便理解的文字来描述数字
    let Sex;
    (function (Sex) {
        Sex[Sex["women"] = 0] = "women";
        Sex[Sex["men"] = 1] = "men";
        Sex[Sex["other"] = 2] = "other";
    })(Sex || (Sex = {}));
    function getSex(sex) {
        if (sex === Sex.women) {
        }
        else if (sex === Sex.men) {
        }
    }
    let Direction;
    (function (Direction) {
        Direction[Direction["Up"] = 87] = "Up";
        Direction[Direction["Down"] = 83] = "Down";
        Direction[Direction["Left"] = 65] = "Left";
        Direction[Direction["Right"] = 68] = "Right";
    })(Direction || (Direction = {}));
    const action = {
        [Direction.Left]() {
            box.style.left = box.offsetLeft - 10 + 'px';
        },
        [Direction.Up]() {
            box.style.top = box.offsetTop - 10 + 'px';
        },
        [Direction.Right]() {
            box.style.left = box.offsetLeft + 10 + 'px';
        },
        [Direction.Down]() {
            box.style.top = box.offsetTop + 10 + 'px';
        }
    };
    const box = document.querySelector('.box');
    document.addEventListener('keydown', e => {
        action[e.keyCode] && action[e.keyCode]();
    });
}
