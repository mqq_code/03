"use strict";
{
    const xm = {
        name: '小明',
        age: 33,
        sex: '女'
    };
    const fn = (text) => {
        return text.length;
    };
    const aa = fn('abc');
    const arr = [{ name: 'xm' }, { name: '11' }];
    // 重名的 type 会报错
    // type Doctor = {
    //   say: string;
    // }
    const xh = {
        name: '小红',
        age: 22,
        job: '护士',
        sex: '男',
        hobby: ['aa', 'bb']
    };
    // interface 和 type 的区别：
    // - 1. interface 定义类型，type 是定义类型别名
    // - 2. type 可以定义基础类型、联合类型、交叉类型， interface 不行
    // - 3. interface 重名时会合并，可以扩展现有的 interface 定义的类型，例如扩展 window 对象
    // - 4. type 重名会报错
    // - 5. interface 使用 extends 继承，type 使用交叉类型扩展
}
