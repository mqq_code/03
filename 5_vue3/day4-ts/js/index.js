"use strict";
{
    const obj = {
        name: '小明',
        age: 22,
        sex: '男',
        hobby: ['吃饭']
    };
    const fn = (a, b) => {
        return (a + b).toFixed(2);
    };
    const sum = (...rest) => {
        return rest.reduce((prev, val) => prev + val, 0);
    };
}
// 1. typescript 有什么优势？
// 2. interface 和 type 的区别？
// 3. 什么是泛型？
// 4. 常用的内置泛型有哪些？
// 5. any、void、never？
