"use strict";
{
    const obj = {
        name: '小明',
        age: 22,
        sex: '男',
        hobby: [],
        say(text) {
            return 123;
        }
    };
    const routes = [
        {
            path: '/11',
            name: '11',
            children: [
                {
                    path: '/11/1',
                    name: '11/1'
                },
                {
                    path: '/11/2',
                    name: '11/2',
                    children: [
                        {
                            path: '/11/2/1',
                            name: '11/2/1'
                        },
                        {
                            path: '/11/2/2',
                            name: '11/2/2'
                        }
                    ]
                }
            ]
        },
        {
            path: '/22',
            name: '22'
        }
    ];
}
