"use strict";
{
    // 泛型约束
    // function getLen<T extends { length: number }>(a: T): number {
    //   return a.length
    // }
    // console.log(getLen({ a: 'aa', length: 10 }))
    // function fn<T extends string | number>(a: T) {
    //   console.log(a)
    // }
    // fn(111)
    function getVal(obj, key) {
        return obj[key];
    }
    const obj = {
        name: '小小',
        age: 22,
        hobby: ['吃饭', '喝水']
    };
    console.log(getVal(obj, 'hobby'));
    const xh = {
        title: '小红',
        logoColor: 'red'
    };
    console.log(getVal(xh, 'title'));
    function fn(a) {
        if (typeof a === 'string') {
            return a.length;
        }
        else {
            return a.toFixed(2);
        }
    }
    let aa = fn('111');
    const bb = '11';
    const xm = {
        name: 'xxxx',
        age: 11
    };
}
