"use strict";
{
    // typeof: 返回一个 js 变量的 ts 类型
    const obj = {
        name: '小明',
        age: 22
    };
    const arr = [1, 2, 3, 4, 5];
    const fn = (a) => {
        return a.toFixed(2);
    };
    const obj1 = {
        name: '小红',
        age: 33
    };
    const str = 'abc';
    console.log(typeof str);
    const xm = {
        name: '小明',
        age: 22,
        sex: '男',
        hobby: ['吃饭']
    };
    // type XmKey = "name" | "age" | "sex" | "hobby"
    const a = 'age';
}
