"use strict";
{
    // 泛型：类型的形参，当函数中的某个或者某些变量类型只有调用时才能确定类型时可以使用泛型
    function ref(a) {
        return { value: a };
    }
    const num = ref(0);
    num.value = 22;
    const obj = ref({ name: 'aaa' });
    const arr = ref([]);
    arr.value.push(1);
    const str = ref('abcd');
    function fn2(a, b) {
        return [a, b];
    }
    const bb = fn2('aaa', 100);
    function createArr(len, c) {
        return new Array(len).fill(c);
    }
    const arr3 = createArr(3, { name: 'aaa', age: 22 });
    arr3.forEach(it => {
        console.log(it.name);
    });
    const obj1 = {
        name: 'aaaa',
        age: 22,
        hobby: 11
    };
    const tt = ['a', 'd'];
    // const arr4: DeepArr<string>[] = [[[[[[['1', '2', '3'], '4'], '5'], '6'], '7'], '8'], '9']
    function flat(arr) {
        return arr.reduce((prev, val) => {
            return prev.concat(Array.isArray(val) ? flat(val) : val);
        }, []);
    }
    console.log(flat([[1, [2, 3], 4], 5]));
    console.log(flat([['a', ['b', 'd', 'c'], 'e'], 'f']));
}
