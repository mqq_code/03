"use strict";
{
    let a = true;
    function fn(p, b) {
        // 联合类型的变量只能访问多个类型的公用属性和方法
        // console.log(p)
        if (typeof p === 'string') {
            // 通过类型保护缩小变量的类型范围
            console.log(p.split(''));
        }
        else {
            console.log(p.join(''));
        }
    }
    fn('abc');
    const sex = '男';
    function getSexText(s) {
        return s === 0 ? '男' : '女';
    }
    const s1 = getSexText(0);
    const xm = {
        name: '小明',
        age: 22,
        job: '外科医生',
        hobby: ['吃饭'],
        sex: 1
    };
}
