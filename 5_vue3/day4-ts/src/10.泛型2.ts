{

  // 泛型约束
  // function getLen<T extends { length: number }>(a: T): number {
  //   return a.length
  // }
  // console.log(getLen({ a: 'aa', length: 10 }))


  // function fn<T extends string | number>(a: T) {
  //   console.log(a)
  // }
  // fn(111)


  function getVal<T, K extends keyof T>(obj: T, key: K) {
    return obj[key]
  }

  const obj = {
    name: '小小',
    age: 22,
    hobby: ['吃饭', '喝水']
  }

  console.log(getVal(obj, 'hobby'))

  const xh = {
    title: '小红',
    logoColor: 'red'
  }
  console.log(getVal(xh, 'title'))







  

  // 条件类型
  type FnRe<T> = T extends string ? number : string

  function fn<T extends string | number>(a: T): FnRe<T>  {
    if (typeof a === 'string') {
      return a.length as FnRe<T>
    } else {
      return a.toFixed(2) as FnRe<T>
    }
  }

  let aa = fn('111')

  const bb: FnRe<number> = '11'







  // 泛型默认值
  interface Person<T = number> {
    name: string;
    age: T;
  }

  const xm: Person = {
    name: 'xxxx',
    age: 11
  }







}