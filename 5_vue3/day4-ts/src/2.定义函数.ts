{
  // 函数
  function fn1(a: number, b: string): number {
    return 100
  }
  let a = fn1(100, '100')
  console.log(a)

  const fn2 = (a: number, b: boolean): string => {
    return 'aaaa'
  }

  const obj = {
    say(a: string): string {
      return a
    }
  }


  // 函数可选参数
  function fn3(a: string, b?: number): string {
    return a
  }
  // fn3('aaaaa')

  // 参数默认值
  function fn4(a: string, b: number = 2) {
    return a.slice(0, b)
  }
  // console.log(fn4('abcdefg', 3))



  // 函数剩余参数
  function fn5(...rest: string[]): void {
    console.log(rest)
  }
  fn5('a', 'b', 'd')


  // 参数解构赋值
  function fn6([a, b, c, d]: number[]) {
    console.log(a, b, c, d)
  }
  fn6([1,2,3,4])

  

  function fn7(obj: { name: string; age: number }) {
    console.log(obj)
  }
  fn7({ name: 'xm', age: 22})


}