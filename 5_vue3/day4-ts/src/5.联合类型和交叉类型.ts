{

  // 联合类型

  type A = number | string | boolean
  let a: A  = true

  type FnParams = number[] | string
  function fn(p: FnParams, b?: number) {
    // 联合类型的变量只能访问多个类型的公用属性和方法
    // console.log(p)
    if (typeof p === 'string') {
      // 通过类型保护缩小变量的类型范围
      console.log(p.split(''))
    } else {
      console.log(p.join(''))
    }
  }
  fn('abc')


  type Sex = '男' | '女' | '其他'
  const sex: Sex = '男'

  function getSexText(s: 0 | 1) {
    return s === 0 ? '男' : '女'
  }
  const s1 = getSexText(0)



  // 交叉类型
  type Person = {
    name: string;
    age: number;
  }
  type Job = {
    job: string;
    hobby: string[];
  }
  // 合并多个类型
  type Doctor = Person & Job & { sex: 0 | 1 }

  const xm: Doctor = {
    name: '小明',
    age: 22,
    job: '外科医生',
    hobby: ['吃饭'],
    sex: 1
  }


}