{

// number、string、boolean、null、undefined、symbol、object、array、function
// any、void、never、unknown

// 添加类型注解
let num: number = 100
let str: string = 'abc'
let flag: boolean = true
let n: null = null
let un: undefined = undefined
let s: symbol = Symbol('a')

// symbol使用场景: 给对象添加唯一属性
// let s1 = Symbol('s')
// let s2 = Symbol('s')
// const obj = {
//   name: 'ssss',
//   [s1]: '小明',
//   [s2]: 100
// }
// console.log(obj[s2])

// 定义数组方式1:
const arr: number[] = [1,2,3,4,5,6,7]
const arr1: string[] = []
arr1.push('1')

// 定义数组方式2:
const arr2: Array<number> = [1, 2, 3, 4, 5]

// 元组：定义一直长度和类型的数组
const arr3: [number, string, boolean] = [100, 'aaa', false]

// any: 任意类型，相当于放弃了类型校验
let a: any = 100
a = 'aaaa'
a = true
a = []

// void: 没有值，函数没有返回值时使用
function fn(): void {

}

// never: 不存在的值、永远不会出现的值
let b: number & string

// unknown: 暂时不确定，又不想放弃类型校验,等确定类型时使用类型断言确定变量类型
let c: unknown

setTimeout(() => {
  let n = Math.random()
  if (n > 0.5) {

    c = 'abcdefg'
    console.log((c as string).indexOf('a'))

  } else {
    c = 100;
    (c as number).toFixed(2)
  }
}, 1000)




}