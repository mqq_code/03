{

  // typeof: 返回一个 js 变量的 ts 类型
  const obj = {
    name: '小明',
    age: 22
  }
  const arr = [1,2,3,4,5]
  const fn = (a: number) => {
    return a.toFixed(2)
  }

  // 返回一个变量的类型
  type Obj = typeof obj
  type Arr = typeof arr
  type Fn = typeof fn

  const obj1: Obj = {
    name: '小红',
    age: 33
  }

  const str = 'abc'
  console.log(typeof str)



  // keyof: 返回一个对象类型中所有 key 组成的联合类型
  interface Person {
    name: string;
    age: number;
    sex: string;
  }
  const xm = {
    name: '小明',
    age: 22,
    sex: '男',
    hobby: ['吃饭']
  }

  // 返回该对象的所有 key 组成的联合类型
  type XmKey = keyof typeof xm
  // type XmKey = "name" | "age" | "sex" | "hobby"

  const a: XmKey = 'age'





}