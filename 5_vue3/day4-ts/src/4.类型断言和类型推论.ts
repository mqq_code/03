{
  // 类型推论：当定义变量时如果没有指定类型，ts会根据当前值反推变量的类型
  let a = 100
  const b = 'bb'
  const fn = (a: number, b: number) => {
    return a.toFixed(b)
  }

  let c = fn(100, 2)



  // 类型断言：当开发者比编辑器更确定变量的类型时使用
  // 变量!  非空断言
  // 变量 as 类型
  // <类型>变量

  // const inp = document.querySelector('input')!
  // const inp = document.querySelector('inp') as HTMLInputElement
  const inp = <HTMLInputElement>document.querySelector('.inp')
  console.log(inp.value)
  inp.addEventListener('change', e => {
    console.log((e.target as HTMLInputElement).value)
  })


  const h2 = document.querySelector('.title') as HTMLHeadingElement
  const box = document.querySelector('.box') as HTMLDivElement
  box.style.cssText = 'background: red; width: 100px; height: 100px'


  box.addEventListener('click', e => {
    console.log(e.pageX)
  })


}