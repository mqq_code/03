{
  // 定义对象类型
  // 方式1: 使用 type 定义类型别名
  // type Obj = {
  //   name: string;
  //   age: number;
  //   sex: string;
  // }

  // 方式2: 使用 interface 定义对象类型
  // interface Obj {
  //   name: string;
  //   age: number;
  //   sex: string;
  // }

  // const obj: Obj = {
  //   name: '小明',
  //   age: 22,
  //   sex: '男'
  // }

  // function fn1(xm: Obj) {

  // }


  // 可选属性
  interface Obj {
    readonly name: string; // 只读
    age: number;
    sex?: string; // 可选属性
    hobby: string[];
    say: (text: string) => number;
    // say(text: string): number;
  }

  const obj: Obj = {
    name: '小明',
    age: 22,
    sex: '男',
    hobby: [],
    say(text: string): number {
      return 123
    }
  }

  // obj.name = '1234'
  // console.log(obj.sex?.slice(0))




  // 定义树形结构类型
  interface RouteItem {
    path: string;
    name: string;
    children?: RouteItem[]
  }

  const routes: RouteItem[] = [
    {
      path: '/11',
      name: '11',
      children: [
        {
          path: '/11/1',
          name: '11/1'
        },
        {
          path: '/11/2',
          name: '11/2',
          children: [
            {
              path: '/11/2/1',
              name: '11/2/1'
            },
            {
              path: '/11/2/2',
              name: '11/2/2'
            }
          ]
        }
      ]
    },
    {
      path: '/22',
      name: '22'
    }
  ]



  
  

}