{

  // 泛型：类型的形参，当函数中的某个或者某些变量类型只有调用时才能确定类型时可以使用泛型

  function ref<T>(a: T): { value: T } {
    return { value: a }
  }

  const num = ref(0)
  num.value = 22

  type Obj = { name: string }
  const obj = ref<Obj>({ name: 'aaa' })

  const arr = ref<number[]>([])
  arr.value.push(1)

  const str = ref<string>('abcd')


  function fn2<T, K>(a: T, b: K): [T, K] {
    return [a, b]
  }

  const bb = fn2<string, number>('aaa', 100)



  function createArr<T>(len: number, c: T): T[] {
    return new Array(len).fill(c)
  }

  type Item = {
    name: string;
    age: number;
  }
  const arr3 = createArr<Item>(3, { name: 'aaa', age: 22 })
  arr3.forEach(it => {
    console.log(it.name)
  })



  interface Person<T> {
    name: string;
    age: number;
    hobby: T
  }

  const obj1: Person<number> = {
    name: 'aaaa',
    age: 22,
    hobby: 11
  }


  type Test<T> = T | T[]
  const tt: Test<string> = ['a', 'd']







  type DeepArr<T> = T | DeepArr<T>[]

  // const arr4: DeepArr<string>[] = [[[[[[['1', '2', '3'], '4'], '5'], '6'], '7'], '8'], '9']


  function flat<T>(arr: DeepArr<T>[]): T[] {
    return arr.reduce((prev: T[], val) => {
      return prev.concat(Array.isArray(val) ? flat(val) : val)
    }, [] as T[])
  }

  console.log(flat<number>([[1,[2,3],4],5]))
  console.log(flat<string>([['a', ['b', 'd', 'c'], 'e'], 'f']))




}