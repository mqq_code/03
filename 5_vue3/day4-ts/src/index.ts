{

 // 内置泛型
  interface Person {
    name: string;
    age: number;
    sex: string;
    hobby?: string[];
  }

  const obj: Person = {
  name: '小明',
  age: 22,
  sex: '男',
  hobby: ['吃饭']
  } 

  type PartialPerson = Partial<Person> // 把所有属性变成可选属性
  type RequiredPerson = Required<Person> // 把所有属性变成必传属性
  type ReadonlyPerson = Readonly<Person> // 把所有属性变成只读属性

  type PickPerson = Pick<Person, 'name' | 'age' | 'hobby'> // 摘选部分属性组成一个新类型
  type OmitPerson = Omit<Person, 'name' | 'age'> // 排除部分属性组成一个新类型

  // 返回一个对象类型，key是第一个参数， value的类型是第二个参数
  type RecordObj = Record<'name' | 'age' | 'hobby', number>


  type A = string | number | number[]
  type B = string | number | null | boolean

  // 返回两个类型公有的子类型
  type ExtractType = Extract<A, B>

  // 从 A 类型中排除 A和B 公有的类型
  type ExcludeType = Exclude<A, B>


  const fn = (a: number, b: number) => {
    return (a + b).toFixed(2)
  }
  const sum = (...rest: number[]) => {
    return rest.reduce((prev, val) => prev + val, 0)
  }

  // type FnType = typeof fn

  // 获取函数返回值类型
  type FnReturn = ReturnType<typeof fn>

  type SumReturn = ReturnType<typeof sum>



}

// 1. typescript 有什么优势？
// 2. interface 和 type 的区别？
// 3. 什么是泛型？
// 4. 常用的内置泛型有哪些？
// 5. any、void、never？