## Typescript
1. 微软开发的开源项目
2. ts 是 js 的超集，ts 包含了 js 的所有语法，在js基础上扩展了一些新语法
3. ts 不可以在浏览器直接运行，需要编译成 js 运行
4. ts 增加了静态类型，可以在编译时校验类型，减少运行时的错误
5. 类型注解可以在一定程度上充当文档，提高代码可维护性
5. 编辑器对 ts 语法提示有较好的支持ts，自动补全，智能提示....

### 使用 typescript
1. 全局安装编译 ts 的工具 `npm i -g typescript`
2. 查看 tsc 版本号 `tsc -v`
3. 生成 tsconfig.json 编译配置 `tsc --init`
4. 实时编译 ts 文件 `tsc --watch`

### 类型注解
1. ts类型：number、string、boolean、null、undefined、symbol、array、object、function、any、void、never、unknown
2. 定义变量时添加类型注解，定义类型后变量不能赋值为其他类型
3. 定义指定类型的数组
- 泛型方式定义数组
- 元组：已知类型和长度的数组
4. any: 任意类型，相当于放弃类型校验
5. void: 没有值，通常用在函数没有返回值的情况
6. never: 永远不会出现的值
7. unknown: 暂时不确定值的类型，但是不想放弃校验，后续确定类型时可以使用类型断言


### interface 和 type：
- 1. interface 定义类型，type 是定义类型别名
- 2. type 可以定义基础类型、联合类型、交叉类型， interface 不行
- 3. interface 重名时会合并，可以扩展现有的 interface 定义的类型，例如扩展 window 对象
- 4. type 重名会报错
- 5. interface 使用 extends 继承，type 使用交叉类型扩展


### 泛型: 类型的形参，当函数中的某个或者某些变量类型只有调用时才能确定类型时可以使用泛型
