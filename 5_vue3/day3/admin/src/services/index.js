import request from './request'


// 统一管理所有接口
// 登录
export const loginApi = (params) => {
  return request.post('/api/login', params)

}

// 用户列表
export const getUserListApi = (params) => {
  return request.get('/api/userlist', {
    params
  })
}

// 用户信息
export const getUserInfoApi = () => {
  return request.get('/api/user/info')
}

// 删除用户
export const delUserApi = (id) => {
  return request.post('/api/user/delete', { id })
}

// 新增用户
export const createUserApi = (params) => {
  return request.post('/api/user/create', params)
}

// 编辑用户
export const updateUserApi = (params) => {
  return request.post('/api/user/update', params)
}