import axios from 'axios'
import router from '@/router'
import { ElMessage } from 'element-plus'

// 创建新的 axios 实例对象
const instance = axios.create({
  // 调用接口时统一拼接此域名
  baseURL: 'http://121.89.213.194:9001'
})


// 添加请求拦截器
instance.interceptors.request.use(function (config) {
  // 在请求发送之前统一添加公用参数，例如token
  if (config.url !== '/api/login') {
    config.headers.Authorization = localStorage.getItem('token')
  }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  return response;
}, function (error) {
  console.log(error)
  // 超出 2xx 范围的状态码都会触发该函数。
  // 统一处理错误信息，例如 401 自动跳转登录，403 跳转无权限页面
  if (error.response.status === 401) {
    localStorage.removeItem('token')
    ElMessage.error('登录信息失效，请重新登录')
    router.push('/login')
  } else {
    ElMessage.error(error.message)
  }
  return Promise.reject(error);
});

export default instance

