import { defineStore } from 'pinia'
import { ref } from 'vue'
import { getUserInfoApi } from '@/services'

export const useUserStore = defineStore('user', () => {
  const userInfo = ref({
    username: '',
    avatar: ''
  })

  const getUserInfo = async () => {
    const res = await getUserInfoApi()
    userInfo.value = res.data.values
  }

  return {
    userInfo,
    getUserInfo
  }
})