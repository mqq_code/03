import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Movie from '../views/Movie.vue'
import Cinema from '../views/Cinema.vue'
import Mine from '../views/Mine.vue'
import Detail from '../views/Detail.vue'
import NotFound from '../views/404.vue'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      redirect: '/movie',
      children: [
        {
          path: '/movie',
          name: 'movie',
          component: Movie
        },
        {
          path: '/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail,
      meta: {
        isAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/:pathMatch(.*)*', // 使用 .* 匹配任意字符，* 表示匹配 0 次或多次
      name: 'NotFound',
      component: NotFound
    }
  ]
})

router.beforeEach((to, from) => {
  if (to.meta.isAuth) {
    const token = localStorage.getItem('token')
    if (!token) {
      return {
        path: '/login',
        query: {
          a: 100,
          b: 'abcdefg'
        }
      }
    }
  }
})

export default router
