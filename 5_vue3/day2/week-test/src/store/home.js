import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

export const useHomeStore = defineStore('home', () => {
  const list = ref([])

  const add = (payload) => {
    list.value.push({
      ...payload,
      id: Date.now(),
      createTime: new Date().toLocaleString()
    })
  }

  const total = computed(() => {
    return list.value.reduce((prev, val) => {
      if (val.bjBack) {
        prev.bjBack += val.bj
      } else {
        prev.bj += val.bj
      }
      if (val.yjBack) {
        prev.yjBack += val.yj
      } else {
        prev.yj += val.yj
      }
      return prev
    }, {
      bjBack: 0,
      bj: 0,
      yjBack: 0,
      yj: 0
    })
  })

  const save = (payload) => {
    const index = list.value.findIndex(v => v.id === payload.id)
    list.value.splice(index, 1, {...payload})
  }

  return {
    list,
    total,
    add,
    save
  }
})