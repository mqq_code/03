import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import {
  Button,
  Form,
  Field,
  CellGroup,
  RadioGroup,
  Radio,
  Checkbox,
  Toast,
  Switch
} from 'vant'
import 'vant/lib/index.css'

const app = createApp(App)

app.use(router)
// 安装状态管理工具
app.use(createPinia())

app.use(Button)
app.use(Form)
app.use(Field)
app.use(CellGroup)
app.use(RadioGroup)
app.use(Radio)
app.use(Checkbox)
app.use(Toast)
app.use(Switch)




app.mount('#app')
