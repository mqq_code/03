import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'


const app = createApp(App)

app.use(router)
// 安装状态管理工具
app.use(createPinia())

app.mount('#app')
