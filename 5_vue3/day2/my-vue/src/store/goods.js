import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

// 定义 store
export const useGoodsStore = defineStore('goods', () => {
  // 定义数据
  const list = ref([])

  const total = computed(() => {
    return list.value.reduce((prev, val) => prev + val, 0)
  })

  return {
    list,
    total
  }
})