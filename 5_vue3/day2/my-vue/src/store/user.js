import { defineStore } from 'pinia'
import { ref } from 'vue'

// 定义 store
export const useUserStore = defineStore('user', () => {
  // 定义数据
  const name = ref('小明')
  const age = ref(22)


  const changeName = n => {
    name.value = n
  }


  return {
    name,
    age,
    changeName
  }
})