export const menuList = [
  {
    title: '资金管理',
    list: [
      {
        subTitle: '导航1',
        path: '/child1'
      },
      {
        subTitle: '导航2',
        path: '/child2'
      },
      {
        subTitle: '导航3',
        path: '/child3'
      }
    ]
  },
  {
    title: '系统管理',
    list: [
      {
        subTitle: '导航4',
        path: '/child4'
      },
      {
        subTitle: '导航5',
        path: '/child5'
      }
    ]
  }
]