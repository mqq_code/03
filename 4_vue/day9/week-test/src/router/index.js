import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import Child1 from '../views/home/Child1.vue'
import Child2 from '../views/home/Child2.vue'
import Child3 from '../views/home/Child3.vue'
import Child4 from '../views/home/Child4.vue'
import Child5 from '../views/home/Child5.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/child1',
    meta: {
      needLogin: true
    },
    children: [
      {
        path: '/child1',
        name: 'child1',
        component: Child1
      },
      {
        path: '/child2',
        name: 'child2',
        component: Child2
      },
      {
        path: '/child3',
        name: 'child3',
        component: Child3
      },
      {
        path: '/child4',
        name: 'child4',
        component: Child4
      },
      {
        path: '/child5',
        name: 'child5',
        component: Child5
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login/Login.vue')
  }
]

const router = new VueRouter({
  // hash模式： url有#
  // 原理：通过 window.onhashchange 事件监听 #后的内容发生改变，渲染对应的组件
  // history： url没有#，看起更美观
  // 通过 html5 的 pushState或者replaceState 修改历史记录对象,
  // 上线后必须有后端配置，不然页面刷新会404
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(v => v.meta.needLogin)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next({
        path: '/login'
      })
    }
  }
  next()
})

export default router
