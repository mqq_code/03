export default {
  // 命名空间，把此仓库内的变量和函数名和其他 modules 隔离
  namespaced: true,
  state: () => {
    return {
      username: '小明',
      age: 22,
      sex: '男',
      hobby: ['吃饭']
    }
  },
  mutations: {
    add(state, payload) {
      console.log('user.js', payload)
      state.age += payload
    }
  }
}