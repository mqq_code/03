import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
import user from './modules/user'
import goods from './modules/goods'
Vue.use(Vuex)

// 状态管理：存项目中的全局数据
export default new Vuex.Store({
  state: {
    title: '我是大仓库的标题'
  },
  mutations: {
    add() {
      console.log('index.js 的add')
    }
  },
  actions: {
  },
  getters: {
  },
  // 注册子仓库
  modules: {
    user,
    goods
  },
  plugins: [logger()]
})
