import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'

Vue.use(Vuex)

// 状态管理：存项目中的全局数据
export default new Vuex.Store({
  // 定义数据
  state: {
    banners: []
  },
  // 修改数据的唯一方式，必须是同步函数
  mutations: {
    setBanner(state, payload) {
      state.banners = payload
    }
  },
  // 在actions中定义调用接口的方法
  actions: {
    getBannerApi(context, a) {
      console.log(a)
      // context: 类似组件中的 this.$store，可以获取数据，调用 mutations 和 actions 的函数
      fetch('https://zyxcl.xyz/music/api/banner')
        .then(res => res.json())
        .then(res => {
          context.commit('setBanner', res.banners)
        })
    }
  },
  // 功能类似计算属性，函数内的变量改变时自动重新计算结果
  getters: {
  },
  // 插件
  plugins: [logger()]
})
