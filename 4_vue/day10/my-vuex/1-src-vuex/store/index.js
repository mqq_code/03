import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 状态管理：存项目中的全局数据
export default new Vuex.Store({
  // 定义数据
  state: {
    count: 10,
    arr: [],
    isOdd: false
  },
  // 修改数据的唯一方式，必须是同步函数
  mutations: {
    addCount(state, payload) {
      console.log(payload)
      state.count += payload
    },
    push(state, payload) {
      state.arr.push(payload)
    },
    changeOdd(state) {
      state.isOdd = !state.isOdd
    }
  },
  // 功能类似计算属性，函数内的变量改变时自动重新计算结果
  getters: {
    curArr(state) {
      if (state.isOdd) {
        return state.arr.filter(v => v % 2 === 1)
      }
      return state.arr
    }
  }
})
