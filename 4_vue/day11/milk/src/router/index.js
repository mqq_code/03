import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import List from '../views/home/List'

import Detail from '../views/detail/Detail'
import Search from '../views/search/Search'
import Notfound from '../views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/list/zh',
    children: [
      {
        path: '/list/:type',
        name: 'list',
        component: List
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/404',
    component: Notfound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
