const urls = [
  'https://zyxcl.xyz/exam_api/zh',
  'https://zyxcl.xyz/exam_api/xl',
  'https://zyxcl.xyz/exam_api/sx',
]

export const getAllData = async () => {
  const promises = urls.map(v => fetch(v).then(res => res.json()))
  const res = await Promise.all(promises)
  return res.reduce((prev, val) => {
    return prev.concat(val.items)
  }, [])
}