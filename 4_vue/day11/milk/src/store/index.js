import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
Vue.use(Vuex)

// 状态管理：存项目中的全局数据
export default new Vuex.Store({
  state: {
    isGrid: false,
    sortType: 0
  },
  mutations: {
    changeGrid(state) {
      state.isGrid = !state.isGrid
    },
    changeSort(state) {
      state.sortType++
      if (state.sortType > 2) {
        state.sortType = 0
      }
    }
  },
  plugins: [logger()]
})
