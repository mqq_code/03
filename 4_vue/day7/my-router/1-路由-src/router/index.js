import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home'
import Detail from '../pages/Detail'
import Login from '../pages/Login'

// 给vue安装路由插件
Vue.use(VueRouter)

// 配置路由，页面跳转地址时展示对应的组件
const routes = [
  {
    path: '/',
    redirect: '/home' // 重定向
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/detail',
    name: 'detail',
    component: Detail
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]
// 创建路由实例对象
const router = new VueRouter({
  // hash: url有#
  // history: url没有#
  mode: 'hash',
  routes
})

export default router
