import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router, // 把路由实例对象添加到vue实例上
  render: h => h(App)
}).$mount('#app')
