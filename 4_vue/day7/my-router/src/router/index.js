import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home'
import Movie from '../pages/home/movie/Movie'
import NowPlaying from '../pages/home/movie/now/NowPlaying'
import Coming from '../pages/home/movie/coming/Coming'
import Cinema from '../pages/home/cinema/Cinema'
import Mine from '../pages/home/mine/Mine'
import Detail from '../pages/detail/Detail'
import Login from '../pages/login/Login'
import City from '../pages/city/City'
import Notfound from '../pages/404'

// 给vue安装路由插件
Vue.use(VueRouter)

// 配置路由，页面跳转地址时展示对应的组件
const routes = [
  {
    path: '/',
    redirect: '/home' // 重定向
  },
  {
    path: '/home',
    name: 'home',
    redirect: '/home/movie',
    component: Home,
    meta: {
      isLogin: true
    },
    // 嵌套路由
    children: [
      {
        path: '/home/movie',
        name: 'movie',
        component: Movie,
        redirect: '/home/movie/now',
        children: [
          {
            path: '/home/movie/now',
            name: 'now',
            component: NowPlaying,
            meta: {
              title: '正在热映'
            }
          },
          {
            path: '/home/movie/coming',
            name: 'coming',
            component: Coming,
            meta: {
              title: '即将上映'
            }
          }
        ]
      },
      {
        path: '/home/cinema',
        name: 'cinema',
        component: Cinema,
        meta: {
          title: '影院'
        }
      },
      {
        path: '/home/mine',
        name: 'mine',
        component: Mine,
        meta: {
          title: '我的'
        },
        // 路由独享守卫
        beforeEnter: (to, from, next) => {
          // ...
          console.log('想要进入个人中心页面')
          next()
        }
      }
    ]
  },
  {
    // 动态路由, /:形参
    path: '/detail/:filmId',
    name: 'detail',
    component: Detail,
    meta: {
      title: '详情',
      isLogin: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      title: '登录'
    }
  },
  {
    path: '/city',
    name: 'city',
    component: City,
    meta: {
      title: '城市'
    }
  },
  {
    path: '*',
    name: '404',
    component: Notfound
  }
]
// 创建路由实例对象
const router = new VueRouter({
  // hash: url有#
  // history: url没有#
  mode: 'hash',
  routes
})


// 全局前置导航守卫
router.beforeEach((to, from, next) => {
  // console.log('来源路由', from)
  console.log('目标路由', to)
  document.title = to.meta.title
  // 从路由信息中判断此路由是否需要登录信息
  if (to.matched.some(v => v.meta.isLogin)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next({
        path: '/login',
        query: {
          redirectUrl: to.fullPath
        }
      })
      return
    }
  }
  next()
})

// 全局后置钩子
router.afterEach((to, from) => {
  console.log('页面跳转完成')
})

export default router
