import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')


// const obj = () => {
//   return {
//     name: 11
//   }
// }

// const obj1 = obj()
// const obj2 = obj()
// const obj3 = obj()
// const obj4 = obj()

// obj1.name = 22

// console.log(obj1)
// console.log(obj2)
// console.log(obj3)
// console.log(obj4)