import Vue from 'vue'
import App from './App.vue'
import loadingImg from './assets/loading.gif'
import failImg from './assets/fail.webp'

Vue.config.productionTip = false



const loadImg = (el, value) => {
  // 显示加载中
  el.src = loadingImg
  // 开始加载图片
  const img = new Image()
  img.src = value
  img.onload = () => {
    console.log('加载成功')
    el.src = img.src
  }
  img.onerror = () => {
    console.log('加载失败')
    el.src = failImg
  }
}

// 注册指令
Vue.directive('load', {
  // bind：只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置。
  bind(el, binding) {
    console.log('load 指令绑定成功', el, binding.value)
    loadImg(el, binding.value)
  },
  // inserted: 只调用一次，被绑定元素插入父节点时调用
  // inserted(el, binding) {
  //   console.log('inserted 指令绑定成功', el.parentNode)
  // }
  // update: 所在组件更新，不一定更新完成
  update(el, binding) {
    if (binding.value !== binding.oldValue) {
      console.log('update', el, binding)
      loadImg(el, binding.value)
    }
  },
  // componentUpdated：更新成功，可以获取到最新的dom
  // componentUpdated() {
  // }
  // unbind：指令解绑时调用，清除异步任务，例如定时器、原生事件
  unbind(el, binding) {
    console.log('指令解绑')
  }
})

Vue.directive('copy', {
  bind(el, binding) {
    console.log(el, binding.value)

    el.copyText = binding.value
    el.handleClick = () => {
      const textarea = document.createElement('textarea')
      textarea.value = el.copyText
      document.body.appendChild(textarea)
      textarea.select()
      // 复制当前页面选中的内容
      document.execCommand('copy')
      document.body.removeChild(textarea)
      alert('复制成功')
    }
    el.addEventListener('click', el.handleClick)

  },
  update(el, binding) {
    console.log(el, binding.value)
    el.copyText = binding.value
  },
  unbind(el) {
    // 清除异步
    el.removeEventListener('click', el.handleClick)
  }
})


new Vue({
  render: h => h(App)
}).$mount('#app')
