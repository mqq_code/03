import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
Vue.use(Vuex)

// 状态管理：存项目中的全局数据
export default new Vuex.Store({

  plugins: [logger()]
})
