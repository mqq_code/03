import Mock from 'mockjs'

const data = Mock.mock({
  'list|200': [{
    id: '@id',
    name: '@cname',
    'age|18-40': 10,
    'sex|0-1': 0,
    address: '@county(true)',
    email: '@email',
    'no|+1': 1
  }]
})

// 拦截接口
// Mock.mock('/api/list', 'post', (url) => {
//   const { pageNum, pageSize } = JSON.parse(url.body)
//   console.log('请求地址', url.body)

//   return {
//     code: 0,
//     msg: '成功',
//     data: {
//       total: data.list.length,
//       list: data.list.slice((pageNum - 1) * pageSize, pageNum * pageSize)
//     }
//   }
// })


Mock.mock('/api/list', (url) => {
  return data.list
})