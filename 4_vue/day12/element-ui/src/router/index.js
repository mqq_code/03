import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Detail from '../views/detail/Detail'
import Search from '../views/search/Search'
import List from '../views/list/List'
import TableList from '../views/table-list/TableList'

import Notfound from '../views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/detail',
    name: 'detail',
    component: Detail
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/list',
    name: 'list',
    component: List
  },
  {
    path: '/tableList',
    name: 'tableList',
    component: TableList
  },
  
  {
    path: '/404',
    component: Notfound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
