const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'http://ustbhuangyi.com',
        changeOrigin: true,
        pathRewrite: { '^/api': '' }
      }
    }
  }
})
