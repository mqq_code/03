import Mock from 'mockjs'
import city from './city.json'



function format(obj) {
  return Object.entries(obj).map(([value, label]) => {
    const childObj = {}
    if (city[value]) {
      childObj.children = format(city[value])
    }
    return {
      label,
      value: label,
      ...childObj
    }
  })
}






const data = Mock.mock({
  'list|200': [{
    id: '@id',
    name: '@cname',
    'age|18-40': 10,
    'sex|0-1': 0,
    address: '@county(true)',
    email: '@email',
    'no|+1': 1
  }]
})

// 拦截接口
// Mock.mock('/api/list', 'post', (url) => {
//   const { pageNum, pageSize } = JSON.parse(url.body)
//   console.log('请求地址', url.body)

//   return {
//     code: 0,
//     msg: '成功',
//     data: {
//       total: data.list.length,
//       list: data.list.slice((pageNum - 1) * pageSize, pageNum * pageSize)
//     }
//   }
// })


Mock.mock('/api/list', (url) => {
  return data.list
})


const cityData = format(city[86])
console.log(cityData)
Mock.mock('/api/city', () => {
  return cityData
})