import request from './request'
import type {
  BaseRes,
  UserList,
  UserListParams,
  UpdateUserListParams,
  CreateUserParams,
  User,
  RoleItem
} from '../types/services/systemapi'

export const userListApi = (params: UserListParams) => {
  return request.get<BaseRes<UserList>>('/user/list', {
    params
  })
}

export const updateUserListApi = (params: UpdateUserListParams) => {
  return request.post<BaseRes>('/user/update', params)
}

export const delUserListApi = (params: { id: string }) => {
  return request.post<BaseRes>('/user/remove', params)
}
export const createUserApi = (params: CreateUserParams) => {
  return request.post<BaseRes>('/user/create', params)
}

// 角色列表
export const roleListApi = () => {
  return request.get<BaseRes<{ list: RoleItem[] }>>('/role/list')
}
// 编辑角色
export const roleUpdateApi = (params: { id: string; permission: string[] }) => {
  return request.post<BaseRes>('/role/update', params)
}


// 修改当前用户信息
type UpdateUserParams = Partial<Pick<User, | 'age' | 'sex' | 'email' | 'avator'>> & { username: string }
export const updateUserApi = (params: UpdateUserParams) => {
  return request.post<BaseRes>('/user/update/info', params)
}


// 查询左侧菜单接口
export const menuListApi = () => {
  return request.get<BaseRes>('/user/menulist')
}


// 查询所有菜单
export const permissionListApi = () => {
  return request.get<BaseRes>('/permission/list')
}