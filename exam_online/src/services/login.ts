import request from './request'
import type {
  BaseRes,
  LoginParams,
  LoginResponse,
  CaptchaResponse,
  UserInfoResponse
} from '../types/services/login'



export const loginApi = (params: LoginParams) => {
  return request.post<BaseRes<LoginResponse>>('/login', params)
}

export const loginCaptchaApi = () => {
  return request.get<BaseRes<CaptchaResponse>>('/login/captcha')
}


export const userInfoApi = () => {
  return request.get<BaseRes<UserInfoResponse>>('/user/info')
}



// /user/logout