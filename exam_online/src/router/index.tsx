import Home from "../pages/home/Home"
import Login from "../pages/login/Login"
import UserInfo from "../pages/userInfo/UserInfo"
import UserList from "../pages/userList/UserList"
import RoleList from "../pages/roleList/RoleList"
import GroupList from "../pages/groupList/GroupList"
import { Button, Result } from 'antd'
import { Link } from 'react-router-dom'
import Layout from "../components/layout/Layout"
import Auth from '../components/auth/Auth'

const routes = [
  {
    path: '/',
    element: (
      <Layout>
        <Home />
      </Layout>
    )
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/userManage/personal',
    element: (
      <Layout>
        <Auth>
          <UserInfo />
        </Auth>
      </Layout>
    )
  },
  {
    path: '/userManage/manage-page',
    element: (
      <Layout>
        <Auth>
          <UserList />
        </Auth>
      </Layout>
    )
  },
  {
    path: '/userManage/system',
    element: (
      <Layout>
        <Auth>
          <RoleList />
        </Auth>
      </Layout>
    )
  },
  {
    path: '/manage-group/group-list',
    element: (
      <Layout>
        <Auth>
          <GroupList />
        </Auth>
      </Layout>
    )
  },
  {
    path: '/manage-group/group-students',
    element: (
      <Layout>
        <Auth>
          <div>学生</div>
        </Auth>
      </Layout>
    )
  },
  {
    path: '/exam/create',
    element: (
      <Layout>
        <Auth>
          <div>创建考试</div>
        </Auth>
      </Layout>
    )
  },
  {
    path: '/exam/record',
    element: (
      <Layout>
        <Auth>
          <div>考试记录</div>
        </Auth>
      </Layout>
    )
  },
  {
    path: '/403',
    element: <Result
      status="403"
      title="403"
      subTitle="没有权限访问此页面，请联系管理员！"
      extra={(
        <Link to="/">
          <Button type="primary">回到首页</Button>
        </Link>
      )}
    />
  },
  {
    path: '*',
    element: <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={(
        <Link to="/">
          <Button type="primary">回到首页</Button>
        </Link>
      )}
    />
  }
]

export default routes