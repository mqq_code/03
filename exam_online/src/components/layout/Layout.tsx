import {
  LogoutOutlined,
  UserOutlined,
  CrownFilled,
  SmileFilled,
} from '@ant-design/icons'
import {
  PageContainer,
  ProCard,
  ProConfigProvider,
  ProLayout,
} from '@ant-design/pro-components'
import {
  Dropdown,
  Spin
} from 'antd';
import React, { useEffect, useState } from 'react'
// import defaultProps from './defaultProps'
import {
  useNavigate,
  useLocation,
  Link
} from 'react-router-dom'
import { getUserInfoAction } from '../../store/models/user'
import { useDispatch, useSelector } from 'react-redux'
import type { RootState, AppDispatch } from '../../store'
import { menuListApi } from '../../services/systemapi'

interface Props {
  children: React.ReactNode
}

const Layout: React.FC<Props> = (props) => {
  const navigate = useNavigate()
  const location = useLocation()
  const dispatch: AppDispatch = useDispatch()
  const userInfo = useSelector((state: RootState) => state.user.info)
  const loading = useSelector((state: RootState) => state.user.loading)
  const [menuList, serMenuList] = useState<any>([])

  // console.log(userInfo)

  useEffect(() => {
    // 发送异步action
    dispatch(getUserInfoAction())
    menuListApi()
      .then(res => {
        // console.log(res.data.data.list)
        serMenuList(res.data.data.list.map(item => {
          return {
            ...item,
            routes: item.children
          }
        }))
      })
      .catch(e => {
        console.log(e)
      })
  }, [])

  if (loading) return (
    <div style={{ width: '100vw', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <Spin size="large" />
    </div>
  )

  return (
    <div
      id="test-pro-layout"
      style={{
        height: '100vh',
        overflow: 'auto',
      }}
    >
      <ProConfigProvider hashed={false}>
        <ProLayout
          title="online"
          logo="https://cn.redux.js.org/img/redux.svg"
          prefixCls="my-prefix"
          // 左侧菜单
          route={{
            path: '/',
            routes: [
              {
                path: '/',
                name: '首页',
                icon: <SmileFilled />,
              },
              ...menuList
              // {
              //   path: '/userManage',
              //   name: '管理页',
              //   icon: <CrownFilled />,
              //   routes: [
              //     {
              //       path: '/userManage/personal',
              //       name: '个人信息',
              //       icon: <CrownFilled />,
              //     },
              //     {
              //       path: '/userManage/manage-page',
              //       name: '用户列表',
              //       icon: <CrownFilled />,
              //     }
              //   ]
              // }
            ]
          }}
          location={{
            pathname: location.pathname,
          }}
          token={{
            header: {
              colorBgMenuItemSelected: 'rgba(0,0,0,0.04)',
            },
          }}
          siderMenuType="group"
          menu={{
            collapsedShowGroupTitle: true,
          }}
          avatarProps={{
            src: userInfo.avator,
            size: 'small',
            title: userInfo.username,
            render: (props, dom) => {
              return (
                <Dropdown
                  menu={{
                    items: [
                      {
                        key: 'logout',
                        icon: <LogoutOutlined />,
                        label: '退出登录',
                      },
                      {
                        key: 'userinfo',
                        icon: <UserOutlined />,
                        label: <Link to="/userManage/personal">个人信息</Link>,
                      }
                    ],
                    onClick: ({ key }) => {
                      if (key === 'logout') {
                        localStorage.removeItem('token')
                        navigate('/login')
                      }
                    }
                  }}
                >
                  {dom}
                </Dropdown>
              );
            },
          }}
          menuItemRender={(item, dom, props) => (
            <div
              onClick={() => {
                const cur = props.route?.routes.find(v => v.path === item.path)
                if (cur?.routes) {
                  navigate(cur?.routes[0].path)
                } else {
                  navigate(item.path!)
                }
              }}
            >
              {dom}
            </div>
          )}
          fixSiderbar={true}
          layout="mix"
          splitMenus={true}
        >
          <PageContainer>
            <ProCard
              style={{
                minHeight: 500,
              }}
            >
              {props.children}
            </ProCard>
          </PageContainer>
        </ProLayout>
      </ProConfigProvider>
    </div>
  );
};

export default Layout
