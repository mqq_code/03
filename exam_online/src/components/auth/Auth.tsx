import React from 'react'
import { useSelector } from 'react-redux'
import type { RootState } from '../../store'
import { useLocation, Navigate } from 'react-router-dom'

type Props = React.PropsWithChildren

const Auth = (props: Props) => {
  const location = useLocation()
  const permission = useSelector((state: RootState) => state.user.info.permission)
  console.log(permission, location.pathname)
  // 获取当前页面的path，去所有权限中查找是否存在此权限
  const route = permission.find(v => v.path === location.pathname)
  if (route) {
    return props.children
  }
  return <Navigate to="/403" />
}

export default Auth