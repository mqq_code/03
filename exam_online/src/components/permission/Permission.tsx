import React from 'react'
import { useSelector } from 'react-redux'
import type { RootState } from '../../store'

type Props ={
  children: React.ReactNode
  perKey: string // 需要判断的权限值
}

const Permission: React.FC<Props> = (props) => {
  const permission = useSelector((state: RootState) => state.user.info.permission)

  if (permission.find(v => v.path === props.perKey)) {
    return props.children
  }
  return null
}

export default Permission