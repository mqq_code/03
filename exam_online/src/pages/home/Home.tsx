import React from 'react'
import { Button } from 'antd'
import Permission from '../../components/permission/Permission'
const Home = () => {
  return (
    <div>
      <h1>首页</h1>
      <Permission perKey="paperDel">
        <Button danger type="primary">删除试卷</Button>
      </Permission>
    </div>
  )
}

export default Home