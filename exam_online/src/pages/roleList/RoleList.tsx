import React, { useEffect, useState } from 'react'
import { roleListApi, permissionListApi, roleUpdateApi } from '../../services/systemapi'
import { Space, Table, Button, Drawer, Tree, message } from 'antd'
import type { TableProps } from 'antd'
import type { RoleItem } from '../../types/services/systemapi'
import dayjs from 'dayjs'


type CheckedPer = {
  checked: string[]
  halfChecked: string[]
}


const RoleList = () => {
  const [open, setOpen] = useState(false)
  const [data, setData] = useState<RoleItem[]>([])
  const [permission, setPermission] = useState<any[]>([])
  const [curRowPer, serCurRowPer] = useState<CheckedPer>({
    checked: [],
    halfChecked: []
  })
  const [editId, setEditId] = useState<string | null>(null)

  const getRoleList = () => {
    roleListApi()
      .then(res => {
        setData(res.data.data.list)
      })
  }

  useEffect(() => {
    getRoleList()
    
    permissionListApi()
      .then(res => {
        setPermission(res.data.data.list)
      })
  }, [])

  const columns: TableProps<RoleItem>['columns'] = [
    {
      title: '角色名',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '创建人',
      dataIndex: 'creator',
      key: 'creator',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button size="small" type="primary" onClick={() => {
            setOpen(true)
            const obj: CheckedPer = {
              checked: [],
              halfChecked: []
            }
            record.permission.forEach(id => {
              // 遍历当前行的权限，判断此权限是不是第一级权限
              const first = permission.find(item => item._id === id)
              if (first) {
                // 如果是第一级权限，并且所有子权限都在当前权限内，表示当前权限可以全选
                if (first.children.every(v => record.permission.includes(v._id))) {
                  obj.checked.push(id)
                } else {
                  obj.halfChecked.push(id)
                }
              } else {
                obj.checked.push(id)
              }
            })
            serCurRowPer(obj)
            setEditId(record._id)
          }}>分配角色</Button>
        </Space>
      ),
    },
  ]

  const submit = async () => {
    const res = await roleUpdateApi({
      id: editId!,
      permission: curRowPer.halfChecked.concat(curRowPer.checked)
    })
    if (res.data.code === 200) {
      message.success('修改成功')
      setOpen(false)
      getRoleList()
    } else {
      message.error(res.data.msg)
    }
  }

  return (
    <div>
      <Table columns={columns} dataSource={data} rowKey="_id" />
      <Drawer
        title="分配菜单"
        onClose={() => setOpen(false)}
        open={open}
        width={500}
        footer={<Button type="primary" onClick={submit}>确定</Button>}
      >
        <Tree
          checkable
          defaultExpandAll
          checkedKeys={curRowPer}
          onCheck={(checked: any, obj: any) => {
            serCurRowPer({
              checked,
              halfChecked: obj.halfCheckedKeys
            })
          }}
          treeData={permission}
          fieldNames={{
            key: '_id',
            title: 'name'
          }}
        />
      </Drawer>

    </div>
  )
}

export default RoleList