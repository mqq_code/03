import React, { useState } from 'react'
import Info from './components/Info'
import Edit from './components/Edit'

const UserInfo = () => {
  const [showInfo, setShowInfo] = useState(true)
  return (
    <div>
      {showInfo ?
        <Info onChange={() => setShowInfo(false)} />
      : 
        <Edit onCancel={() => setShowInfo(true)} />
      }
    </div>
  )
}

export default UserInfo