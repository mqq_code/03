import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import type { RootState, AppDispatch } from '../../../store'
import type { FormProps } from 'antd'
import { Button, Space, Form, Input, InputNumber, Select, message, Image, Upload } from 'antd'
import { updateUserApi } from '../../../services/systemapi'
import { getUserInfoAction } from '../../../store/models/user'
import type { GetProp, UploadFile, UploadProps } from 'antd'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import style from './Edit.module.scss'

type FieldType = {
  username: string
  age?: number
  sex?: '男' | '女'
  email?: string
  avator?: string
}

type Props = {
  onCancel: () => void
}


type FileType = Parameters<GetProp<UploadProps, 'beforeUpload'>>[0];

// 把图片转成 base64
const getBase64 = (img: FileType, callback: (url: string) => void) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result as string));
  reader.readAsDataURL(img)
}

const Edit: React.FC<Props> = (props) => {
  const userInfo = useSelector((state: RootState) => state.user.info)
  const dispatch: AppDispatch = useDispatch()
  const [form] = Form.useForm()

  const onFinish: FormProps<FieldType>['onFinish'] = async (values) => {
    const res = await updateUserApi(values)
    if (res.data.code === 200) {
      message.success('修改用户信息成功')
      dispatch(getUserInfoAction())
      props.onCancel()
    } else {
      message.error(res.data.msg)
    }
  }

  const [imageUrl, setImageUrl] = useState<string>()

  // 上传图片时执行此函数
  const handleChange: UploadProps['onChange'] = (info) => {
    // 上传完成，把图片转成 base64 展示到页面
    getBase64(info.file as FileType, (url) => {
      setImageUrl(url)
      // form.setFieldValue('avator', url)
      form.setFieldValue('avator', 'https://q8.itc.cn/q_70/images03/20240827/897db59cd0c0469d8a3f26f6bb05acea.jpeg')
    })
  }

  useEffect(() => {
    setImageUrl(userInfo.avator)
  }, [])


  return (
    <Form
      form={form}
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 20 }}
      initialValues={userInfo}
      style={{ maxWidth: 500 }}
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        wrapperCol={{ offset: 4 }}
        name="avator"
      >
        <Upload
          listType="picture-circle"
          showUploadList={false}
          beforeUpload={() => false}
          onChange={handleChange}
          className={style.pictrue}
        >
          {imageUrl ?
            <img src={imageUrl} alt="avatar" style={{ width: '100%', height: '100%' }} />
          :
            <button style={{ border: 0, background: 'none' }} type="button">
              <PlusOutlined />
              <div style={{ marginTop: 8 }}>Upload</div>
            </button>
          }
        </Upload>
      </Form.Item>

      <Form.Item<FieldType>
        label="用户名"
        name="username"
        rules={[{ required: true, message: '请输入用户名!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="年龄"
        name="age"
      >
        <InputNumber />
      </Form.Item>

      <Form.Item<FieldType>
        label="性别"
        name="sex"
      >
        <Select
          options={[
            { label: '男', value: '男' },
            { label: '女', value: '女' }
          ]}
        />
      </Form.Item>

      <Form.Item<FieldType>
        label="邮箱"
        name="email"
      >
        <Input />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 4 }}>
        <Space>
          <Button type="primary" htmlType="submit">保存</Button>
          <Button onClick={props.onCancel}>取消</Button>
        </Space>
      </Form.Item>
    </Form>
  )
}

export default Edit