import React from 'react'
import { Button } from 'antd'
import Permission from '../../components/permission/Permission'

const GroupList = () => {


  return (
    <div>
      <h2>班级列表</h2>
      <Permission perKey="groupDelBtn">
        <Button danger>删除班级</Button>
      </Permission>
    </div>
  )
}

export default GroupList