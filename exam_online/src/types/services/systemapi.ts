export type { BaseRes } from './base'


export type UserListParams = {
  page: number
  pagesize: number
}

// 用户信息
export type User = {
  avator: string
  creator: string
  lastOnlineTime: number
  _id: string
  role: string[]
  password: string
  status: 0 | 1
  username: string
  sex: '男' | '女'
  email: string
  age: number
}

export type UserList = {
  list: User[]
  total: number
  totalPage: number
}
// 创建用户参数
export type CreateUserParams = Pick<User, 'username' | 'password' | 'status' | 'age' | 'email' | 'sex'>
// 编辑用户的参数
export type UpdateUserListParams = { id: string } & Partial<Omit<User, 'creator' | 'lastOnlineTime' | '_id'>>


// 角色列表
export type RoleItem = {
  createTime: number
  creator: string
  disabled: boolean
  name: string
  permission: string[]
  value: string
  _id: string
}