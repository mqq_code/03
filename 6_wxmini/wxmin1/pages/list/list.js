// pages/list/list.js
import moment from 'moment'

console.log(moment(Date.now()).format('YYYY-MM-DD'));

Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 0
  },

  changeNum(e) {
    // 接收标签上的自定义属性
    const { num } = e.target.dataset
    this.setData({
      num: this.data.num + num
    })
  },
  childChangeNum(e) {
    console.log('子组件调用了changeNum', e);
    // 接收子组件传入的参数
    const { num } = e.detail
    this.setData({
      num: this.data.num + num
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})