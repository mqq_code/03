// pages/mine/mine.js

interface Banner {
  imageUrl: string;
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners: [] as Banner[]
  },

  goDetail() {
    wx.navigateTo({
      url: '/pages/detail/detail?a=100&b=200'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.request<{ banners: Banner[] }>({
      url: 'https://zyxcl.xyz/music/api/banner',
      success: res => {
        this.setData({
          banners: res.data.banners
        })

      }
    })
    console.log('%c onload', 'color: red; font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log('%c onReady', 'color: red; font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log('%c onShow', 'color: red; font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log('%c onHide', 'color: green; font-size: 30px');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log('%c onUnload', 'color: green; font-size: 30px');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})