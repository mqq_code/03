// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '标题',
    count: 0,
    arr: ['a', 'b', 'c', 'd', 'e'],
    obj: {
      name: '小明',
      age: 22,
      info: {
        a: {
          b: {
            c: {
              d: 11
            }
          }
        }
      }
    },
    scrollTop: 0
  },
  changeCount(e) {
    console.log('接收页面传入的自定义属性', e.target.dataset);
    const { num } = e.target.dataset
    // 定义函数修改数据, 通过setData触发页面更新
    this.setData({
      count: this.data.count + num
    })
    // console.log(this.data.count);
  },
  addArr() {
    // const newArr = [...this.data.arr]
    // newArr.unshift(Math.random())
    // this.setData({
    //   arr: newArr
    // })
    this.setData({
      arr: [...this.data.arr, Math.random()],
      scrollTop: this.data.scrollTop + 40
    })
  },
  addAge() {
    this.setData({
      'obj.age': this.data.obj.age + 1,
      'obj.info.a.b.c.d': this.data.obj.info.a.b.c.d + 1
    })
  },

  changeName(e) {
    console.log('获取input内容', e.detail.value);
    this.setData({
      'obj.name': e.detail.value
    })
  },

  scroll(e) {
    console.log(e);
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})