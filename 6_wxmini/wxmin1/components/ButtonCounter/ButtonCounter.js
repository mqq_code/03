// components/ButtonCounter/ButtonCounter.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    num: Number
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    tapBtn(e) {
      // console.log(e.target.dataset);
      // 调用父组件传入的函数, 给父组件传参数
      this.triggerEvent('clickbtn', e.target.dataset)
    }
  }

})