"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _component_uni_countdown = common_vendor.resolveComponent("uni-countdown");
  _component_uni_countdown();
}
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  __name: "mine",
  setup(__props) {
    common_vendor.ref({
      name: "aaa",
      age: 22
    });
    const title = common_vendor.ref("标题");
    const arr = common_vendor.ref([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]);
    const changeTitle = () => {
      title.value = "123";
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          day: 1,
          hour: 1,
          minute: 12,
          second: 40
        }),
        b: common_vendor.o(changeTitle),
        c: common_vendor.t(title.value),
        d: title.value,
        e: common_vendor.o(($event) => title.value = $event.detail.value),
        f: common_vendor.f(arr.value, (item, k0, i0) => {
          return {
            a: common_vendor.t(item),
            b: item
          };
        })
      };
    };
  }
});
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/03/6_wxmini/uniapp/my-uniapp/pages/mine/mine.vue"]]);
wx.createPage(MiniProgramPage);
